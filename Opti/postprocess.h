#ifndef POSTPROCESS_H_INCLUDE
#define POSTPROCESS_H_INCLUDE
#include <fstream>

#include "RectPart2D.h"
using namespace std;

void VTKOutPut(int num_procs_x,int num_procs_y,int num_procs_z,int nx,int ny,int nz,int storeSteps);
void GenerateLoadImbalanceFile(Partitioner_t *partitioner,int steps,ofstream &floadim);

void GenerateExecutedInfo(int num_procs1,int num_procs2,int num_procs3,int steps,ofstream &fld);

#endif
