#include "compute_cell.h"
#include "cell.h"

#include "mkl_vsl.h"
#include <cmath>
#include <cstring>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <random>

using namespace std;
void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z,double t, double ist, int bcl, double ***v,double ***dtstep,int Ndhpr , int Ntotdyads,int Nryr,double ***flagArray,double rt)
{
    double dt = dtstep[ix][iy][iz];
    double kmcsqn = 1;
    int Ndyads = 100;
     double csqnbar = 10;

    //added by qianglan to generate random data using intel MKL lib
    long randomseed;
    //Cell.totalCellSize = 400*400*400;
    randomseed = Cell.cellID + Cell.timestep*Cell.totalCellSize;
    Cell.timestep+=1;
    //__attribute__((align(64))) double * l_Random;
    double * l_Random;
    l_Random = Cell.l_Random;
    unsigned long rand_n = Cell.rand_n;
    if(!l_Random)
    {
            printf("malloc random memory failed!\n");
            return ;
    }
   // VSLStreamStatePtr Randomstream;
   // vslNewStream(&Randomstream, VSL_BRNG_MCG31, randomseed);

    //vdRngUniform(0, Randomstream, rand_n, l_Random, 0.0, 1.0-(1.2e-12));

    //////////////////////////////////////////below code is moved from inside of the dyads loop////////////
    ///////////////begin of dyads loop//////////////////////if(%10==0) dt=0.1
    for (int i=0; i<Ndyads;i++)
    {
       
         double Km = 5;
         double Kmcsqn = 150;
         double dryr = 4;
         int nryropen = 0;
         Cell.nactive[i] = 0;

         for (int k=0; k<Nryr; k++)
         {
                 if(((Cell.unitsInCell[i]).ryrstate[k] == 1) || ((Cell.unitsInCell[i]).ryrstate[k] == 3)) nryropen++;
         }

         if (  (1.0*nryropen/Nryr)>0.2) {Cell.trel[i] = t; Cell.nactive[i] = 1;}

         double  kC1O1 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Km,4)));
         double kO1C1 = 0.5;
         double kC2O2 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Kmcsqn,4)));
         double kO2C2 = kO1C1;

         //calsequestrin
         double csqn = csqnbar*pow(kmcsqn,8)/(pow(Cell.unitsInCell[i].cajsr,8) + pow(kmcsqn,8) );
         double csqnca = csqnbar - csqn;

         double kC1C2 = 1*(csqn/csqnbar);

         double tref;

             tref = t-Cell.trel[i]-0.95*bcl;
         double kC2C1 = 1*(csqnca/csqnbar)*1/(1+exp(-tref/0.001));//*(t-Cell.trel[i]-tref);

         double kO1O2 = kC1C2;
         double kO2O1 = kC2C1;


         ////%%%%%%8.1 comp_ical_dyad()%%%%%%%%%%%//
        //km2n=Cell.jca*1.0;
        //anca=1.0/(k2n/km2n+pow(1.0+Kmn/(0.001*Cell.unitsInCell[i].cads),4.0));
         int nryrc1,nryro1,nryrc2,nryro2;
       /* printf("ryrstate caculate start!\n"); 
        printf("before nryr_in_state_0=%d\n",Cell.unitsInCell[i].nryr_in_state_0);
        printf("before nryr_in_state_1=%d\n",Cell.unitsInCell[i].nryr_in_state_1);
        printf("before nryr_in_state_2=%d\n",Cell.unitsInCell[i].nryr_in_state_2);
        printf("before nryr_in_state_3=%d\n",Cell.unitsInCell[i].nryr_in_state_3);*/
        std::random_device rd;
        std::mt19937 gen(rd());
        //replaces case 0
        std::binomial_distribution<> a0(Cell.unitsInCell[i].nryr_in_state_0, kC1O1*dt);    //binomial distribution returns number of transitions from 0 to 1
        nryro1=a0(gen);
        std::binomial_distribution<> b0(Cell.unitsInCell[i].nryr_in_state_0, kC1C2*dt);   //binomial distribution returns number of transitions from 0 to 1
        nryrc2=b0(gen);        
        nryrc1=Cell.unitsInCell[i].nryr_in_state_0-nryro1-nryrc2;        //remainder is the number of unchanged states 0
        
        //replaces case 1 etc
        std::binomial_distribution<> a1(Cell.unitsInCell[i].nryr_in_state_1, kO1C1*dt);    
        nryrc1+=a1(gen);
        std::binomial_distribution<> b1(Cell.unitsInCell[i].nryr_in_state_1, kO1O2*dt);   
        nryro2=b1(gen);        
        nryro1+=Cell.unitsInCell[i].nryr_in_state_1-nryrc1-nryro2;        
        //
        std::binomial_distribution<> a2(Cell.unitsInCell[i].nryr_in_state_2, kC2C1*dt);    
        nryrc1+=a2(gen);
        std::binomial_distribution<> b2(Cell.unitsInCell[i].nryr_in_state_2, kC2O2*dt);   
        nryro2+=b2(gen);        
        nryrc2+=Cell.unitsInCell[i].nryr_in_state_2-nryrc1-nryro2;        
        //
        std::binomial_distribution<> a3(Cell.unitsInCell[i].nryr_in_state_3, kO2C2*dt);    
        nryrc2+=a3(gen);
        std::binomial_distribution<> b3(Cell.unitsInCell[i].nryr_in_state_3, kO2O1*dt);   
        nryro1+=b3(gen);        
        nryro2+=Cell.unitsInCell[i].nryr_in_state_3-nryrc2-nryro1;        
        //
        Cell.unitsInCell[i].nryr_in_state_0=nryrc1;
        Cell.unitsInCell[i].nryr_in_state_1=nryro1;
        Cell.unitsInCell[i].nryr_in_state_2=nryrc2;
        Cell.unitsInCell[i].nryr_in_state_3=nryro2;
       /* printf("ryrstate caculate end!\n");
        printf("after nryr_in_state_0=%d\n",Cell.unitsInCell[i].nryr_in_state_0);
        printf("after nryr_in_state_1=%d\n",Cell.unitsInCell[i].nryr_in_state_1);
        printf("after nryr_in_state_2=%d\n",Cell.unitsInCell[i].nryr_in_state_2);
        printf("after nryr_in_state_3=%d\n",Cell.unitsInCell[i].nryr_in_state_3);*/


     }//////end of dyads loop



    //vslDeleteStream(&Randomstream);
}
