#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>

#include <omp.h>
#include <malloc.h>
#include <stdlib.h>


#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"
#include "UpdateGhost.h"
#include "postprocess.h"
#include "printInfo.h"
#include <time.h>

#define Ntotdyads 10000


using namespace std;
//

double ***v, ***vtemp,***dvdt,***dvdtclock;
double ***flagArray;
double ***dtstep;
double t,dt,d_x,Dx;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=500;

ofstream fout;
ofstream f0out;
ostringstream file;
ostringstream file0;

ofstream fcalcium ("calcium");      // Output Data file for whole-cell variables
ofstream fcurrents ("currents");
ofstream fdyad ("dyad");    // Output Data file for dyadic variables
//ofstream fcacyt ("cacyt");   // Output Data file for 1D Cai plot (along z axis)
//ofstream fcajsr ("cajsr");   // Output Data file for 1D CaSR plot (along z axis)
//ofstream f5out("initial_conditions");
//ofstream fpaper ("cpvt_paper");
ofstream flag;
ostringstream flagName;

ofstream floadim;
ostringstream floadName;

ofstream fld("fldInfo");

int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	//Dx = 0.1;
        Dx = 0.2;


	tstim = 100;
	stimtime = 100;

//	steps = 2;
		
	time_t now;
    struct tm *ptr_ts;	

	//***************************************************
	Partitioner_t* partitioner;
	int num_procs1=0, num_procs2=0,num_procs3=0;
	int global_N_x,global_N_y,global_N_z;
	int  Nryr = 100;
	int Ndhpr = 15;
	int Ndyads = 100;
	int beats = 1;
	int Unit_X=100;
	int Unit_Y=1;
	int Unit_Z=1;	


	steps = beats*bcl/dt;
        steps = 11;
	int N_x, N_y,N_z;
	char *output_dir=NULL; 
	//int x_axi=0;
	int storeStrideX=1,storeStrideY=1,storeStrideZ=1;

	MPI_Init (&nargs, &args);
	if (nargs>1)
            global_N_x = atoi(args[1]);
	if (nargs>2)
            global_N_y = atoi(args[2]);
	if(nargs>3)
	    global_N_z = atoi(args[3]);
	if (nargs>4)
            num_procs1 = atoi(args[4]);
	if (nargs>5)
            num_procs2 = atoi(args[5]);
	if(nargs>6)
	    num_procs3 = atoi(args[6]);
	
	if(nargs>7)
	    Ndyads = atoi(args[7]);
	if(nargs>8)
	    Unit_X = atoi(args[8]);
	if(nargs>9)
	    Unit_Y = atoi(args[9]);
	if(nargs>10)
	    Unit_Z = atoi(args[10]);

	if(nargs>11)
	    storeStrideX = atoi(args[11]);
	if(nargs>12)
	    storeStrideY = atoi(args[12]);
	if(nargs>13)
	    storeStrideZ = atoi(args[13]);
	if(nargs>14)
	    output_dir = args[14];

	if(Ndyads!=Unit_X*Unit_Y*Unit_Z)
	{
		Ndyads = 100;
		Unit_X = 100;
		Unit_Y = 1;
		Unit_Z = 1;
	}


        MPI_Comm comm=MPI_COMM_WORLD;
	partitioner = Partitioner_construct(comm,global_N_x,global_N_y,global_N_z,
				                        num_procs1,num_procs2,num_procs3);
	N_x = partitioner->sub_N[0];
	N_y = partitioner->sub_N[1];
	N_z = partitioner->sub_N[2];

	int offsetX,offsetY,offsetZ;
	offsetX = partitioner->my_offset[0];
	offsetY = partitioner->my_offset[1];
	offsetZ = partitioner->my_offset[2];

	int s_ix,s_iy,s_iz;
	int remain;
	int ds;
	remain = offsetX%storeStrideX;
	if(remain==0)
		s_ix=1;
	else
	{
		ds = storeStrideX - remain;
		s_ix = ds+1;
	}	
	remain = offsetY%storeStrideY;
	if(remain==0)
		s_iy=1;
	else
	{
		ds = storeStrideY - remain;
		s_iy = ds+1;
	}	
	remain = offsetZ%storeStrideZ;
	if(remain==0)
		s_iz=1;
	else
	{
		ds = storeStrideZ - remain;
		s_iz = ds+1;
	}	

	/*int out_put_rank;
	int x_axi_rank=-1;
	int rank_coord[3];
	int offset;
	offset = partitioner->my_offset[0]; 
	rank_coord[0] = partitioner->mpi_info.coord[0];
	rank_coord[1] = partitioner->mpi_info.coord[1];
	rank_coord[2] = partitioner->mpi_info.coord[2];
	if(offset<=x_axi&&x_axi<offset+N_x-2)
		x_axi_rank = rank_coord[0];
	if(x_axi_rank==rank_coord[0])
	{
		out_put_rank=rank_coord[1]*num_procs3+rank_coord[2];
		printf("out put rank  is :%d\n",out_put_rank);
	}*/
	
        	

	cell ***subcells;
	subcells = Allocate3DSubCells(N_x,N_y,N_z);

	v = allocate_3D_double(N_x,N_y,N_z);
	vtemp = allocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdt = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdt[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdtclock = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdtclock[0][0],0,N_x*N_y*N_z*sizeof(double));
	dtstep = allocate_3D_double(N_x,N_y,N_z);
	memset(dtstep[0][0],0,N_x*N_y*N_z*sizeof(double));
	flagArray = allocate_3D_double(N_x,N_y,N_z);
        memset(flagArray[0][0],0,N_x*N_y*N_z*sizeof(double));
	//flagArray = allocate_3D_int(N_x,N_y,N_z);
        //memset(flagArray[0][0],0,N_x*N_y*N_z*sizeof(int));

	for(int cx = 0; cx < N_x; cx++)
	    for(int cy =0; cy < N_y; cy++)
		for(int cz = 0;cz < N_z;cz++)
		    InitCellValue(subcells[cx][cy][cz],Unit_X,Unit_Y,Unit_Z,Nryr,Ndhpr);

	for(int cx = 1;cx<N_x-1;cx++)
	    for(int cy = 1;cy<N_y-1;cy++)
		for(int cz =1;cz<N_z-1;cz++)
		{
		      compute_cell_id(subcells[cx][cy][cz], cx,cy,cz, partitioner);//also initialize the data random seed
            	      v[cx][cy][cz] = -87.6;
	   	      //printf("cell id is : %d \n", subcells[cx][cy][cz].cellID);
		}
	//Files for output
	
	/*if(x_axi_rank==rank_coord[0])
	{
		if(output_dir)
			file << output_dir << "file_" << out_put_rank;
		else
			file << "file_" << out_put_rank;
	}*/

	if(output_dir)
	{
		file << output_dir << "file_" << partitioner->mpi_info.my_rank;
		flagName << output_dir << "flag_" << partitioner->mpi_info.my_rank;
		floadName << output_dir << "fload_" << partitioner->mpi_info.my_rank;
                
		file0 << output_dir << "cell";
	}
	else
	{
		file << "file_" << partitioner->mpi_info.my_rank;
		flagName << "flag_" << partitioner->mpi_info.my_rank;
		floadName << "fload_" << partitioner->mpi_info.my_rank;
		file0 << "cell";
	}
	fout.open(file.str().c_str());	
        flag.open(flagName.str().c_str());
        floadim.open(floadName.str().c_str());

        if (partitioner->mpi_info.my_rank == 0)
		f0out.open(file0.str().c_str());
	


	int offset_x = partitioner->my_offset[0];
	int offset_z = partitioner->my_offset[2];

	int total = (N_x-2)*(N_y-2)*(N_z-2);	
   
        double rt = (double)1/(double)total;
        printf("rt = %f\n",rt); 
	#pragma omp parallel
	{
		
            for (int icounter = 0; icounter<steps; icounter++)
            {

                #pragma omp for schedule(dynamic,1)
		        for(int I=0;I<total;I++)
		        {
		        	int ix = I/((N_y-2)*(N_z-2))+1;
			        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
			        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                //for (int ix = 1; ix<N_x-1; ix++)
                //    for (int iy = 1; iy<N_y-1; iy++)
                //        for(int iz = 1; iz<N_z-1;iz++)
                            if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy][iz])>0.2)||(dvdtclock[ix][iy][iz]<20) )
                            {
                                comp_cell(subcells[ix][iy][iz],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr,flagArray,rt);
                                dtstep[ix][iy][iz] = 0;
                            }
                }            
       /*         #pragma omp master
                {
                    if(1)
                    {
                        for (int ix = 1; ix<N_x-1; ix++)
                            for (int iy = 1; iy<N_y-1; iy++)
                            {
                                for(int iz = 1; iz<N_z-1;iz++)
                                {
                                    flag << flagArray[ix][iy][iz] << " ";
                                    flagArray[ix][iy][iz] = 0;
                                }
                                flag << endl;
                            }
                    }
	        	}*/

                //////////////////////////////////????????????????????????????????????????//////////////////////////	
                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);


        	    #pragma omp master
		        {
        	        UpdateGhostValue(v,partitioner);
		        }
        	    #pragma omp barrier
                        

                #pragma omp for
                for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {
                            dvdt[ix][iy][iz] = (v[ix][iy][iz] - vtemp[ix][iy][iz])/dt;
                            if (dvdt[ix][iy][iz]>1) 
                                dvdtclock[ix][iy][iz] = 0;
                            dtstep[ix][iy][iz] += dt;
                            dvdtclock[ix][iy][iz] += dt;
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }

                #pragma omp single
                {
                    if (t>=tstim && t<(tstim+dt))
                    {
                        stimtime = 0;
                        ibeat = ibeat+1;
                        tstim = tstim+bcl;
                        if (partitioner->mpi_info.my_rank==0)
                            cout<<"ibeat is :"<<ibeat<<endl;
                    }

                    if (stimtime>=0 && stimtime<=0.5)
                        ist = -80;
                    else
                        ist = 0;

                    //if (ibeat>beats-7) {ist = 0;}

                    if (icounter%1000 == 0) //  && partitioner->is_master_proc())
                    {
		                /*	if(x_axi_rank!=-1)
			            for(int iy = 1;iy<N_y-1;iy++)
			            {
                            for (int iz = 1; iz<N_z-1; iz++)
                                    fout<<v[x_axi-offset+1][iy][iz]<<"	";
                            fout<<endl<<endl;
                        }*/
			            for(int ix=s_ix;ix<N_x-1;ix+=storeStrideX)
				            for(int iy=s_iy;iy<N_y-1;iy+=storeStrideY)
				            {
					            for(int iz=s_iz;iz<N_z-1;iz+=storeStrideZ)
						            fout << v[ix][iy][iz] << " ";
					            fout << endl;
				            }		

			            time(&now);
			            ptr_ts = gmtime(&now);
		
                        if (partitioner->mpi_info.my_rank == 0)
                            //f0out<<t<<"	"<< ptr_ts->tm_hour<<":"<<ptr_ts->tm_min << " " << v[N_x/2][N_y/2][N_z/2]<<endl;
                            f0out<<t<<"  "<< v[N_x/2][N_y/2][N_z/2]<<endl;
  	                    if (partitioner->mpi_info.my_rank == 0)
                            //if (t>(beats-8)*bcl && t<=((beats-8)*bcl+4000))
                            printInfo(t,subcells[N_x/2][N_y/2][N_z/2],v[N_x/2][N_y/2][N_z/2],fcalcium,fdyad,fcurrents);
                        
                    }


                    t+=dt;
                    stimtime += dt;
                }//end single region
            }//end time loop
            
	} //end parallel region


	fout.close();
	
  	if (partitioner->mpi_info.my_rank == 0)
        {
		    f0out.close();
            fcalcium.close();
            fdyad.close();
            fcurrents.close();
        }
        //if (partitioner->mpi_info.my_rank == 0)
	//	VTKOutPut(num_procs1,num_procs2,num_procs3,
	//		  global_N_x/storeStrideX,global_N_y/storeStrideY,global_N_z/storeStrideZ,steps/1000);
        //if (partitioner->mpi_info.my_rank == 0)
	//	VTKOutPut(num_procs1,num_procs2,num_procs3,
	//		  global_N_x/storeStrideX,global_N_y/storeStrideY,global_N_z/storeStrideZ,steps/10);

        //GenerateLoadImbalanceFile(partitioner,steps/10,floadim);
       // flag.close();

        if (partitioner->mpi_info.my_rank == 0)
        {
          //  GenerateExecutedInfo(num_procs1,num_procs2,num_procs3,steps/10,fld);
           // fld.close();
        }



        deallocate_3D_double (v,N_x);
        deallocate_3D_double (vtemp,N_x);
        deallocate_3D_double (dvdt,N_x);
        deallocate_3D_double (dvdtclock,N_x);

        deallocate_3D_double (dtstep,N_x);
        for(int cx = 0; cx < N_x; cx++)
            for(int cy = 0;cy < N_y; cy++)
                for(int cz =0; cz < N_z;cz++)
                    releaseCell(subcells[cx][cy][cz]);
        deallocate3DSubCells(subcells,N_x);
        Partitioner_destruct(partitioner);
	MPI_Finalize ();
	printf("finished!\n");
	return 0;
}




