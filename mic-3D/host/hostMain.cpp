#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>

#include <omp.h>
#include <malloc.h>
#include <stdlib.h>
//#define __PAPI__
#ifdef __PAPI__
#include <papi.h>
#endif


#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"
#include "UpdateGhost.h"

#define global_N_x 100
#define global_N_y 100
#define global_N_z 100
#define Nryr  100
#define Ndhpr 15
#define Ndyads 100
#define Ntotdyads 10000
#define beats 1

//#define MPI_TIME


using namespace std;


double ***v, ***vtemp,***dvdt,***dvdtclock;
double ***dtstep;
double t,dt,d_x,Dx;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=1000;

ofstream fout;
//ofstream f1out;
ofstream f0out;
ostringstream file;
ostringstream file0;




int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	Dx = 0.2;

	tstim = 100;
	stimtime = 100;

	steps = beats*bcl/dt;
//	steps = 2;

	//***************************************************
	inter_Partitioner_t* inter_partitioner;
	int num_procs1=0, num_procs2=0,num_procs3=0;
	int N_x, N_y,N_z;
	char *output_dir=NULL; 

	MPI_Init (&nargs, &args);
	if (nargs>1)
            num_procs1 = atoi(args[1]);
	if (nargs>2)
            num_procs2 = atoi(args[2]);
	if(nargs>3)
	    num_procs3 = atoi(args[3]);
	if(nargs>4)
	    output_dir = args[4];

	double comp_cell_start, comp_cell_end, comp_cell_time, comp_cell_total=0;
	double diffusion_start, diffusion_end, diffusion_time, diffusion_total=0;
	double comm_start, comm_end, comm_time, comm_total=0;
	double main_start;
	#ifdef MPI_TIME
	#pragma omp master
	{
		MPI_Barrier(MPI_COMM_WORLD);
		main_start = MPI_Wtime();
		
	}
	#endif

//	#ifdef __PAPI__
        //#pragma omp master
  //    {
        float real_time, proc_time,mflops;
        long long flpins;
        int retval;
  //    }
  //    #endif
        MPI_Comm comm=MPI_COMM_WORLD;
	////////////////construct the node-level task partitioner///////////////////////////
	inter_partitioner = inter_Partitioner_construct(comm,global_N_x,global_N_y,global_N_z,
				                        num_procs1,num_procs2,num_procs3);
	N_x = partitioner->sub_N[0];
	N_y = partitioner->sub_N[1];
	N_z = partitioner->sub_N[2];

	cell ***subcells;
	subcells = Allocate3DSubCells(N_x,N_y,N_z);

	v = allocate_3D_double(N_x,N_y,N_z);
	vtemp = allocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdt = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdt[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdtclock = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdtclock[0][0],0,N_x*N_y*N_z*sizeof(double));
	dtstep = allocate_3D_double(N_x,N_y,N_z);
	memset(dtstep[0][0],0,N_x*N_y*N_z*sizeof(double));

	for(int cx = 0; cx < N_x; cx++)
	    for(int cy =0; cy < N_y; cy++)
		for(int cz = 0;cz < N_z;cz++)
		    InitCellValue(subcells[cx][cy][cz],Ndyads,1,1,Nryr);

	for(int cx = 1;cx<N_x-1;cx++)
	    for(int cy = 1;cy<N_y-1;cy++)
		for(int cz =1;cz<N_z-1;cz++)
		{
		      compute_cell_id(subcells[cx][cy][cz], cx,cy,cz, partitioner);//also initialize the data random seed
            	      v[cx][cy][cz] = -87.6;
	   	      //printf("cell id is : %d \n", subcells[cx][cy][cz].cellID);
		}
	//Files for output
	if(output_dir)
	{
		file<< output_dir << "file_" <<partitioner->mpi_info.my_rank;
		file0 << output_dir << "v_cell";
	}
	else
	{
		file<< "file_" <<partitioner->mpi_info.my_rank;
		file0 << "v_cell";
	}
	fout.open(file.str().c_str());	
	f0out.open(file0.str().c_str());
	
//	omp_set_num_threads(4);	
	#pragma omp parallel
	{
		//int num_omp_threads = omp_get_num_threads();
		//printf("the number of openmp threads is %d\n",num_omp_threads);
	}
	 #ifdef __PAPI__
        #pragma omp master
        {
                if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                        printf("PAPI_flops failed!\n");
        }
        #endif


	int offset_x = partitioner->my_offset[0];
	int offset_z = partitioner->my_offset[2];
	//////////////////////////////establish the connection with the device/////////////////

	
	//////////////////////get the device information:number of the device ///////////////////
	int deviceNum; 


	///////////////////////////intra partitioner construct///////////////
	int ndims = 3;
	double *workloadRatio;
	intra_Partitioner_t *intra_partitioner;
	intra_partitioner = intra_partitioner_construct(inter_partitioner,deviceNum,nidms,workloadRatio);


	///////////////////////// transfer data to devive///////////////

	//////////////////////////start to compute//////////////////////

	/////////////////////////
	
	#pragma omp parallel
	{
	
            for (int icounter = 0; icounter<steps; icounter++)
            {
                #ifdef MPI_TIME
                #pragma omp master
                {
                    MPI_Barrier(MPI_COMM_WORLD);
                    comp_cell_start = MPI_Wtime();
                }
                #endif


                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy][iz])>0.2)||(dvdtclock[ix][iy][iz]<20) )
                            {
                                comp_cell(subcells[ix][iy][iz],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr);
                                dtstep[ix][iy][iz] = 0;
                            }

                #ifdef MPI_TIME
                #pragma omp master
                {
                    comp_cell_end = MPI_Wtime() - comp_cell_start;
                    MPI_Allreduce(&comp_cell_end, &comp_cell_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
                    comp_cell_total += comp_cell_time;

                    MPI_Barrier(MPI_COMM_WORLD);
                    diffusion_start = MPI_Wtime();
                }
                #endif

                //////////////////////////////////????????????????????????????????????????//////////////////////////	
                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);

                #ifdef MPI_TIME
                #pragma omp master
                {
                    diffusion_end = MPI_Wtime() - diffusion_start;
                    MPI_Allreduce(&diffusion_end, &diffusion_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
                    diffusion_total += diffusion_time;

                    MPI_Barrier(MPI_COMM_WORLD);
                    comm_start = MPI_Wtime();
                }
                #endif

        	#pragma omp master
		{
        	    UpdateGhostValue(v,partitioner);
		}
        	#pragma omp barrier
                        
                #ifdef MPI_TIME
                #pragma omp master
                {
                    comm_end = MPI_Wtime() - comm_start;
                    MPI_Allreduce(&comm_end, &comm_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
                    comm_total += comm_time;
                }
                #endif

                #pragma omp for
                for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {
                            dvdt[ix][iy][iz] = (v[ix][iy][iz] - vtemp[ix][iy][iz])/dt;
                            if (dvdt[ix][iy][iz]>1) 
                                dvdtclock[ix][iy][iz] = 0;
                            dtstep[ix][iy][iz] += dt;
                            dvdtclock[ix][iy][iz] += dt;
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }

                #pragma omp single
                {
                    if (t>=tstim && t<(tstim+dt))
                    {
                        stimtime = 0;
                        ibeat = ibeat+1;
                        tstim = tstim+bcl;
                        if (partitioner->mpi_info.my_rank==0)
                            cout<<"ibeat is :"<<ibeat<<endl;
                    }

                    if (stimtime>=0 && stimtime<=0.5)
                        ist = -80;
                    else
                        ist = 0;

                    //if (ibeat>beats-7) {ist = 0; Dx=0;}

                    if (icounter%1000 == 0) //  && partitioner->is_master_proc())
                    {
/*                        for(int ix = 1;ix<N_x-1;ix++)
                            for (int iy = 1; iy<N_y-1; iy++)
                            {
                                for (int iz = 1; iz<N_z-1; iz++)
                                    fout<<v[ix][iy][iz]<<"	";
                                fout<<endl;
                            }
                            fout<<endl<<endl;
  */                      
                        if (partitioner->mpi_info.my_rank == 0)
                            f0out<<t<<"	"<<v[N_x/2][N_y/2][N_z/2]<<endl;
                        
                    }


                    t+=dt;
                    stimtime += dt;
                }//end single region
            }//end time loop
            
            #ifdef __PAPI__
            #pragma omp master
            {
                if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                    printf("PAPI_flops failed!\n");
                printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMPLOs:\t%f\n",real_time,proc_time,flpins,mflops);
                PAPI_shutdown();
            }
            #endif


	} //end parallel region

	#ifdef __PAPI__
        #pragma omp master
        {
                if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                    printf("PAPI_flops failed!\n");
                printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMPLOs:\t%f\n",real_time,proc_time,flpins,mflops);
                PAPI_shutdown();
        }
        #endif



        deallocate_3D_double (v,N_x);
        deallocate_3D_double (vtemp,N_x);
        deallocate_3D_double (dvdt,N_x);
        deallocate_3D_double (dvdtclock,N_x);

        deallocate_3D_double (dtstep,N_x);

        for(int cx = 0; cx < N_x; cx++)
            for(int cy = 0;cy < N_y; cy++)
                for(int cz =0; cz < N_z;cz++)
                    releaseCell(subcells[cx][cy][cz]);

        deallocate3DSubCells(subcells,N_x);
        Partitioner_destruct(partitioner);

	#ifdef MPI_TIME
        #pragma omp master
        {
            double main_end = MPI_Wtime() - main_start;
            double main_time;
            MPI_Allreduce(&main_end, &main_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
            cout << "############### main time is " << main_time << "################" << endl;
            cout << "############### comp_cell time is " << comp_cell_total << "################" << endl;
            cout << "############### diffusion time is " << diffusion_total << "################" << endl;
            cout << "############### communication time is " << comm_total << "################" << endl;
        }
	#endif

	MPI_Finalize ();
	printf("finished!\n");
	return 0;
}




