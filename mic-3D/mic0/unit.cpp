#include "unit.h"
#include <stdlib.h>

void initUnitValue(struct unit &Unit)
{
    int nryR = Unit.nryr;
    Unit.ryrstate = (int *)malloc(nryR*sizeof(int));
    Unit.cacyt = .72e-4;
    Unit.cass = 1e-4;
    //Unit.cads = 1e-4*1e3;//added by qianglan
    Unit.cads = (6.65e-5)*1e3;//added by qianglan  what is the acculate value???????
    Unit.cansr = 1.44;
    Unit.cajsr = 1.44;
    for(int j=0; j<nryR;j++)
        Unit.ryrstate[j] = 0;

    return;

}

void releaseUnit(struct unit &Unit)
{
    free(Unit.ryrstate);
    return;
}
