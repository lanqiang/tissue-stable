#ifndef CELL_H_INCLUDED
#define CELL_H_INCLUDED

#include "randomGenerate.h"
#include "RectPart2D.h"
#include "unit.h"

struct cell
{
    int cellID;
    struct RandData *pRD;
    struct unit *unitsInCell;

    int unitNum_x;
    int unitNum_y;
    int unitNum_z;

    double m,hf,hs,hsp,j,jp;
    double ml,hl,hlp;
    double nai,ki,nass,kss;

    double ap,a,iF,iS,iFp,iSp;
    double xs1,xs2,xrs,xrf,xk1;
    double d,ff,fcaf,fs,fcas,jca,ffp,fcafp;

   // double CaMKa_cell;//added by qianglan
    double nca_print;//added by qianglan
   // double inacass_dyad;//added by qianglan

    double *nca;
    double *CaMKt;
    //double *CaMKa;//added by qianglan
    double *trel;
    double *Poryr;

    int *nactive;
    //int randcallcount;

    long timestep;
    long totalCellSize;
};


struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z);
void deallocate3DSubCells(struct cell ***SubCells,int Nx);

void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z, int nryr);
void releaseCell(struct cell &Cell);

//void generateSeed(struct cell&Cell, int thread_id,int process_id);
void compute_cell_id(struct cell & Cell, int cx, int cy, int cz, Partitioner_t* partitioner);



#endif // CELL_H_INCLUDED
