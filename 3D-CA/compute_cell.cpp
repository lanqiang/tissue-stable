#include "compute_cell.h"
#include "cell.h"

#include "mkl_vsl.h"
#include <cmath>
#include <cstring>
#include <stdlib.h>

#include <iostream>
using namespace std;
void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z,double t, double ist, int bcl, double ***v,double ***dtstep,int Ndhpr , int Ntotdyads,int Nryr)
{

int Nx = 100; int Ny=1; int Nz=1;
int Nvox = 1;
int Ndyads = Cell.unitNum_x;
double nao = 140;
double ko = 5.4;
double cao = 1.8;
double iso = 0;
double gna = 75;
double mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs;
double mss,hss,jss,hssp;
double h,hp,finap;
double CaMKa_cell=0.1;//
double mlss,mltau,hlss,hlssp,hltau,hlptau;

double gnal,finalp;
double naiont=0,caiont,kiont=0,it,istim;
double ina,inal,INab,ilca,icab,icat,ipca,ilcana,INaK,inaca,inacass,Ito,IKr,IKs,IK1,IKb,ilcak;
double CaMKb_cell,CaMKt_cell;
double nsr, jsr, cadyad=0.1, casub,cai;

double dx = 0.2;
double dy = 0.2;
double dz = 0.2;

double R = 8314;
double temp = 310;
double frdy = 96485;
double ena;

double casstemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cansrtemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cacyttemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cassdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cansrdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cacytdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];

int Nx_diff = (Nx-1)*Nvox + 1;
int Ny_diff = (Ny-1)*Nvox + 1;
int Nz_diff = (Nz-1)*Nvox + 1;

//diffusion coefficients
double Dsr = 0.02;
double Dcyt = 0.2;

//buffers
double kmcsqn = 1;
double csqnbar = 10;
double bsrbar = 0.047;
double bslbar = 1.124;
double kmbsr = 0.00087;
double kmbsl = 0.0087;
double trpnbar =  0.07;
double kmtrpn = 0.0005;
double cmdnbar = 0.05;
double kmcmdn = 0.00238;

//camkinase
double aCaMK = 0.05;
double bCaMK = 0.00068;
double CaMKo = 0.05;
double KmCaM = 0.0015;
double KmCaMK = 0.15;
double CaMKa=0.1;//
double CaMKb;

double zna = 1;
double zca = 2;

double dt = dtstep[ix][iy][iz];


//gather from 2
double EK;
EK=(R*temp/frdy)*log(ko/Cell.ki);//move above because of EK
double km2n;
double anca;
double Kmn = 0.002;//move from 9
double k2n = 1000.0;//move from 9
double PhiCaL;
double PhiCaNa;
double PhiCaK;//move from 9
double PCa=0.0001;
if (iso == 1) PCa = 2.5*PCa;
double PCap=1.1*PCa;
double PCaNa=0.00125*PCa;
double PCaK=3.574e-4*PCa;
double PCaNap=0.00125*PCap;
double PCaKp=3.574e-4*PCap;//move from 9

double icab_dyad=0,ipca_dyad=0,inaca_dyad=0;//move from 8.3




//############################3.comp_ikr()#########################//
double xrss=1.0/(1.0+exp((-(v[ix][iy][iz]+8.337))/6.789));
double txrf=12.98+1.0/(0.3652*exp((v[ix][iy][iz]-31.66)/3.869)+4.123e-5*exp((-(v[ix][iy][iz]-47.78))/20.38));
double txrs=1.865+1.0/(0.06629*exp((v[ix][iy][iz]-34.70)/7.355)+1.128e-5*exp((-(v[ix][iy][iz]-29.74))/25.94));
double Axrf=1.0/(1.0+exp((v[ix][iy][iz]+54.81)/38.21));
double Axrs=1.0-Axrf;
Cell.xrf=xrss-(xrss-Cell.xrf)*exp(-dt/txrf);
Cell.xrs=xrss-(xrss-Cell.xrs)*exp(-dt/txrs);
double xr=Axrf*Cell.xrf+Axrs*Cell.xrs;
double rkr=1.0/(1.0+exp((v[ix][iy][iz]+55.0)/75.0))*1.0/(1.0+exp((v[ix][iy][iz]-10.0)/30.0));
double GKr=0.046; GKr = 1*GKr;

IKr=GKr*sqrt(ko/5.4)*xr*rkr*(v[ix][iy][iz]-EK);
//###############################3.comp_ikr()#######################//



//#########################4.comp_iks()##########################//
double EKs=(R*temp/frdy)*log((ko+0.01833*nao)/(Cell.ki+0.01833*Cell.nai));
double xs1ss=1.0/(1.0+exp((-(v[ix][iy][iz]+11.60))/8.932));
double txs1=817.3+1.0/(2.326e-4*exp((v[ix][iy][iz]+48.28)/17.80)+0.001292*exp((-(v[ix][iy][iz]+210.0))/230.0));
Cell.xs1=xs1ss-(xs1ss-Cell.xs1)*exp(-dt/txs1);
double xs2ss=xs1ss;
double txs2=1.0/(0.01*exp((v[ix][iy][iz]-50.0)/20.0)+0.0193*exp((-(v[ix][iy][iz]+66.54))/31.0));
Cell.xs2=xs2ss-(xs2ss-Cell.xs2)*exp(-dt/txs2);
double KsCa=1.0+0.6/(1.0+pow(3.8e-5/3.8e-5,1.4));
double GKs=0.0034;
if (iso == 1) GKs = 3.2*GKs;

IKs=GKs*KsCa*Cell.xs1*Cell.xs2*(v[ix][iy][iz]-EKs);
//#########################4.comp_iks()##########################//


//###############################5.comp_ik1()#######################//
double xk1ss=1.0/(1.0+exp(-(v[ix][iy][iz]+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
double txk1=122.2/(exp((-(v[ix][iy][iz]+127.2))/20.36)+exp((v[ix][iy][iz]+236.8)/69.33));
Cell.xk1=xk1ss-(xk1ss-Cell.xk1)*exp(-dt/txk1);
double rk1=1.0/(1.0+exp((v[ix][iy][iz]+105.8-2.6*ko)/9.493));
double GK1=0.1908;
IK1=GK1*sqrt(ko)*rk1*Cell.xk1*(v[ix][iy][iz]-EK);
//###############################5.comp_ik1()#######################//



//################################6.comp_inak()####################//
double k1p = 949.5;
double k1m = 182.4;
double k2p = 687.2;
double k2m = 39.4;
double k3p = 1899.0;
double k3m = 79300.0;
double k4p=639.0;
double k4m = 40.0;
double Knai0 = 9.073;
double Knao0 = 27.78;
double delta = -0.1550;
double Knai=Knai0*exp((delta*v[ix][iy][iz]*frdy)/(3.0*R*temp));
if (iso == 1) Knai = 0.7*Knai;
double Knao=Knao0*exp(((1.0-delta)*v[ix][iy][iz]*frdy)/(3.0*R*temp));
double Kki = 0.5;
double Kko = 0.3582;
double MgADP = 0.05;
double MgATP = 9.8;
double Kmgatp = 1.698e-7;
double H=1.0e-7;
double eP = 4.2;
double Khp = 1.698e-7;
double Knap = 224.0;
double Kxkur = 292.0;
double P=eP/(1.0+H/Khp+Cell.nai/Knap+Cell.ki/Kxkur);
double a1=(k1p*pow(Cell.nai/Knai,3.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
double b1=k1m*MgADP;
double a2=k2p;
double b2=(k2m*pow(nao/Knao,3.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double a3=(k3p*pow(ko/Kko,2.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double b3=(k3m*P*H)/(1.0+MgATP/Kmgatp);
double a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
double b4=(k4m*pow(Cell.ki/Kki,2.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
double x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
double x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
double x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
double x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
double E1=x1/(x1+x2+x3+x4);
double E2=x2/(x1+x2+x3+x4);
double E3=x3/(x1+x2+x3+x4);
double E4=x4/(x1+x2+x3+x4);
double zk=1.0;
double JnakNa=3.0*(E1*a3-E2*b3);
double JnakK=2.0*(E4*b1-E3*a1);
double Pnak=30;

INaK=Pnak*(zna*JnakNa+zk*JnakK);
//################################6.comp_inak()####################//


//#################################7.comp_inab()######################//
double xkb=1.0/(1.0+exp(-(v[ix][iy][iz]-14.48)/18.34));
double GKb=0.003;
if (iso == 1)
	 GKb = 2.5*GKb;

IKb=GKb*xkb*(v[ix][iy][iz]-EK);

double PNab=3.75e-10;
double vffrt = v[ix][iy][iz]*frdy*frdy/(R*temp);
double vfrt = v[ix][iy][iz]*frdy/(R*temp);
INab=PNab*vffrt*(Cell.nai*exp(vfrt)-nao)/(exp(vfrt)-1.0);
//#################################7.comp_inab()######################//



//double zna = 1;
//double zca = 2;moved to above





double ar = 0.0011;
double	al = 0.01;
double	pi = 3.142;
double	ageo = 2*pi*ar*ar + 2*pi*ar*al;
double	acap = 2*ageo;
double vcell = 1000*pi*ar*ar*al;
double	vmyo = 0.68*vcell;
double vds = 2e-13;

double	vnsr = 0.0552*vcell;
double	vjsr = 0.0048*vcell;
double	vss = 0.01*vcell;
double	vjsr_dyad = vjsr/Ntotdyads;
double	vnsr_dyad = vnsr/Ntotdyads;
double	vcyt_dyad = vmyo/Ntotdyads;
double	vss_dyad = vss/Ntotdyads;
double	vol_ds = 2e-13;

double nodhpr,jrel,noryr,nmodeca_total,nivf_total,nivs_total;
double factivedyad;


//################################### part of 9.comp_ical()##############################//
double fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58)/3.696));
if (iso == 1) fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58+8)/3.696));

double tff=7.0+1.0/(0.0045*exp(-(v[ix][iy][iz]+20.0)/10.0)+0.0045*exp((v[ix][iy][iz]+20.0)/10.0));
double tfs=1000.0+1.0/(0.000035*exp(-(v[ix][iy][iz]+5.0)/4.0)+0.000035*exp((v[ix][iy][iz]+5.0)/6.0));
double Aff=0.6;
double Afs=1.0-Aff;
Cell.ff=fss-(fss-Cell.ff)*exp(-dt/tff);
Cell.fs=fss-(fss-Cell.fs)*exp(-dt/tfs);
double f=Aff*Cell.ff+Afs*Cell.fs;
double fcass=fss;
double tfcaf=7.0+1.0/(0.04*exp(-(v[ix][iy][iz]-4.0)/7.0)+0.04*exp((v[ix][iy][iz]-4.0)/7.0));
double tfcas=100.0+1.0/(0.00012*exp(-v[ix][iy][iz]/3.0)+0.00012*exp(v[ix][iy][iz]/7.0));
double Afcaf=0.3+0.6/(1.0+exp((v[ix][iy][iz]-10.0)/10.0));
double Afcas=1.0-Afcaf;
Cell.fcaf=fcass-(fcass-Cell.fcaf)*exp(-dt/tfcaf);
Cell.fcas=fcass-(fcass-Cell.fcas)*exp(-dt/tfcas);
double fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
double tjca = 75.0;
Cell.jca=fcass-(fcass-Cell.jca)*exp(-dt/tjca);
double tffp=2.5*tff;
Cell.ffp=fss-(fss-Cell.ffp)*exp(-dt/tffp);
double fp=Aff*Cell.ffp+Afs*Cell.fs;
double tfcafp=2.5*tfcaf;
Cell.fcafp=fcass-(fcass-Cell.fcafp)*exp(-dt/tfcafp);
double fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;
//###################################part of 9.comp_ical()##############################//



//############################8.comp_calc_dyad()#####################################///
ilca = nodhpr = jrel = noryr = nmodeca_total = nivf_total = nivs_total = 0;
ilcana=ilcak=0;
inaca = icab = ipca = icat = inacass = 0;
cai = casub = cadyad = jsr = nsr = 0;
CaMKa_cell = CaMKb_cell = CaMKt_cell = 0;
factivedyad  = 0;


//added by qianglan to generate random data using intel MKL lib
long randomseed;
randomseed = Cell.cellID + Cell.timestep*Cell.totalCellSize;
Cell.timestep+=1;
//__attribute__((align(64))) double * l_Random;
double * l_Random;
unsigned long rand_n = 3*Ndyads*Ndhpr+Nryr;
l_Random = (double *)malloc(rand_n*sizeof(double));
VSLStreamStatePtr Randomstream;
vslNewStream(&Randomstream, VSL_BRNG_MCG31, randomseed);

vdRngUniform(0, Randomstream, rand_n, l_Random, 0.0, 1.0-(1.2e-12));

/*double l_Random;
long rand_n = 1;
VSLStreamStatePtr Randomstream;
vslNewStream(&Randomstream, VSL_BRNG_MCG31, randomseed);
vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));*/

/*if(Cell.cellID==1)
{
	for(int i=0;i<rand_n;i++)
	{
		cout <<l_Random[i]<<  "\t"  << randomseed << endl;
		//cout << Cell.pRD->myrandomseed << "\t" << Cell.pRD->randomDataGenerate() << endl;
		//l_Random[i] = 0;
	}
}*/



for (int i=0; i<Ndyads;i++)
{
////%%%%%%8.1 comp_ical_dyad()%%%%%%%%%%%//
km2n=Cell.jca*1.0;
anca=1.0/(k2n/km2n+pow(1.0+Kmn/(0.001*Cell.unitsInCell[i].cads),4.0));
Cell.nca[i]=anca*k2n/km2n-(anca*k2n/km2n-Cell.nca[i])*exp(-km2n*dt);
int nLCC_dyad_np = 0;
int nLCC_dyad_p = 0;

CaMKb = CaMKo*(1.0-Cell.CaMKt[i] )/(1.0+KmCaM/Cell.unitsInCell[i].cass);
CaMKa = CaMKb + Cell.CaMKt[i];
Cell.CaMKt[i] += dt*(aCaMK*CaMKb*(CaMKb+Cell.CaMKt[i])-bCaMK*Cell.CaMKt[i]);//move here 



for (int k = 0;k<Ndhpr;k++)
{
	//if ( Cell.pRD->randomDataGenerate() > (1.0/(1.0+KmCaMK/CaMKa)) ) //Channel is not CaMK phosphorylated
	if(l_Random[i*Ndhpr*3+k*3+0]>(1.0/(1.0+KmCaMK/CaMKa)))
	//if(l_Random > (1.0/(1.0+KmCaMK/CaMKa)))
	{
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
           //if ( Cell.pRD->randomDataGenerate() > Cell.nca[i] ) //Channel is not in Ca-CaMK dependent state
	   if ( l_Random[i*Ndhpr*3+k*3+1] > Cell.nca[i] )
	   //if(l_Random > Cell.nca[i])
           {
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
              //if (Cell.pRD->randomDataGenerate() <Cell.d*f ) //Channel is Open
	      if (l_Random[i*Ndhpr*3+k*3+2] <Cell.d*f )
	      //if(l_Random <Cell.d*f)
              {
                 nLCC_dyad_np ++;
              }
           }
           else                                    //Channel is in Ca-CaMK dependent state
           {
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
                //if (Cell.pRD->randomDataGenerate() < Cell.d*Cell.jca*fca)
		if (l_Random[i*Ndhpr*3+k*3+2] < Cell.d*Cell.jca*fca)
	     	//if(l_Random < Cell.d*Cell.jca*fca)
              {
                 nLCC_dyad_np ++;
              }
           }
        }
        else                                          //Channel is CaMK phosphorylated
        {
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
           //if ( Cell.pRD->randomDataGenerate() > Cell.nca[i] ) //Channel is not in Ca-CaMK dependent state
	    if ( l_Random[i*Ndhpr*3+k*3+1] > Cell.nca[i] )
	   //if(l_Random > Cell.nca[i])
           {
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
              //if (Cell.pRD->randomDataGenerate() <Cell.d*fp ) //Channel is Open
	      if (l_Random[i*Ndhpr*3+k*3+2] <Cell.d*fp )
		//if(l_Random <Cell.d*fp)
              {
                 nLCC_dyad_p ++;
              }
           }
           else                                    //Channel is in Ca-CaMK dependent state
           {
		//vdRngUniform(0, Randomstream, rand_n, &l_Random, 0.0, 1.0-(1.2e-12));
              //if (Cell.pRD->randomDataGenerate() < Cell.d*Cell.jca*fcap)
	      if (l_Random[i*Ndhpr*3+k*3+2] < Cell.d*Cell.jca*fcap)
		//if(l_Random < Cell.d*Cell.jca*fcap)
              {
                 nLCC_dyad_p ++;
              }
           }
        }

}

//	if ((ix == N_x/2) && (iy == N_y/2))
//		f2out<<t<<"	"<<(nLCC_dyad_p + nLCC_dyad_np)<<endl;

PhiCaL=4.0*vffrt*(1e-3*Cell.unitsInCell[i].cads*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);
PCa=0.0001;
if (iso == 1) PCa = 2.5*PCa;
PCap=1.1*PCa;
PCaNa=0.00125*PCa;
PCaK=3.574e-4*PCa;
PCaNap=0.00125*PCap;
PCaKp=3.574e-4*PCap;

double ilca_dyad = PCa*PhiCaL*(nLCC_dyad_np*1.0/Ndhpr) + PCap*PhiCaL*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF
double icana_dyad = PCaNa*PhiCaNa*(nLCC_dyad_np*1.0/Ndhpr) + PCaNap*PhiCaNa*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF
double icak_dyad = PCaK*PhiCaK*(nLCC_dyad_np*1.0/Ndhpr) + PCaKp*PhiCaK*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF

ilca += ilca_dyad/Ndyads;	//uA/uF
ilcana += icana_dyad/Ndyads;
ilcak += icak_dyad/Ndyads;
nodhpr +=  nLCC_dyad_np + nLCC_dyad_p;

double jlca_dyad = -acap*ilca_dyad*1e3/(2*vds*frdy*Ntotdyads);	//uM/ms
////%%%%%%8.1 comp_ical_dyad()%%%%%%%%%%%//


//////////%%%%%%%8.2 comp_irel_dyad()%%%%%%%%%%%//////
double Km = 5;
double Kmcsqn = 150;
double dryr = 4;
int nryropen = 0;
Cell.nactive[i] = 0;

for (int k=0; k<Nryr; k++)
{
	if(((Cell.unitsInCell[i]).ryrstate[k] == 1) || ((Cell.unitsInCell[i]).ryrstate[k] == 3)) nryropen++;
}

if (  (1.0*nryropen/Nryr)>0.2) {Cell.trel[i] = t; Cell.nactive[i] = 1;}

double  kC1O1 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Km,4)));
double	kO1C1 = 0.5;
double	kC2O2 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Kmcsqn,4)));
double	kO2C2 = kO1C1;

//calsequestrin
double	csqn = csqnbar*pow(kmcsqn,8)/(pow(Cell.unitsInCell[i].cajsr,8) + pow(kmcsqn,8) );
double	csqnca = csqnbar - csqn;

double kC1C2 = 1*(csqn/csqnbar);

double tref;

tref = 0.9*bcl;
double kC2C1 = 1*(csqnca/csqnbar)*(t-Cell.trel[i]-tref);

double	kO1O2 = kC1C2;
double	kO2O1 = kC2C1;

int nryrc1,nryro1,nryrc2,nryro2;
nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;




for ( int k=0;k<Nryr;k++)
{
	//double randomnumber = Cell.pRD->randomDataGenerate();
	double randomnumber = l_Random[3*Ndyads*Ndhpr+k];
	//double randomnumber;
	//vdRngUniform(0, Randomstream, rand_n, &randomnumber, 0.0, 1.0-(1.2e-12));
	switch ((Cell.unitsInCell[i]).ryrstate[k])
	{
		case 0:
		if (randomnumber <= kC1O1*dt)
		{(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}
		if ((randomnumber>kC1O1*dt) && (randomnumber<= (kC1O1+kC1C2)*dt))
		{(Cell.unitsInCell[i]).ryrstate[k]=2; nryrc2++;break;}
		if ((randomnumber> (kC1O1+kC1C2)*dt) && (randomnumber <= 1))
		{(Cell.unitsInCell[i]).ryrstate[k]=0; nryrc1++;break;}

		case 1:
		if (randomnumber <= kO1C1*dt)
		{(Cell.unitsInCell[i]).ryrstate[k] = 0; nryrc1++; break;}
		if ((randomnumber>kO1C1*dt) && (randomnumber<=(kO1C1+kO1O2)*dt))
		{(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
		if ((randomnumber>(kO1C1+kO1O2)*dt) && (randomnumber<=1))
		{(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}

		case 2:
		if (randomnumber <= kC2C1*dt)
		{(Cell.unitsInCell[i]).ryrstate[k] = 0; nryrc1++; break;}
		if ((randomnumber>kC2C1*dt) && (randomnumber<=(kC2C1+kC2O2)*dt))
		{(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
		if ((randomnumber>(kC2C1+kC2O2)*dt) && (randomnumber<=1))
		{(Cell.unitsInCell[i]).ryrstate[k] = 2; nryrc2++; break;}

		case 3:
		if (randomnumber <= kO2C2*dt)
		{(Cell.unitsInCell[i]).ryrstate[k] = 2; nryrc2++; break;}
		if ((randomnumber>kO2C2*dt) && (randomnumber<=(kO2C2+kO2O1)*dt))
		{(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}
		if ((randomnumber>(kO2C2+kO2O1)*dt) && (randomnumber<=1))
		{(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
	}
}

//	nryropen = nryro1 + nryro2;
nryropen = nryro1;

//if (t> (beats-3)*bcl && t<(beats-3)*bcl+20) nryropen = Nryr;

//if (t>(beats-5)*bcl) Dx = 0;




Cell.Poryr[i] =  1.0*nryropen/Nryr;

double jrel_dyad = dryr*nryropen*(Cell.unitsInCell[i].cajsr*1e3-Cell.unitsInCell[i].cads); //uM/ms
jrel += jrel_dyad*(vds/vmyo)*(Ntotdyads/Ndyads); //mM/s

noryr += nryropen;
//////////////////////////%%%%%%%8.2 comp_irel_dyad()%%%%%%%%%%%///////////////////////



//////////%%%%%%%8.3 comp_conc_dyad()%%%%%%%%%%%//////
double tauefflux = 7e-4;
double jcadiff_ds_ss = (Cell.unitsInCell[i].cads-Cell.unitsInCell[i].cass*1e3)/tauefflux;
double jefflux = jcadiff_ds_ss;

Cell.unitsInCell[i].cads =  (jrel_dyad + jlca_dyad + Cell.unitsInCell[i].cass*1e3/tauefflux)*tauefflux;

double	taudiff  = 0.1;
double	bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+Cell.unitsInCell[i].cass),2))) + ((bslbar*kmbsl)/(pow((kmbsl+Cell.unitsInCell[i].cass),2))));
double jcadiff_ss_cyt = (Cell.unitsInCell[i].cass-Cell.unitsInCell[i].cacyt)/taudiff;

// local submembrane space concentration
double inacass_dyad=0;

/////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////
// calcium currents
double kna1 = 15.0;
double kna2 = 5.0;
double kna3 = 88.12;
double kasymm = 12.5;
double wna = 6.0e4;
double wca = 6.0e4;
double wnaca = 5.0e3;
double kcaon = 1.5e6;
double kcaoff = 5.0e3;
double qna = 0.5224;
double qca = 0.1670;
double hca=exp((qca*v[ix][iy][iz]*frdy)/(R*temp));
double hna=exp((qna*v[ix][iy][iz]*frdy)/(R*temp));
double h1=1+Cell.nai/kna3*(1+hna);
double h2=(Cell.nai*hna)/(kna3*h1);
double h3=1.0/h1;
double h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
double h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
double h6=1.0/h4;
double h7=1.0+nao/kna3*(1.0+1.0/hna);
double h8=nao/(kna3*hna*h7);
double h9=1.0/h7;
double h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
double h11=nao*nao/(h10*kna1*kna2);
double h12=1.0/h10;
double k1=h12*cao*kcaon;
double k2=kcaoff;
double k3p=h9*wca;
double k3pp=h8*wnaca;
double k3=k3p+k3pp;
double k4p=h3*wca/hca;
double k4pp=h2*wnaca;
double k4=k4p+k4pp;
double k5=kcaoff;
double k6=h6*Cell.unitsInCell[i].cacyt*kcaon;
double k7=h5*h2*wna;
double k8=h8*h11*wna;
double x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
double x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
double x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
double x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
double E1=x1/(x1+x2+x3+x4);
double E2=x2/(x1+x2+x3+x4);
double E3=x3/(x1+x2+x3+x4);
double E4=x4/(x1+x2+x3+x4);
double KmCaAct = 150.0e-6;
double allo=1.0/(1.0+pow(KmCaAct/Cell.unitsInCell[i].cacyt,2.0));
double zna=1.0;
double JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
double JncxCa=E2*k2-E1*k1;
double Gncx=0.0008;

inaca_dyad=0.8*Gncx*allo*(zna*JncxNa+zca*JncxCa);

h1=1+Cell.nai/kna3*(1+hna);
h2=(Cell.nai*hna)/(kna3*h1);
h3=1.0/h1;
h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
h6=1.0/h4;
h7=1.0+nao/kna3*(1.0+1.0/hna);
h8=nao/(kna3*hna*h7);
h9=1.0/h7;
h10=kasymm+1.0+nao/kna1*(1+nao/kna2);
h11=nao*nao/(h10*kna1*kna2);
h12=1.0/h10;
k1=h12*cao*kcaon;
k2=kcaoff;
k3p=h9*wca;
k3pp=h8*wnaca;
k3=k3p+k3pp;
k4p=h3*wca/hca;
k4pp=h2*wnaca;
k4=k4p+k4pp;
k5=kcaoff;
k6=h6*Cell.unitsInCell[i].cass*kcaon;
k7=h5*h2*wna;
k8=h8*h11*wna;
x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
E1=x1/(x1+x2+x3+x4);
E2=x2/(x1+x2+x3+x4);
E3=x3/(x1+x2+x3+x4);
E4=x4/(x1+x2+x3+x4);
allo=1.0/(1.0+pow(KmCaAct/Cell.unitsInCell[i].cass,2.0));
JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
JncxCa=E2*k2-E1*k1;

inacass_dyad=0.2*Gncx*allo*(zna*JncxNa+zca*JncxCa);

double PCab = 2.5e-8;
icab_dyad=PCab*4.0*vffrt*(Cell.unitsInCell[i].cacyt*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
double GpCa = 0.0005;
ipca_dyad=GpCa*Cell.unitsInCell[i].cacyt/(0.0005+Cell.unitsInCell[i].cacyt);

inaca += (inaca_dyad)/Ndyads;
inacass += (inacass_dyad)/Ndyads;
ipca += (ipca_dyad)/Ndyads;
icab += (icab_dyad)/Ndyads;
/////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////



Cell.unitsInCell[i].cass = Cell.unitsInCell[i].cass + (-bsubspace*(((-2*inacass_dyad)*(acap/(Ntotdyads*zca*frdy*vss_dyad))) + jcadiff_ss_cyt - jcadiff_ds_ss*1e-3*(vds/vss_dyad) ))*dt;
double taurefill = 20;

double jrefill = (Cell.unitsInCell[i].cansr - Cell.unitsInCell[i].cajsr)/taurefill;

double	bjsr = 1/( 1 + (csqnbar*kmcsqn)/pow((kmcsqn + Cell.unitsInCell[i].cajsr),2));

Cell.unitsInCell[i].cajsr = Cell.unitsInCell[i].cajsr + bjsr*(-(jrel_dyad*(vds/vjsr_dyad))/1000. + jrefill)*dt;
double Jupnp=0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.00092);
double Jupp=2.75*0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.00092-0.00017);
if (iso == 1)
{
	Jupnp=0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.54*0.00092);
    	Jupp=2.75*0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.54*(0.00092-0.00017));
}

double fJupp=(1.0/(1.0+KmCaMK/CaMKa));
double ileak=0.0039375*Cell.unitsInCell[i].cansr/15.0;
double iup=((1.0-fJupp)*Jupnp+fJupp*Jupp);

Cell.unitsInCell[i].cansr = Cell.unitsInCell[i].cansr + (iup - ileak - jrefill*(vjsr_dyad/vnsr_dyad))*dt;

// local CaMK bound, active and trapped concentration
/*CaMKb = CaMKo*(1.0-Cell.CaMKt[i] )/(1.0+KmCaM/Cell.unitsInCell[i].cass);
CaMKa = CaMKb + Cell.CaMKt[i];
Cell.CaMKt[i] += dt*(aCaMK*CaMKb*(CaMKb+Cell.CaMKt[i])-bCaMK*Cell.CaMKt[i]);*///move 

//double icab_dyad=0,ipca_dyad=0,inaca_dyad=0;//move to top





double bmyo;

bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+Cell.unitsInCell[i].cacyt),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+Cell.unitsInCell[i].cacyt),2))));

// local myoplasmic Ca concentration
Cell.unitsInCell[i].cacyt = Cell.unitsInCell[i].cacyt + (-bmyo*(((icab_dyad+ipca_dyad-2*inaca_dyad)*(acap/(Ntotdyads*zca*frdy*vcyt_dyad ))) + (iup-ileak)*(vnsr_dyad/vcyt_dyad) - jcadiff_ss_cyt*(vss_dyad/vcyt_dyad) ))*dt;
//////////%%%%%%%8.3 comp_conc_dyad()%%%%%%%%%%%//////



}

free(l_Random);

/////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////
for (int i = 0; i<Ndyads; i++) //xfer ca from reaction to diffusion space
{
	#ifdef DEBUG
		printf("inner %d th loop start!\n",i);
	#endif
	int iix,iiy,iiz;
	iiz= i/(Nx*Ny);
	iiy = (i - iiz*(Nx*Ny))/Nx;
	iix = i -iiz*(Nx*Ny) - iiy*Ny;

	#ifdef DEBUG

	printf("iz = %d \n iy = %d \n  ix = %d \n i = %d \n",iz,iy,ix,i);
	#endif

	cacytdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cacyt;
  	cansrdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cansr;
	cassdiff[ (Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cass;
}


for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
{
	// temp variables to compute ca in diffusion space
	cacyttemp[i] = cacytdiff[i];
	cansrtemp[i] = cansrdiff[i];
	casstemp[i] = cassdiff[i];

	int iz,iy,ix;

	iz= i/(Nx_diff*Ny_diff);    			//3D mapping
	iy = (i - iz*(Nx_diff*Ny_diff))/Nx_diff; 		//3D mapping
	ix = i -iz*(Nx_diff*Ny_diff) - iy*Ny_diff;	//3D mapping

	double bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+cassdiff[i]),2))) + ((bslbar*kmbsl)/(pow((kmbsl+cassdiff[i]),2))));
	double bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+cacytdiff[i]),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+cacytdiff[i]),2))));

	if (ix>0 && ix<Nx_diff-1)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-1] + cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-1] + cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-1] + cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
	}

	if (iy>0 && iy<Ny_diff-1)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-Nx_diff] + cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-Nx_diff] + cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff] + cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}

	if (iz>0 && iz<Nz_diff-1)
	{
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*(cacytdiff[i-Nx_diff*Ny_diff] + cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*(cassdiff[i-Nx_diff*Ny_diff] + cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff*Ny_diff] + cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}

	if (ix==0 && ix!=(Nx_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
	}
	if (ix == Nx_diff-1 && ix != 0)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-1] -2*cansrdiff[i])*dt/(dx*dx);
	}
	if (iy == 0 && iy != (Ny_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}
	if (iy == Ny_diff-1 && iy != 0)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}

	if (iz == 0 && iz != (Nz_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}
	if (iz == Nz_diff-1 && iz != 0)
	{
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}

	}

	for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
	{
		cacytdiff[i] = cacyttemp[i]; cassdiff[i] = casstemp[i]; cansrdiff[i] = cansrtemp[i];
	}


	for (int i = 0; i<Ndyads; i++) //xfer ca from diffusion to reaction space
	{
		int iix,iiy,iiz;
	    	iiz= i/(Nx*Ny);
	     	iiy = (i - iiz*(Nx*Ny))/Nx;
	     	iix = i -iiz*(Nx*Ny) - iiy*Ny;

		Cell.unitsInCell[i].cacyt = cacytdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
  		Cell.unitsInCell[i].cansr = cansrdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
		Cell.unitsInCell[i].cass = cassdiff[ (Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
	}
/////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////


for (int i=0; i<Ndyads; i++)
{
	cai += Cell.unitsInCell[i].cacyt/Ndyads;
	casub += Cell.unitsInCell[i].cass/Ndyads;
	cadyad += Cell.unitsInCell[i].cads/Ndyads;
	jsr += Cell.unitsInCell[i].cajsr/Ndyads;
	nsr += Cell.unitsInCell[i].cansr/Ndyads;
	factivedyad += Cell.nactive[i]/Ndyads;
	CaMKa_cell += CaMKa/Ndyads;
       CaMKb_cell += CaMKb/Ndyads;
       CaMKt_cell += Cell.CaMKt[i]/Ndyads;
}
//############################8.comp_calc_dyad()#####################################///


//###################################part of 9.comp_ical()##############################//
double dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940))/4.230));
if (iso == 1) dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940+14))/4.230));

double td=0.6+1.0/(exp(-0.05*(v[ix][iy][iz]+6.0))+exp(0.09*(v[ix][iy][iz]+14.0)));
Cell.d=dss-(dss-Cell.d)*exp(-dt/td);



//for plotting ICaL only//

km2n=Cell.jca*1.0;
anca=1.0/(k2n/km2n+pow(1.0+Kmn/cadyad,4.0));
//double nca_print=0;   nca_print = anca*k2n/km2n-(anca*k2n/km2n-nca_print)*exp(-km2n*dt);
Cell.nca_print = anca*k2n/km2n-(anca*k2n/km2n-Cell.nca_print)*exp(-km2n*dt);
double fICaLp=(1.0/(1.0+KmCaMK/CaMKa_cell));
/*double PCa=0.0001;
if (iso == 1) PCa = 2.5*PCa;
double PCap=1.1*PCa;
double PCaNa=0.00125*PCa;
double PCaK=3.574e-4*PCa;
double PCaNap=0.00125*PCap;
double PCaKp=3.574e-4*PCap;*///move to top
 PhiCaL=4.0*vffrt*(1e-3*cadyad*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
 PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
 PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

double ICaL_print=(1.0-fICaLp)*PCa*PhiCaL*Cell.d*(f*(1.0-Cell.nca_print)+Cell.jca*fca*Cell.nca_print)+fICaLp*PCap*PhiCaL*Cell.d*(fp*(1.0-Cell.nca_print)+Cell.jca*fcap*Cell.nca_print);
double ICaNa_print=(1.0-fICaLp)*PCaNa*PhiCaNa*Cell.d*(f*(1.0-Cell.nca_print)+Cell.jca*fca*Cell.nca_print)+fICaLp*PCaNap*PhiCaNa*Cell.d*(fp*(1.0-Cell.nca_print)+Cell.jca*fcap*Cell.nca_print);
double ICaK_print=(1.0-fICaLp)*PCaK*PhiCaK*Cell.d*(f*(1.0-Cell.nca_print)+Cell.jca*fca*Cell.nca_print)+fICaLp*PCaKp*PhiCaK*Cell.d*(fp*(1.0-Cell.nca_print)+Cell.jca*fcap*Cell.nca_print);
//###################################part of 9.comp_ical()##############################//



//########################1.comp_ina() below########################//
if (iso == 1) gna = 2.7*gna;
mtau = 1.0/(6.765*exp((v[ix][iy][iz]+11.64)/34.77)+8.552*exp(-(v[ix][iy][iz]+77.42)/5.955));
hftau=1.0/(1.432e-5*exp(-(v[ix][iy][iz]+1.196)/6.285)+6.149*exp((v[ix][iy][iz]+0.5096)/20.27));
hstau=1.0/(0.009794*exp(-(v[ix][iy][iz]+17.95)/28.05)+0.3343*exp((v[ix][iy][iz]+5.730)/56.66));
hsptau=3.0*hstau;
jtau=2.038+1.0/(0.02136*exp(-(v[ix][iy][iz]+100.6)/8.281)+0.3052*exp((v[ix][iy][iz]+0.9941)/38.45));
jptau=1.46*jtau;
Ahf=0.99;
Ahs=1.0-Ahf;
mss = 1.0/(1.0+exp((-(v[ix][iy][iz]+44.57))/9.871));


hss =1.0/(1+exp((v[ix][iy][iz]+82.90)/6.086));
hssp=1.0/(1+exp((v[ix][iy][iz]+89.1)/6.086));
if (iso == 1)
{
	hss =1.0/(1+exp((v[ix][iy][iz]+82.90+5)/6.086));
	hssp=1.0/(1+exp((v[ix][iy][iz]+89.1+5)/6.086));
}
jss = hss;

Cell.m = mss-(mss-Cell.m)*exp(-dt/mtau);
Cell.hf=hss-(hss-Cell.hf)*exp(-dt/hftau);
Cell.hs=hss-(hss-Cell.hs)*exp(-dt/hstau);
Cell.hsp=hssp-(hssp-Cell.hsp)*exp(-dt/hsptau);

h = Ahf*Cell.hf+Ahs*Cell.hs;
hp=Ahf*Cell.hf+Ahs*Cell.hsp;
Cell.j = jss-(jss-Cell.j)*exp(-dt/jtau);
Cell.jp=jss-(jss-Cell.jp)*exp(-dt/jptau);

finap=(1.0/(1.0+KmCaMK/CaMKa_cell));

mlss=1.0/(1.0+exp((-(v[ix][iy][iz]+42.85))/5.264));
mltau = mtau;
Cell.ml=mlss-(mlss-Cell.ml)*exp(-dt/mltau);

hlss=1.0/(1.0+exp((v[ix][iy][iz]+87.61)/7.488));
hlssp=1.0/(1.0+exp((v[ix][iy][iz]+93.81)/7.488));
hltau=200.0;
hlptau=3.0*hltau;
Cell.hl=hlss-(hlss-Cell.hl)*exp(-dt/hltau);
Cell.hlp=hlssp-(hlssp-Cell.hlp)*exp(-dt/hlptau);

gnal = 0.0075;
finalp = finap;

ena = ((R*temp)/frdy)*log(nao/Cell.nai);

ina=gna*(v[ix][iy][iz]-ena)*Cell.m*Cell.m*Cell.m*((1.0-finap)*h*Cell.j+finap*hp*Cell.jp);
inal=gnal*(v[ix][iy][iz]-ena)*Cell.ml*((1.0-finalp)*Cell.hl+finalp*Cell.hlp);
//########################1.comp_ina() below########################//




//######################2.comp_ito() below########################//
//double EK;
//EK=(R*temp/frdy)*log(ko/Cell.ki);//move above because of EK

double ass;
ass=1.0/(1.0+exp((-(v[ix][iy][iz]-14.34))/14.82));
double atau;
atau=1.0515/(1.0/(1.2089*(1.0+exp(-(v[ix][iy][iz]-18.4099)/29.3814)))+3.5/(1.0+exp((v[ix][iy][iz]+100.0)/29.3814)));

Cell.a=ass-(ass-Cell.a)*exp(-dt/atau);

double iss;
iss=1.0/(1.0+exp((v[ix][iy][iz]+43.94)/5.711));
double delta_epi=1.0;
double iftau,istau;

iftau=4.562+1/(0.3933*exp((-(v[ix][iy][iz]+100.0))/100.0)+0.08004*exp((v[ix][iy][iz]+50.0)/16.59));
istau=23.62+1/(0.001416*exp((-(v[ix][iy][iz]+96.52))/59.05)+1.780e-8*exp((v[ix][iy][iz]+114.1)/8.079));

iftau*=delta_epi;
istau*=delta_epi;

double AiF,AiS;
AiF=1.0/(1.0+exp((v[ix][iy][iz]-213.6)/151.2));
AiS=1.0-AiF;

Cell.iF=iss-(iss-Cell.iF)*exp(-dt/iftau);
Cell.iS=iss-(iss-Cell.iS)*exp(-dt/istau);

double i_ito;
i_ito=AiF*Cell.iF+AiS*Cell.iS;

double assp;
assp=1.0/(1.0+exp((-(v[ix][iy][iz]-24.34))/14.82));
Cell.ap=assp-(assp-Cell.ap)*exp(-dt/atau);

double dti_develop,dti_recover;
dti_develop=1.354+1.0e-4/(exp((v[ix][iy][iz]-167.4)/15.89)+exp(-(v[ix][iy][iz]-12.23)/0.2154));
dti_recover=1.0-0.5/(1.0+exp((v[ix][iy][iz]+70.0)/20.0));

double tiFp,tiSp;
tiFp=dti_develop*dti_recover*iftau;
tiSp=dti_develop*dti_recover*istau;

Cell.iFp=iss-(iss-Cell.iFp)*exp(-dt/tiFp);
Cell.iSp=iss-(iss-Cell.iSp)*exp(-dt/tiSp);

double ip;
ip=AiF*Cell.iFp+AiS*Cell.iSp;

double Gto = 0.02;
double fItop;
fItop=(1.0/(1.0+KmCaMK/CaMKa_cell));

Ito=Gto*(v[ix][iy][iz]-EK)*((1.0-fItop)*Cell.a*i_ito+fItop*Cell.ap*ip);
//######################2.comp_ito() below########################//


//############################10. comp_it()#####################################///
//if ( (ix>3*N_x/8 && ix<5*N_x/8) && (iy>3*N_y/8 && iy<5*N_y/8) ) istim = ist; else istim = 0;
if ( iz == 1 and offset_z == 0 ) istim = ist; else istim = 0;

naiont = ina+inal+INab+ilcana+3*INaK+3*inaca+3*inacass;
kiont = Ito + IKr + IKs + IK1 + ilcak + IKb - 2*INaK + istim;
caiont = ilca+icab+ipca-2*inaca-2*inacass;
it = naiont + kiont + caiont;
//############################10. comp_it()#####################################///



//#############################11.calc_nai()###################################////
double dnai = -dt*(naiont*acap)/(vmyo*zna*frdy);
Cell.nai = dnai + Cell.nai;
//#############################11.calc_nai()###################################////


//#############################12.calc_ki()###################################////
double dki = -dt*((kiont)*acap)/(vmyo*zk*frdy);
Cell.ki = dki + Cell.ki;
//#############################12.calc_ki()###################################////




//printf("it value computed by compute_cell is %f\n",it);
v[ix][iy][iz] += -it*dt;
}
