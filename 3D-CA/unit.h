#ifndef UNIT_H_INCLUDED
#define UNIT_H_INCLUDED

struct unit
{
    double cacyt;
    double cass;
    double cansr;
    double cajsr;
    double cads;

    int nryr;
    int *ryrstate;
};

void initUnitValue(struct unit &Unit);

void releaseUnit(struct unit &Unit);


#endif // UNIT_H_INCLUDED
