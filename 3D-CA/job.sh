#!/bin/bash

#SBATCH --job-name=3d-tissue
#SBATCH --time=00:50:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=./Output_File.out

#SBATCH --ntasks=8
#SBATCH --ntasks-per-node=8
#SBATCH --exclusive
#SBATCH --cpus-per-task=2

#module load vtune_amplifier/2015.0

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#time mpirun -np 4  amplxe-cl -c hotspots -r amp_result -source-search-dir=/usit/abel/u1/qianglan/HPC-APP/tissue-stable/code -- ./tissue 2 2 
time mpirun -np 8 ./tissue 2 2 2
