#include <scif.h>
#include "cell.h"
#include "unit.h"
#include <cstring>
#include <stdlib.h>
#include "compute_cell.h"
#include "allocArray.h"
#define PORTNUMBER0 3000
#define PORTNUMBER1 3020

#define Ntotdyads 10000

double *** PHIallocate_3D_double(int Nx, int Ny,int Nz)
{
  double ***ptr;
  double *base_ptr = (double*)_mm_malloc(Nx*Ny*Nz*sizeof(double),64);
  ptr = (double***)_mm_malloc(Nx*sizeof(double**),64);
  for (int ix=0; ix<Nx; ix++)
  {
	ptr[ix] = (double**)_mm_malloc(Ny*sizeof(double*),64);
	for (int iy = 0; iy<Ny; iy++)
       {
		ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
       }
   }
   return ptr;
}


struct cell ***PHIAllocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z)
{
    struct cell ***SubCells;
    struct cell *cells = (struct cell*)_mm_malloc(sub_N_x*sub_N_y*sub_N_z*sizeof(struct cell),64);
    SubCells = (struct cell***)_mm_malloc(sub_N_x*sizeof(struct cell**),64);
    for(int ix=0; ix<sub_N_x; ix++)
    {
        SubCells[ix] = (struct cell**)_mm_malloc(sub_N_y*sizeof(struct cell*),64);
        for(int iy=0;iy<sub_N_y;iy++)
	     SubCells[ix][iy] = &(cells[ix*sub_N_y*sub_N_z+iy*sub_N_z]);
    }
    return SubCells;

}

void PHIdeallocate_3D_int(int ***ptr,int Nx)
{
	_mm_free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    _mm_free(ptr[i]);
	_mm_free(ptr);
}

int main()
{

	#define PORTNUMBER0 3000
       
	scif_epd_t source;
        int portNum = PORTNUMBER0;
	struct scif_portID dstID;
	dstID.node = 0;
	dstID.port = portNum;

	source = scif_open();
	if(source == ((scif_epd_t)-1))
	{
		printf("scif open failed!\n");
	}
	else 
		printf("mic0 scif open success!\n");
	
__retry:
	if(scif_connect(source, &dstID)<0)
	{
		if((errno == ECONNREFUSED))
		{
			printf("Scif_connect connect to host failed! \n");
			goto __retry;
		}

	}
	else 
		printf("mic0 connect to host success!\n");
	


	double ist,dt;
	double t=0;
	int  Nryr = 100;
	int Ndhpr = 15;
	int bcl=1000;

	int offset_x;
	int offset_z;

	int N_x,N_y,N_z;
	int Ndyads;
	int micsignal=0; 

	if(scif_recv(source,&N_x,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&N_y,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&N_z,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&Ndyads,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&bcl,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&dt,sizeof(double),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");


	if(scif_recv(source,&offset_x,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	if(scif_recv(source,&offset_z,sizeof(int),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

	cell ***subcells;
	subcells = Allocate3DSubCells(N_x,N_y,N_z);


//_mm_malloc(sizeof(int)*size*RNZ,64);
	v = PHIallocate_3D_double(N_x,N_y,N_z);
	vtemp = PHIallocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdt = PHIallocate_3D_double(N_x,N_y,N_z);
	memset(dvdt[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdtclock = PHIallocate_3D_double(N_x,N_y,N_z);
	memset(dvdtclock[0][0],0,N_x*N_y*N_z*sizeof(double));
	dtstep = PHIallocate_3D_double(N_x,N_y,N_z);
	memset(dtstep[0][0],0,N_x*N_y*N_z*sizeof(double));

	for(int cx = 0; cx < N_x; cx++)
	    for(int cy =0; cy < N_y; cy++)
		for(int cz = 0;cz < N_z;cz++)
		    InitCellValue(subcells[cx][cy][cz],Ndyads,1,1,Nryr);

	for(int cx = 1;cx<N_x-1;cx++)
	    for(int cy = 1;cy<N_y-1;cy++)
		for(int cz =1;cz<N_z-1;cz++)
		{
		      compute_cell_id(subcells[cx][cy][cz], cx,cy,cz, partitioner);//also initialize the data random seed
            	      v[cx][cy][cz] = -87.6;
	   	      //printf("cell id is : %d \n", subcells[cx][cy][cz].cellID);
		}


	
	int goon=1;
	int semaphor=-1;


//omp_set_num_threads(118);

	int threads;
	#pragma omp parallel
	{
			
			threads = omp_get_num_threads();
		
	}

	scif_send(source,&threads,sizeof(int),SCIF_SEND_BLOCK);


	#pragma omp parallel
	{


		while(goon)
		{

			#pragma omp master
			{
				scif_recv(source,&semaphor,sizeof(int),SCIF_RECV_BLOCK);

			}
			#pragma omp barrier

			if(semaphor==1)
			{

				#pragma omp master
				{
					if(scif_recv(source,&ist,sizeof(double),SCIF_RECV_BLOCK)<0)
						printf("mic 0: error in scif_recv for array a %d\n",errno);
					else
						printf("mic 0 recv for array a success!\n");
				}


                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy][iz])>0.2)||(dvdtclock[ix][iy][iz]<20) )
                            {
                                comp_cell(subcells[ix][iy][iz],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr);
                                dtstep[ix][iy][iz] = 0;
                            }

                //////////////////////////////////????????????????????????????????????????//////////////////////////	
                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);




  			#pragma omp barrier

  				#pragma omp master
				{
					scif_send(source,&goon,sizeof(int),SCIF_SEND_BLOCK);
				}
  	  		}


			if(semaphor==3)
			{
				#pragma omp master
				{
					goon=0;
					scif_send(source,&goon,sizeof(int),SCIF_SEND_BLOCK);
				}
				#pragma omp barrier
			}


		}
	}
	



	//scif_send(source,c,num*sizeof(int),SCIF_SEND_BLOCK);

    PHIdeallocate_3D_double (v,N_x);
    PHIdeallocate_3D_double (vtemp,N_x);
    PHIdeallocate_3D_double (dvdt,N_x);
    PHIdeallocate_3D_double (dvdtclock,N_x);
    PHIdeallocate_3D_double (dtstep,N_x);
	return 0;

}