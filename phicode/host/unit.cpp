#include "unit.h"
#include <stdlib.h>

void initUnitValue(struct unit &Unit)
{
    int nryR = Unit.nryr;
    Unit.ryrstate = (int *)malloc(nryR*sizeof(int));
    Unit.cacyt = 6.73e-5;
    Unit.cass = 6.65e-5;
    //Unit.cads = 1e-4*1e3;//added by qianglan
    Unit.cads = (6.65e-5)*1e3;//added by qianglan  what is the acculate value???????
    Unit.cansr = 2.5;
    Unit.cajsr = 2.5;
    for(int j=0; j<nryR;j++)
        Unit.ryrstate[j] = 0;

    Unit.nryr_in_state_0 = nryR;
    Unit.nryr_in_state_1 = 0;
    Unit.nryr_in_state_2 = 0;
    Unit.nryr_in_state_3 = 0;

    return;

}

void releaseUnit(struct unit &Unit)
{
    free(Unit.ryrstate);
    return;
}
