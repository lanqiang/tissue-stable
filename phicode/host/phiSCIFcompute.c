#include <stdlib.h>
#include <stdio.h> 
#include <assert.h> 
#include <string.h>
#include <scif.h>
#include <source/COIProcess_source.h>
#include <source/COIEngine_source.h>

#define PORTNUMBER0 3000
#define PORTNUMBER1 3020



void PHIdevicesetupSCIF(int phinum,int Nx, int Ny, int Nz,int offset_x, int offset_z, int ndyads, int bcl, double dt)
{

	COIRESULT result0 = COI_ERROR;
	COIPROCESS proc0;
	COIENGINE engine0;
	unsigned int num_engines = 0;
	
	const char *SINK_NAME0 = "PHICOMP";



	result0 = COIEngineGetCount(COI_ISA_KNC, &num_engines);
	if(result0!=COI_SUCCESS)
	{
		printf("COIEngineGetCount result %s\n",	COIResultGetName(result0));
		return;
	}
	if(num_engines<1)
	{
		printf("Error: Need at least 1 engine!\n");
		return;
	}
	printf("the total engines num is %d\n",num_engines);

	result0 = COIEngineGetHandle(COI_ISA_KNC,0,&engine0);
	if(result0!=COI_SUCCESS)
	{
		printf("COIEngineGetHandle result0 %s\n",COIResultGetName(result0));
		return;
	}
	else
		printf("COIEngineGetHandle0 success!\n");

	scif_epd_t source0;
	unsigned int portNum0;
	struct scif_portID portID0;
//	scif_epd_t dst0;
	
	//first call scif_open()
	source0 = scif_open();
	if(source0 == ((scif_epd_t)-1))
	{
		printf("scif open failed!\n");
	}

	else 
		printf("scif open success !\n");
	portNum0 = PORTNUMBER0;
	
	//then bind the port 
	if(scif_bind(source0,portNum0)<0)
	{
		printf("scif bind failed!\n");
		return;
	}
	else 
		printf("scif bind success!\n");


	//listen some endpoint to connect
	if(scif_listen(source0,5)<0)
	{
		printf("scif listen failed!\n");
		return;
	}

	else 
		printf("host listen success!");



	result0 = COIProcessCreateFromFile(engine0,
                                        SINK_NAME0,
                                        0,NULL,
                                        0,NULL,
                                        1,NULL,
                                        0,
                                        NULL,
                                        &proc0
        );
        if(result0!=COI_SUCCESS)
        {
                printf ("COIProcessCreateFromFile result %s\n",COIResultGetName (result0));

                return;
        }
        else
                printf("COIProcessCreateFromFile success!\n");





	//accept if some remote endpoint connect 
	if(scif_accept(source0,&portID0,&P->dst0,SCIF_ACCEPT_SYNC)<0)
	{
		printf("scif accept mic0 failed!\n");
		return;
	}

	else
		printf("host accept mic0 success!\n");

				int start = L->GPUstart[0];
			int size  = L->GPUstart[L->GPUs]-L->GPUstart[0];

	scif_send(P->dst0,&Nx,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&Ny,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&Nz,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&ndyads,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&bcl,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&dt,sizeof(double),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&offset_x,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&offset_z,sizeof(int),SCIF_SEND_BLOCK);


	int threads= -1;

	scif_recv(P->dst0,&threads,sizeof(int),SCIF_RECV_BLOCK);

	printf("Running %d  threads\n",threads);

	return;

}

void PHIcomputeSCIF(int phinum, double ist)
{
	int semaphor = 1;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&ist,sizeof(double),SCIF_SEND_BLOCK);
	scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK);
}
/*
void PHIswapSCIF(int phinum, localPHItype* P)
{
	int semaphor = 2;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK);
}
*/

void PHIfinishSCIF(int phinum)
{
	int semaphor = 3;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK);
}