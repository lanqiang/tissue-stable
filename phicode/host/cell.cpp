#include "cell.h"
#include "unit.h"
#include <cstring>
#include <stdlib.h>

struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z)
{
    struct cell ***SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sub_N_z*sizeof(struct cell));
    SubCells = (struct cell***)malloc(sub_N_x*sizeof(struct cell**));
    for(int ix=0; ix<sub_N_x; ix++)
    {
        SubCells[ix] = (struct cell**)malloc(sub_N_y*sizeof(struct cell*));
        for(int iy=0;iy<sub_N_y;iy++)
	     SubCells[ix][iy] = &(cells[ix*sub_N_y*sub_N_z+iy*sub_N_z]);
    }
    return SubCells;

}

void deallocate3DSubCells(struct cell ***SubCells,int Nx)
{
    free(SubCells[0][0]);
    for(int i=0;i<Nx;i++)
    	free(SubCells[i]);
    free(SubCells);
    return;
}


void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z,int nryr,int ndhpr,int beats)
{
    Cell.nai=5.7;//?
    Cell.nass = Cell.nai;
    Cell.ki=143;
    Cell.kss = Cell.ki;

    Cell.m=0.02;
    Cell.hf=0.49;
    Cell.hs=0.49;
    Cell.j=0.49;
    Cell.hsp=0.26;
    Cell.jp=0.49;

    Cell.ml=0.00019;
    Cell.hl=0.46;
    Cell.hlp=0.22;

    Cell.a=0.001;
    Cell.iF=1;
    Cell.iS=.45;
    Cell.ap=.0005;
    Cell.iFp=1;
    Cell.iSp=.49;

    Cell.d=0;
    Cell.ff=1;
    Cell.fs=.87;
    Cell.fcaf=1;
    Cell.fcas=1;
    Cell.jca=1;
    Cell.ffp=1;
    Cell.fcafp=1;

    Cell.xrf=0;
    Cell.xrs=0.59;
    Cell.xs1=0.3;
    Cell.xs2=0;
    Cell.xk1=1;

    Cell.nca_print = 0;//added by qianglan
   

    Cell.unitNum_x = unitNum_x;
    Cell.unitNum_y = unitNum_y;
    Cell.unitNum_z = unitNum_z;

    Cell.Ndhpr = ndhpr;
    Cell.Nryr = nryr;

    int ndyads = unitNum_x*unitNum_y*unitNum_z;
    Cell.Ndyads = ndyads;

    Cell.beats = beats;

    Cell.nca = (double *)malloc(ndyads*sizeof(double)); memset(Cell.nca,0,ndyads*sizeof(double));
    Cell.CaMKt = (double *)malloc(ndyads*sizeof(double));memset(Cell.CaMKt,0.1,ndyads*sizeof(double));
    Cell.CaMKa = (double *)malloc(ndyads*sizeof(double));memset(Cell.CaMKa,0.1,ndyads*sizeof(double));
    Cell.CaMKb = (double *)malloc(ndyads*sizeof(double));memset(Cell.CaMKb,0.1,ndyads*sizeof(double));
   
    Cell.trel = (double *)malloc(ndyads*sizeof(double));memset(Cell.trel,-2000,ndyads*sizeof(double));
    Cell.Poryr = (double *)malloc(ndyads*sizeof(double));memset(Cell.Poryr,0,ndyads*sizeof(double));
    Cell.nactive = (int *)malloc(ndyads*sizeof(int));memset(Cell.nactive,0,ndyads*sizeof(int));

    //malloc unit ptr within cell
    Cell.unitsInCell = (unit *)malloc(ndyads*sizeof(unit));

    Cell.casstemp = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.casstemp,0,ndyads*sizeof(double));
    Cell.cansrtemp = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.cansrtemp,0,ndyads*sizeof(double));
    Cell.cacyttemp = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.cacyttemp,0,ndyads*sizeof(double));
    Cell.cassdiff = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.cassdiff,0,ndyads*sizeof(double));
    Cell.cansrdiff = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.cansrdiff,0,ndyads*sizeof(double));
    Cell.cacytdiff = (double *)malloc(ndyads*sizeof(double));
    memset(Cell.cacytdiff,0,ndyads*sizeof(double));

    Cell.rand_n = ndyads*(3*ndhpr+nryr);
    Cell.l_Random = (double *)malloc(Cell.rand_n*sizeof(double)); 
    memset(Cell.l_Random,0,ndyads*sizeof(double));

    //init the value of 1d mem
    for(int i=0; i<ndyads;i++)
    {
        Cell.CaMKt[i] = 0.009;
        Cell.nca[i] = 0.001;
        Cell.unitsInCell[i].nryr = nryr;
        initUnitValue(Cell.unitsInCell[i]);
    }

    //malloc random generator
    Cell.pRD = (struct RandData *)malloc(sizeof(struct RandData));
    //Cell.randcallcount  = 0;
    Cell.timestep = 0;

    return;
}


void releaseCell(struct cell &Cell)
{
    int ndyads = Cell.unitNum_x;
    for(int i=0; i<ndyads;i++)
        releaseUnit(Cell.unitsInCell[i]);
    free(Cell.unitsInCell);

    free(Cell.pRD);

    free(Cell.nca);
    free(Cell.CaMKt);
    free(Cell.CaMKa);
    free(Cell.CaMKb);
    free(Cell.trel);
    free(Cell.Poryr);
    free(Cell.nactive);

    if(Cell.casstemp!=NULL);
        free(Cell.casstemp);
    if(Cell.cansrtemp!=NULL);
        free(Cell.cansrtemp);
    if(Cell.cacyttemp!=NULL);
        free(Cell.cacyttemp);
    if(Cell.cassdiff!=NULL);
        free(Cell.cassdiff);
    if(Cell.cansrdiff!=NULL);
        free(Cell.cansrdiff);
    if(Cell.cacytdiff!=NULL);
        free(Cell.cacytdiff);
    if(Cell.l_Random!=NULL);
        free(Cell.l_Random);

    return;
}

void compute_cell_id(struct cell & Cell, int cx, int cy,int cz, Partitioner_t* partitioner)
{
    int total_N_x = partitioner->N[0];
    int total_N_y = partitioner->N[1];
    int total_N_z = partitioner->N[2];
    int my_offset_x = partitioner->my_offset[0];
    int my_offset_y = partitioner->my_offset[1];
    int my_offset_z = partitioner->my_offset[2];

//    int cell_id = (my_offset_y + cy - 1)*total_N_x + my_offset_x + cx -1;
    int cell_id = (my_offset_x+cx-1)*total_N_z*total_N_y+(my_offset_z+cz-1)*total_N_z+my_offset_y+cy-1;
    Cell.cellID = cell_id+1;//id from 1
    //Cell.cellID = 1;

    /*int my_rank_x = partitioner->my__rank_x();
    int my_rank_y = partitioner->my__rank_y();
    int sub_Nx = partitioner->sub_N_x();
    int sub_Ny = partitioner->sub_N_y();
    int cell_id = my_rank_y*((sub_Ny-2)*total_N_x)+my_rank_x*((sub_Ny-2)*(sub_Nx-2))+(sub_Nx-2)*(cy-1)+(cx-1);
    Cell.cellID = cell_id;*/
    Cell.pRD->myrandomseed = -Cell.cellID;
    Cell.pRD->iy = 0;
    Cell.pRD->randcallcount = 0;

    Cell.totalCellSize = total_N_x*total_N_y*total_N_z;


    return;

}

