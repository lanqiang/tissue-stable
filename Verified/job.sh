#!/bin/bash

#SBATCH --job-name=3d-tissue
#SBATCH --time=01:10:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=./Output_File.out
#SBATCH --partition=accel --gres=phi:1



#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --cpus-per-task=1

#module load vtune_amplifier/2015.0

source /cluster/bin/jobsetup
cp $SUBMITDIR/tissue $SCRATCH

module load gcc/4.8.2
module load intel/2015.1
module load intelmpi.intel

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export KMP_AFFINITY="verbose,granularity=fine,scatter"
#,granularity=fine,scatter"

#time mpirun -np 4  amplxe-cl -c hotspots -r amp_result -source-search-dir=/usit/abel/u1/qianglan/HPC-APP/tissue-stable/code -- ./tissue 2 2 
time mpirun -np 1 ./tissue  1 1 1 1 1 1 100 100 1 1 1 1 1
