#include "compute_cell.h"
#include "cell.h"


#include <cmath>
#include <cstring>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
//#include <random>



#include <sys/time.h>

#include "constant.h"

//#define TIME 



using namespace std;

//1
void comp_ina( double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double &,double ***,int ,int ,int ,double , cell &);
//2
void comp_ito(double &EK,double ***v,int ix,int iy,int iz,double dt,double &Ito,cell &Cell);
//3
void comp_ikr(double &IKr,double ***v,int ix,int iy,int iz,double dt,double EK,cell &Cell);
//4
void comp_iks(double ***v,int ix,int iy,int iz,double dt,double &IKs,cell &Cell);
//5
void comp_ik1(double &EK,double &IK1,double ***v,int ix,int iy,int iz,double dt,cell &Cell);
//6
void comp_inak(double &INaK,double ***v,int ix,int iy,int iz,cell &Cell);
//7
void comp_inab(double &IKb,double EK,double vffrt,double vfrt,double &INab,double ***v,int ix,int iy,int iz,cell &Cell);

//8
void comp_calc_dyad(double &jrel,double &ilca,double &ilcana,double &ilcak,double &inaca,double &icab,double &ipca,double &icat,double &inacass, double &cai,double &casub,double &cadyad,double &jsr,double &nsr, double &CaMKb_cell,double &CaMKt_cell,double ***v,int ix,int iy,int iz,double dt,double &km2n,double PCa,double PCap,double PCaNa,double PCaK,double PCaNap,double PCaKp,double &PhiCaNa,double vffrt,double frt,double &PhiCaK,double acap,double Ntotdyads,double vss_dyad,double vcyt_dyad,double &anca,int Ndhpr,double **T,double *ZT,double **B,double &PhiCaL,int Nryr,double t,int bcl,double vmyo,double vjsr_dyad,double vnsr_dyad,double kmtrpn,cell &Cell);

//9
void comp_ical(double ***v,int ix,int iy,int iz,double dt,double &km2n,double &anca,double &PhiCaL,double &PhiCaNa,double &PhiCaK,double &ICaL_print,double cadyad,double vffrt,double vfrt,double PCa,double PCap,double PCaNa,double PCaNap,double PCaK,double PCaKp,cell &Cell);


inline double timing(){
        double time;
        struct timeval timmer;

        gettimeofday(&timmer,NULL);
        time = timmer.tv_sec + timmer.tv_usec*1e-6;
        //time /= 1000000;
        return time;
}


inline double powerd(double x,int y)
{
    double tep;
    if(y==0)
        return 1;
    tep = powerd(x,y/2);
    if((y%2)==0){
        return tep*tep;
    } else {
        if(y>0)
            return x*tep*tep;
        else
            return (tep*tep)/x;
    }
}


 


void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z,double t, double ist, int bcl, double ***v,double ***dtstep,int Ndhpr , int Ntotdyads,int Nryr,double **T,double **B,double *ZT)
{

    //double T1=timing();
    //double T2=timing();
    //double DT = T2-T1;
//#ifdef TIME
    double T_Start = timing();
//#endif
    //test 
    
    
    double gna = 75;
    double mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs;
    double mss,hss,jss,hssp;
    double h,hp,finap;
    //double CaMKa_cell=0.1;//
    double mlss,mltau,hlss,hlssp,hltau,hlptau;

    double gnal,finalp;
    double naiont=0,caiont,kiont=0,it,istim;
    double ina,inal,INab,ilca,icab,icat,ipca,ilcana,INaK,inaca,inacass,Ito,IKr,IKs,IK1,IKb,ilcak;
    double CaMKb_cell,CaMKt_cell;
    double nsr, jsr, cadyad=0.1, casub,cai;

    
    double ena;

    /*double casstemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
    double cansrtemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
    double cacyttemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
    double cassdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
    double cansrdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
    double cacytdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
*/
    

    

    
    double kmtrpn = 0.0005;///

    //added 
    if (iso == 1) kmtrpn = 1.6*kmtrpn;

    


    /////////////////////
    
    double	ageo = 2*pi*ar*ar + 2*pi*ar*al;///
    double	acap = 2*ageo;///
    double vcell = 1000*pi*ar*ar*al;///
    double	vmyo = 0.68*vcell;////
    //double vds = 1e-13;//same as vol_ds

    double	vnsr = 0.0552*vcell;///
    double	vjsr = 0.0048*vcell;///
    double	vss = 0.01*vcell;///
    double	vjsr_dyad = vjsr/Ntotdyads;///
    double	vnsr_dyad = vnsr/Ntotdyads;///
    double	vcyt_dyad = vmyo/Ntotdyads;///
    double	vss_dyad = vss/Ntotdyads;///
    //double	vol_ds = 1e-13;
 
    //double Kmn = 0.002;///
    //double k2n = 1000.0;///
    double km2n;
    double anca;

    double  PhiCaL;
    double  PhiCaNa;
    double  PhiCaK;

    double PCa=0.0001;
    if (iso == 1) PCa = 2.5*PCa;
    double PCap=1.1*PCa;
    double PCaNa=0.00125*PCa;
    double PCaK=3.574e-4*PCa;
    double PCaNap=0.00125*PCap;
    double PCaKp=3.574e-4*PCap;

    double vffrt = v[ix][iy][iz]*frdy*frdy/(R*temp);
    double vfrt = v[ix][iy][iz]*frdy/(R*temp);

    double dt = dtstep[ix][iy][iz];

    double EK;
    double ICaL_print;

    double jrel;
    
#ifdef TIME
    double T_start,T_end;
#endif

#ifdef TIME
    T_start = timing();
#endif
//########################1.comp_ina() below########################//
comp_ina( gna,mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs,mss,hss,hssp,jss,h,hp,finap,mlss,mltau,hlss,hlssp,hltau,hlptau,gnal,finalp,ena,ina,inal,v,ix,iy,iz,dt,Cell);
    
    //########################1.comp_ina() below########################//
#ifdef TIME
    T_end = timing();
    Cell.T_ina += T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //######################2.comp_ito() below########################//
 comp_ito(EK,v, ix, iy, iz, dt,Ito,Cell);
    
    //######################2.comp_ito() below########################//
#ifdef TIME
    T_end = timing();
    Cell.T_ito += T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
 //############################3.comp_ikr()#########################//
 comp_ikr( IKr,v, ix, iy, iz, dt,EK,Cell);
    //###############################3.comp_ikr()#######################//
#ifdef TIME
    T_end = timing();
    Cell.T_ikr+=T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
//#########################4.comp_iks()##########################//
comp_iks(v,ix, iy, iz, dt, IKs,Cell);
    //#########################4.comp_iks()##########################//
#ifdef TIME
    T_end = timing();
    Cell.T_iks+=T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
//###############################5.comp_ik1()#######################//
 comp_ik1(EK,IK1, v, ix, iy, iz, dt,Cell);
    //###############################5.comp_ik1()#######################//
#ifdef TIME
    T_end = timing();
    Cell.T_ik1+=T_end-T_start;
#endif

    
#ifdef TIME
    T_start = timing();
#endif
//################################6.comp_inak()####################//
comp_inak( INaK, v, ix, iy, iz,Cell);
    //################################6.comp_inak()####################//
#ifdef TIME
    T_end = timing();
    Cell.T_inak += T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //#################################7.comp_inab()######################//
 comp_inab( IKb, EK,vffrt, vfrt, INab, v, ix, iy, iz, Cell);
    //#################################7.comp_inab()######################//
#ifdef TIME
    T_end = timing();
    Cell.T_inab += T_end-T_start;
#endif

   


    


#ifdef TIME
    T_start = timing();
#endif


    //############################8.comp_calc_dyad()#####################################/// 
comp_calc_dyad( jrel, ilca, ilcana, ilcak, inaca, icab, ipca, icat, inacass,  cai, casub, cadyad, jsr, nsr, CaMKb_cell,CaMKt_cell,v, ix, iy, iz, dt, km2n, PCa, PCap, PCaNa, PCaK, PCaNap, PCaKp, PhiCaNa, vffrt, vfrt, PhiCaK, acap, Ntotdyads, vss_dyad, vcyt_dyad, anca, Ndhpr, T,ZT,B,PhiCaL, Nryr, t, bcl, vmyo, vjsr_dyad, vnsr_dyad, kmtrpn,Cell); 

    //############################8.comp_calc_dyad()#####################################///
#ifdef TIME
    T_end = timing();
    Cell.T_calc_dyad += T_end-T_start;
#endif


#ifdef TIME
    T_start=timing();
#endif
    //################################### part of 9.comp_ical()##############################//
     ////////////////
 comp_ical(v, ix, iy, iz, dt, km2n, anca, PhiCaL, PhiCaNa, PhiCaK,ICaL_print,cadyad, vffrt, vfrt, PCa, PCap, PCaNa, PCaNap, PCaK, PCaKp, Cell);
    /*double dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940))/4.230));
    if (iso == 1) dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940+14))/4.230));

    double td=0.6+1.0/(exp(-0.05*(v[ix][iy][iz]+6.0))+exp(0.09*(v[ix][iy][iz]+14.0)));
    Cell.d=dss-(dss-Cell.d)*exp(-dt/td);
     ////////////////


    double fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58)/3.696));
    if (iso == 1) fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58+8)/3.696));

    double tff=7.0+1.0/(0.0045*exp(-(v[ix][iy][iz]+20.0)/10.0)+0.0045*exp((v[ix][iy][iz]+20.0)/10.0));
    double tfs=1000.0+1.0/(0.000035*exp(-(v[ix][iy][iz]+5.0)/4.0)+0.000035*exp((v[ix][iy][iz]+5.0)/6.0));
    double Aff=0.6;
    double Afs=1.0-Aff;
    Cell.ff=fss-(fss-Cell.ff)*exp(-dt/tff);
    Cell.fs=fss-(fss-Cell.fs)*exp(-dt/tfs);
    //double f=Aff*Cell.ff+Afs*Cell.fs;
     Cell.f=Aff*Cell.ff+Afs*Cell.fs;
    double fcass=fss;
    double tfcaf=7.0+1.0/(0.04*exp(-(v[ix][iy][iz]-4.0)/7.0)+0.04*exp((v[ix][iy][iz]-4.0)/7.0));
    double tfcas=100.0+1.0/(0.00012*exp(-v[ix][iy][iz]/3.0)+0.00012*exp(v[ix][iy][iz]/7.0));
    double Afcaf=0.3+0.6/(1.0+exp((v[ix][iy][iz]-10.0)/10.0));
    double Afcas=1.0-Afcaf;
    Cell.fcaf=fcass-(fcass-Cell.fcaf)*exp(-dt/tfcaf);
    Cell.fcas=fcass-(fcass-Cell.fcas)*exp(-dt/tfcas);
    //double fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
    Cell.fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
    double tjca = 75.0;
    Cell.jca=fcass-(fcass-Cell.jca)*exp(-dt/tjca);
    double tffp=2.5*tff;
    Cell.ffp=fss-(fss-Cell.ffp)*exp(-dt/tffp);
    //double fp=Aff*Cell.ffp+Afs*Cell.fs;
    Cell.fp=Aff*Cell.ffp+Afs*Cell.fs;
    double tfcafp=2.5*tfcaf;
    Cell.fcafp=fcass-(fcass-Cell.fcafp)*exp(-dt/tfcafp);
    //double fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;
    Cell.fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;
    //###################################part of 9.comp_ical()##############################//

//###################################part of 9.comp_ical()##############################//  
   //for plotting ICaL only//
    //double Kmn = 0.002;//move top
    //double k2n = 1000.0;//move top
     km2n=Cell.jca*1.0;
     anca=1.0/(k2n/km2n+pow(1.0+Kmn/cadyad,4.0));
    //double nca_print=0;   nca_print = anca*k2n/km2n-(anca*k2n/km2n-nca_print)*exp(-km2n*dt);
    Cell.nca_print = anca*k2n/km2n-(anca*k2n/km2n-Cell.nca_print)*exp(-km2n*dt);
    double fICaLp=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));
    
      PhiCaL=4.0*vffrt*(1e-3*cadyad*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
      PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
      PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

     ICaL_print=(1.0-fICaLp)*PCa*PhiCaL*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCap*PhiCaL*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaNa_print=(1.0-fICaLp)*PCaNa*PhiCaNa*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaNap*PhiCaNa*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaK_print=(1.0-fICaLp)*PCaK*PhiCaK*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaKp*PhiCaK*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);*/
    //###################################part of 9.comp_ical()##############################//
#ifdef TIME
    T_end = timing();
    Cell.T_ical+=T_end-T_start;
#endif
    


    

#ifdef TIME
    T_start = timing();
#endif
    //############################10. comp_it()#####################################///
    //if ( (ix>3*N_x/8 && ix<5*N_x/8) && (iy>3*N_y/8 && iy<5*N_y/8) ) istim = ist; else istim = 0;
    if ( iz == 1 and offset_z == 0 ) istim = ist; else istim = 0;
    //istim = ist;

    naiont = ina+inal+INab+ilcana+3*INaK+3*inaca+3*inacass;
    kiont = Ito + IKr + IKs + IK1 + ilcak + IKb - 2*INaK + istim;
    caiont = ilca+icab+ipca-2*inaca-2*inacass;
    it = naiont + kiont + caiont;
    //############################10. comp_it()#####################################///



    //#############################11.calc_nai()###################################////
    double dnai = -dt*(naiont*acap)/(vmyo*zna*frdy);
    Cell.nai = dnai + Cell.nai;
    //#############################11.calc_nai()###################################////


    //#############################12.calc_ki()###################################////
    double dki = -dt*((kiont)*acap)/(vmyo*zk*frdy);
    Cell.ki = dki + Cell.ki;
    //#############################12.calc_ki()###################################////




    //printf("it value computed by compute_cell is %f\n",it);
    v[ix][iy][iz] += -it*dt;

        //out put info
        Cell.ina = ina;
        Cell.ICaL_print=ICaL_print;
        Cell.Ito=Ito;
        Cell.IKs=IKs;
        Cell.IKr=IKr;
        Cell.IK1=IK1;
        Cell.INaK=INaK;
        Cell.inaca=inaca;
        Cell.inacass=inacass;
        Cell.jrel=jrel;
//#ifdef TIME
   double T_end = timing();
 //   Cell.T_rest += T_end-T_start;
//#endif

//#ifdef TIME
    Cell.T_cell += T_end-T_Start;
//#endif



}

















