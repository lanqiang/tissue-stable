#ifndef COMPUTE_CELL_H_INCLUDED
#define COMPUTE_CELL_H_INCLUDED

#include "cell.h"
//#include <random>


void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_y, double t, double ist, int bcl, double ***v,double ***dtstep,int Ndhpr , int Ntotdyads, int nryr,double **T,double **B,double *ZT);


#endif // COMPUTE_CELL_H_INCLUDED
