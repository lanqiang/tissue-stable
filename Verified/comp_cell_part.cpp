#include "constant.h"
#include "cell.h"

#include "mkl_vsl.h"
#include "mkl_lapacke.h"
#include "immintrin.h"

#include <sys/time.h>
#include <cmath>


inline double timing(){
        double time;
        struct timeval timmer;

        gettimeofday(&timmer,NULL);
        time = timmer.tv_sec + timmer.tv_usec*1e-6;
        //time /= 1000000;
        return time;
}

void binomialVec(int N,int n0,int n1,int n2,int n3,double p0,double p1,double p2,double p3,double *randomData,int &k0,int &k1,int &k2,int &k3,double **T)
{
    int flag;
    double p_nk[4] __attribute__((aligned(32)))={};
    double q[4] __attribute__((aligned(32))) ={};
    p_nk[0] = pow((1-p0),n0);
    p_nk[1] = pow((1-p1),n1);
    p_nk[2] = pow((1-p2),n2);
    p_nk[3] = pow((1-p3),n3);

    q[0] = p0/(1-p0);
    q[1] = p1/(1-p1);
    q[2] = p2/(1-p2);
    q[3] = p3/(1-p3);

    double *dix;
    dix = (double *)_mm_malloc(4*sizeof(double),32);
    
    __m256d Q=_mm256_load_pd(q);
    __m256d P_NK=_mm256_load_pd(p_nk);
    __m256d remainder =  _mm256_load_pd(randomData);
    double *Factor = &(T[0][0]);
    k0=-1;    k1=-1;    k2=-1;    k3=-1;
    int ix0,ix1,ix2,ix3;
    flag=0;
    if(randomData[0]>=0){
        flag=1;
        k0++;
    }
    if(randomData[1]>=0){
        flag=1;
        k1++;
    }
    if(randomData[2]>=0){
        flag=1;
        k2++;
    }
    if(randomData[3]>=0){
        flag=1;
        k3++;
    }


    int mn0=n0*101,mn1=n1*101,mn2=n2*101,mn3=n3*101;

    __m256d dk,vindex,Tnk,Tp,Tpnk;
    int h=0;

    while(flag&&h<(N-1))
    {
        h++;
        ix0 = mn0+k0;
        ix1 = mn1+k1;
        ix2 = mn2+k2;
        ix3 = mn3+k3;

        Tnk = _mm256_setr_pd(Factor[ix0],Factor[ix1],Factor[ix2],Factor[ix3]);//T[n][k]
        Tpnk = _mm256_mul_pd(Tnk,P_NK);//T[n][k]*p_knk
        remainder = _mm256_sub_pd(remainder,Tpnk);//remainder-=T[n][k]*p_k*p_nk
        P_NK = _mm256_mul_pd(P_NK,Q);//p_k=p_k*p

        _mm256_store_pd(dix,remainder);
        flag=0;
        if(dix[0]>=0){
            flag=1;
            k0++;
        }
        if(dix[1]>=0){
            flag=1;
            k1++;
        }
        if(dix[2]>=0){
            flag=1;
            k2++;
        }
        if(dix[3]>=0){
            flag=1;
            k3++;
        }
    }
    _mm_free(dix);
}
inline void CaculateRyrOpenBinomialVec(int i,int Ndhpr,int Nryr,double *l_Random,struct cell &Cell, int &nryrc1, int &nryrc2, int &nryro1, int &nryro2,double dt,double kC1O1,double kC1C2,double kO1C1,double kO1O2,double kC2C1,double kC2O2,double kO2C2,double kO2O1,double **T)
{
    int N=101;
    int k0=-1,k1=-1,k2=-1,k3=-1;
    int n0 = Cell.unitsInCell[i].nryr_in_state_0;
    int n1 = Cell.unitsInCell[i].nryr_in_state_1;
    int n2 = Cell.unitsInCell[i].nryr_in_state_2;
    int n3 = Cell.unitsInCell[i].nryr_in_state_3;

    double p0 = kC1O1*dt;
    double p1 = kO1C1*dt;
    double p2 = kC2C1*dt;
    double p3 = kO2C2*dt;
    //if(i==0)
    //{
        //printf("n0=%d n1=%d n2=%d n3=%d\n",n0,n1,n2,n3);
    //}

    binomialVec( N,n0,n1,n2,n3,p0,p1,p2,p3,l_Random+i*(Ndhpr+1+8)+Ndhpr+1,k0,k1,k2,k3,T);

    n0 = Cell.unitsInCell[i].nryr_in_state_0-k0;
    n1 = Cell.unitsInCell[i].nryr_in_state_1-k1;
    n2 = Cell.unitsInCell[i].nryr_in_state_2-k2;
    n3 = Cell.unitsInCell[i].nryr_in_state_3-k3;

    nryrc1+=n0;
    nryro1+=k0;
    nryro1+=n1;
    nryrc1+=k1;
    nryrc2+=n2;
    nryrc1+=k2;
    nryro2+=n3;
    nryrc2+=k3;

    p0 = kC1C2*dt/(1-kC1O1*dt);
    p1 = kO1O2*dt/(1-kO1C1*dt);
    p2 = kC2O2*dt/(1-kC2C1*dt);
    p3 = kO2O1*dt/(1-kO2C2*dt);
    k0=-1;  k1=-1;  k2=-1;  k3=-1;

    binomialVec(N,n0,n1,n2,n3,p0,p1,p2,p3,l_Random+i*(Ndhpr+1+8)+Ndhpr+1+4,k0,k1,k2,k3,T);
    nryrc1-=k0;
    nryrc2+=k0;
    nryro1-=k1;
    nryro2+=k1;
    nryrc2-=k2;
    nryro2+=k2;
    nryro2-=k3;
    nryro1+=k3;
    Cell.unitsInCell[i].nryr_in_state_0=nryrc1;
    Cell.unitsInCell[i].nryr_in_state_1=nryro1;
    Cell.unitsInCell[i].nryr_in_state_2=nryrc2;
    Cell.unitsInCell[i].nryr_in_state_3=nryro2;
    
        
}
inline int constp_binomial(struct cell &Cell,double remainder,double *BR)
{
    int i = 0;
    while(remainder>BR[i])
    {
        i++;
    }
    Cell.RandUsedN+=1;
    return i;
}

inline int binomial(struct cell &Cell,int n,double p,double remainder,double *TR,double *ZT)
{

    //if(((p<0.00000001)&&remainder<ZT[n])||(n==0))
    if((p<0.00000001)||(n==0))
	return 0;
    int i = -1;
    double p_knk=pow((1-p),n);
    //double p_knk=powerd((1-p),n);
    double p1p=p/(1-p);
    while(remainder>=0)  // &&i<n
    {
        i++;
        remainder -=TR[i]*p_knk;
        p_knk = p_knk*p1p;
    }
    Cell.RandUsedN+=1;
    return i;
}


inline void CaculateRyrOpenBinomial(int i,int Ndhpr,int Nryr,double *l_Random,struct cell &Cell, int &nryrc1, int &nryrc2, int &nryro1, int &nryro2,double dt,double kC1O1,double kC1C2,double kO1C1,double kO1O2,double kC2C1,double kC2O2,double kO2C2,double kO2O1,double **T,double **B,double *ZT)
{

    int c;
    int d;
    double remainder1 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+0];
    double remainder2 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+1];
    double remainder3 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+2];
    double remainder4 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+3];
    double remainder5 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+4];
    double remainder6 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+5];
    double remainder7 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+6];
    double remainder8 = l_Random[i*(Cell.randU)+Cell.LtypeRandN+7];
    c=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_0,kC1O1*dt,remainder1,T[Cell.unitsInCell[i].nryr_in_state_0],ZT);
    d=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_0-c,kC1C2*dt/(1-kC1O1*dt),remainder5,T[Cell.unitsInCell[i].nryr_in_state_0-c],ZT);
    nryrc1+=Cell.unitsInCell[i].nryr_in_state_0-(c+d);
    nryro1+=c;
    nryrc2+=d;

    //c=binomial(Cell.unitsInCell[i].nryr_in_state_1,kO1C1*dt,remainder2,T[Cell.unitsInCell[i].nryr_in_state_1]);
    c=constp_binomial(Cell,remainder2,B[Cell.unitsInCell[i].nryr_in_state_1]);

    d=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_1-c,kO1O2*dt/(1-kO1C1*dt),remainder6,T[Cell.unitsInCell[i].nryr_in_state_1-c],ZT);
    nryro1+=Cell.unitsInCell[i].nryr_in_state_1-(c+d);
    nryrc1+=c;
    nryro2+=d;

    c=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_2,kC2C1*dt,remainder3,T[Cell.unitsInCell[i].nryr_in_state_2],ZT);
    d=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_2-c,kC2O2*dt/(1-kC2C1*dt),remainder7,T[Cell.unitsInCell[i].nryr_in_state_2-c],ZT);
    nryrc2+=Cell.unitsInCell[i].nryr_in_state_2-(c+d);
    nryrc1+=c;
    nryro2+=d;

    //c=binomial(Cell.unitsInCell[i].nryr_in_state_3,kO2C2*dt,remainder4,T[Cell.unitsInCell[i].nryr_in_state_3]);
    c=constp_binomial(Cell,remainder4,B[Cell.unitsInCell[i].nryr_in_state_3]);

    d=binomial(Cell,Cell.unitsInCell[i].nryr_in_state_3-c,kO2O1*dt/(1-kO2C2*dt),remainder8,T[Cell.unitsInCell[i].nryr_in_state_3-c],ZT);
    nryro2+=Cell.unitsInCell[i].nryr_in_state_3-(c+d);
    nryrc2+=c;
    nryro1+=d;

    Cell.unitsInCell[i].nryr_in_state_0=nryrc1;
    Cell.unitsInCell[i].nryr_in_state_1=nryro1;
    Cell.unitsInCell[i].nryr_in_state_2=nryrc2;
    Cell.unitsInCell[i].nryr_in_state_3=nryro2;
}

inline void CaculateRyrOpenOriginal(int i,int Ndhpr,int Nryr,double *l_Random,struct cell &Cell, int &nryrc1, int &nryrc2, int &nryro1, int &nryro2,double dt,double kC1O1,double kC1C2,double kO1C1,double kO1O2,double kC2C1,double kC2O2,double kO2C2,double kO2O1)
{
    for ( int k=0;k<Nryr;k++)
    {
        double randomnumber = l_Random[i*(Cell.randU)+Cell.LtypeRandN+k];
        //double randomnumber = l_Random[i*(Ndhpr+Nryr)+Ndhpr+k];
        // double randomnumber = Cell.pRD->randomDataGenerate();
        switch ((Cell.unitsInCell[i]).ryrstate[k])
        {

            case 0:
            if ((randomnumber> (kC1O1+kC1C2)*dt) )
                {(Cell.unitsInCell[i]).ryrstate[k]=0; nryrc1++;break;}
            if ((randomnumber>kC1O1*dt))
                {(Cell.unitsInCell[i]).ryrstate[k]=2; nryrc2++;break;}
            else
                {(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}
            case 1:
            if ((randomnumber>(kO1C1+kO1O2)*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}
            if ((randomnumber>kO1C1*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
            else
                {(Cell.unitsInCell[i]).ryrstate[k] = 0; nryrc1++; break;}
            case 2:
            if ((randomnumber>(kC2C1+kC2O2)*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 2; nryrc2++; break;}
            if ((randomnumber>kC2C1*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
            else
                {(Cell.unitsInCell[i]).ryrstate[k] = 0; nryrc1++; break;}
            case 3:
            if ((randomnumber>(kO2C2+kO2O1)*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 3; nryro2++; break;}
            if ((randomnumber>kO2C2*dt))
                {(Cell.unitsInCell[i]).ryrstate[k] = 1; nryro1++; break;}
            else
                {(Cell.unitsInCell[i]).ryrstate[k] = 2; nryrc2++; break;}
            
        }
    }
}



////////////////1.comp_ina()
void comp_ina( double &gna,double &mtau,double &hftau,double &hstau,double &hsptau,double &jtau,double &jptau,double &Ahf,double &Ahs,double &mss,double &hss,double &hssp,double &jss,double &h,double &hp,double &finap,double &mlss,double &mltau,double &hlss,double &hlssp,double &hltau,double &hlptau,double &gnal,double &finalp,double &ena,double &ina,double &inal,double ***v,int ix,int iy,int iz,double dt, cell &Cell)
{
    if (iso == 1) gna = 2.7*gna;
    mtau = 1.0/(6.765*exp((v[ix][iy][iz]+11.64)/34.77)+8.552*exp(-(v[ix][iy][iz]+77.42)/5.955));
    hftau=1.0/(1.432e-5*exp(-(v[ix][iy][iz]+1.196)/6.285)+6.149*exp((v[ix][iy][iz]+0.5096)/20.27));
    hstau=1.0/(0.009794*exp(-(v[ix][iy][iz]+17.95)/28.05)+0.3343*exp((v[ix][iy][iz]+5.730)/56.66));
    hsptau=3.0*hstau;
    jtau=2.038+1.0/(0.02136*exp(-(v[ix][iy][iz]+100.6)/8.281)+0.3052*exp((v[ix][iy][iz]+0.9941)/38.45));
    jptau=1.46*jtau;
    Ahf=0.99;
    Ahs=1.0-Ahf;
    mss = 1.0/(1.0+exp((-(v[ix][iy][iz]+44.57))/9.871));


    hss =1.0/(1+exp((v[ix][iy][iz]+82.90)/6.086));
    hssp=1.0/(1+exp((v[ix][iy][iz]+89.1)/6.086));
    if (iso == 1)
    {
        hss =1.0/(1+exp((v[ix][iy][iz]+82.90+5)/6.086));
        hssp=1.0/(1+exp((v[ix][iy][iz]+89.1+5)/6.086));
    }
    jss = hss;

    Cell.m = mss-(mss-Cell.m)*exp(-dt/mtau);
    Cell.hf=hss-(hss-Cell.hf)*exp(-dt/hftau);
    Cell.hs=hss-(hss-Cell.hs)*exp(-dt/hstau);
    Cell.hsp=hssp-(hssp-Cell.hsp)*exp(-dt/hsptau);

    h = Ahf*Cell.hf+Ahs*Cell.hs;
    hp=Ahf*Cell.hf+Ahs*Cell.hsp;
    Cell.j = jss-(jss-Cell.j)*exp(-dt/jtau);
    Cell.jp=jss-(jss-Cell.jp)*exp(-dt/jptau);

    finap=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));

    mlss=1.0/(1.0+exp((-(v[ix][iy][iz]+42.85))/5.264));
    mltau = mtau;
    Cell.ml=mlss-(mlss-Cell.ml)*exp(-dt/mltau);

    hlss=1.0/(1.0+exp((v[ix][iy][iz]+87.61)/7.488));
    hlssp=1.0/(1.0+exp((v[ix][iy][iz]+93.81)/7.488));
    hltau=200.0;
    hlptau=3.0*hltau;
    Cell.hl=hlss-(hlss-Cell.hl)*exp(-dt/hltau);
    Cell.hlp=hlssp-(hlssp-Cell.hlp)*exp(-dt/hlptau);

    gnal = 0.0075;
    finalp = finap;

    ena = ((R*temp)/frdy)*log(nao/Cell.nai);

    ina=gna*(v[ix][iy][iz]-ena)*Cell.m*Cell.m*Cell.m*((1.0-finap)*h*Cell.j+finap*hp*Cell.jp);
    inal=gnal*(v[ix][iy][iz]-ena)*Cell.ml*((1.0-finalp)*Cell.hl+finalp*Cell.hlp);
}





/////////////2.comp_ito()
void comp_ito(double &EK,double ***v,int ix,int iy,int iz,double dt,double &Ito,cell &Cell)
{
    
    EK=(R*temp/frdy)*log(ko/Cell.ki);//move above because of EK

    double ass;
    ass=1.0/(1.0+exp((-(v[ix][iy][iz]-14.34))/14.82));
    double atau;
    atau=1.0515/(1.0/(1.2089*(1.0+exp(-(v[ix][iy][iz]-18.4099)/29.3814)))+3.5/(1.0+exp((v[ix][iy][iz]+100.0)/29.3814)));

    Cell.a=ass-(ass-Cell.a)*exp(-dt/atau);

    double iss;
    iss=1.0/(1.0+exp((v[ix][iy][iz]+43.94)/5.711));
    double delta_epi=1.0;
    double iftau,istau;

    iftau=4.562+1/(0.3933*exp((-(v[ix][iy][iz]+100.0))/100.0)+0.08004*exp((v[ix][iy][iz]+50.0)/16.59));
    istau=23.62+1/(0.001416*exp((-(v[ix][iy][iz]+96.52))/59.05)+1.780e-8*exp((v[ix][iy][iz]+114.1)/8.079));

    iftau*=delta_epi;
    istau*=delta_epi;

    double AiF,AiS;
    AiF=1.0/(1.0+exp((v[ix][iy][iz]-213.6)/151.2));
    AiS=1.0-AiF;

    Cell.iF=iss-(iss-Cell.iF)*exp(-dt/iftau);
    Cell.iS=iss-(iss-Cell.iS)*exp(-dt/istau);

    double i_ito;
    i_ito=AiF*Cell.iF+AiS*Cell.iS;

    double assp;
    assp=1.0/(1.0+exp((-(v[ix][iy][iz]-24.34))/14.82));
    Cell.ap=assp-(assp-Cell.ap)*exp(-dt/atau);

    double dti_develop,dti_recover;
    dti_develop=1.354+1.0e-4/(exp((v[ix][iy][iz]-167.4)/15.89)+exp(-(v[ix][iy][iz]-12.23)/0.2154));
    dti_recover=1.0-0.5/(1.0+exp((v[ix][iy][iz]+70.0)/20.0));

    double tiFp,tiSp;
    tiFp=dti_develop*dti_recover*iftau;
    tiSp=dti_develop*dti_recover*istau;

    Cell.iFp=iss-(iss-Cell.iFp)*exp(-dt/tiFp);
    Cell.iSp=iss-(iss-Cell.iSp)*exp(-dt/tiSp);

    double ip;
    ip=AiF*Cell.iFp+AiS*Cell.iSp;

    double Gto = 0.02;
    double fItop;
    fItop=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));

    Ito=Gto*(v[ix][iy][iz]-EK)*((1.0-fItop)*Cell.a*i_ito+fItop*Cell.ap*ip);
}


/////////3.comp_ikr
void comp_ikr(double &IKr,double ***v,int ix,int iy,int iz,double dt,double EK,cell &Cell)
{
    double xrss=1.0/(1.0+exp((-(v[ix][iy][iz]+8.337))/6.789));
    double txrf=12.98+1.0/(0.3652*exp((v[ix][iy][iz]-31.66)/3.869)+4.123e-5*exp((-(v[ix][iy][iz]-47.78))/20.38));
    double txrs=1.865+1.0/(0.06629*exp((v[ix][iy][iz]-34.70)/7.355)+1.128e-5*exp((-(v[ix][iy][iz]-29.74))/25.94));
    double Axrf=1.0/(1.0+exp((v[ix][iy][iz]+54.81)/38.21));
    double Axrs=1.0-Axrf;
    Cell.xrf=xrss-(xrss-Cell.xrf)*exp(-dt/txrf);
    Cell.xrs=xrss-(xrss-Cell.xrs)*exp(-dt/txrs);
    double xr=Axrf*Cell.xrf+Axrs*Cell.xrs;
    double rkr=1.0/(1.0+exp((v[ix][iy][iz]+55.0)/75.0))*1.0/(1.0+exp((v[ix][iy][iz]-10.0)/30.0));
    double GKr=0.046; GKr = 1*GKr;

    IKr=GKr*sqrt(ko/5.4)*xr*rkr*(v[ix][iy][iz]-EK);
}


//////////4.comp_iks()
void comp_iks(double ***v,int ix,int iy,int iz,double dt,double &IKs,cell &Cell)
{
    double EKs=(R*temp/frdy)*log((ko+0.01833*nao)/(Cell.ki+0.01833*Cell.nai));
    double xs1ss=1.0/(1.0+exp((-(v[ix][iy][iz]+11.60))/8.932));
    double txs1=817.3+1.0/(2.326e-4*exp((v[ix][iy][iz]+48.28)/17.80)+0.001292*exp((-(v[ix][iy][iz]+210.0))/230.0));
    Cell.xs1=xs1ss-(xs1ss-Cell.xs1)*exp(-dt/txs1);
    double xs2ss=xs1ss;
    double txs2=1.0/(0.01*exp((v[ix][iy][iz]-50.0)/20.0)+0.0193*exp((-(v[ix][iy][iz]+66.54))/31.0));
    Cell.xs2=xs2ss-(xs2ss-Cell.xs2)*exp(-dt/txs2);
    double KsCa=1.0+0.6/(1.0+pow(3.8e-5/3.8e-5,1.4));
    double GKs=0.0034;
    if (iso == 1) GKs = 3.2*GKs;

    IKs=GKs*KsCa*Cell.xs1*Cell.xs2*(v[ix][iy][iz]-EKs);
}

/////////5.comp_ik1()
void comp_ik1(double &EK,double &IK1,double ***v,int ix,int iy,int iz,double dt,cell &Cell)
{
    double xk1ss=1.0/(1.0+exp(-(v[ix][iy][iz]+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
    double txk1=122.2/(exp((-(v[ix][iy][iz]+127.2))/20.36)+exp((v[ix][iy][iz]+236.8)/69.33));
    Cell.xk1=xk1ss-(xk1ss-Cell.xk1)*exp(-dt/txk1);
    double rk1=1.0/(1.0+exp((v[ix][iy][iz]+105.8-2.6*ko)/9.493));
    double GK1=0.1908;
//if (iso == 1)
 GK1 = 0.4*0.1908;
    IK1=GK1*sqrt(ko)*rk1*Cell.xk1*(v[ix][iy][iz]-EK);
}


/////////6.comp_inak()
void comp_inak(double &INaK,double ***v,int ix,int iy,int iz,cell &Cell)
{
    
    double Knai=Knai0*exp((delta*v[ix][iy][iz]*frdy)/(3.0*R*temp));
    if (iso == 1) Knai = 0.7*Knai;
    double Knao=Knao0*exp(((1.0-delta)*v[ix][iy][iz]*frdy)/(3.0*R*temp));
  
    double P=eP/(1.0+H/Khp+Cell.nai/Knap+Cell.ki/Kxkur);
    double a1=(k1p*pow(Cell.nai/Knai,3.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
    double b1=k1m*MgADP;
    double a2=k2p;
    double b2=(k2m*pow(nao/Knao,3.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
    double a3=(k3p*pow(ko/Kko,2.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
    double b3=(k3m*P*H)/(1.0+MgATP/Kmgatp);
    double a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
    double b4=(k4m*pow(Cell.ki/Kki,2.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
    double x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
    double x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
    double x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
    double x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
    double E1=x1/(x1+x2+x3+x4);
    double E2=x2/(x1+x2+x3+x4);
    double E3=x3/(x1+x2+x3+x4);
    double E4=x4/(x1+x2+x3+x4);
    //double zk=1.0;
    double JnakNa=3.0*(E1*a3-E2*b3);
    double JnakK=2.0*(E4*b1-E3*a1);
    //double Pnak=30;

    INaK=Pnak*(zna*JnakNa+zk*JnakK);
}


////////7.comp_inab()
void comp_inab(double &IKb,double EK,double vffrt,double vfrt,double &INab,double ***v,int ix,int iy,int iz,cell &Cell)
{
    double xkb=1.0/(1.0+exp(-(v[ix][iy][iz]-14.48)/18.34));
    double GKb=0.003;
    //  if (iso == 1)///?
    //       GKb = 2.5*GKb;////?

    IKb=GKb*xkb*(v[ix][iy][iz]-EK);

    double PNab=3.75e-10;
    INab=PNab*vffrt*(Cell.nai*exp(vfrt)-nao)/(exp(vfrt)-1.0);
}
//////////////////////######################################################################################################################################################
/////////////////////////#################################################################################################################################################





/////////////////////////////#################################8.comp_calc_dyad()
//#####################################################################################################
void comp_calc_dyad(double &jrel,double &ilca,double &ilcana,double &ilcak,double &inaca,double &icab,double &ipca,double &icat,double &inacass, double &cai,double &casub,double &cadyad,double &jsr,double &nsr, double &CaMKb_cell,double &CaMKt_cell,double ***v,int ix,int iy,int iz,double dt,double &km2n,double PCa,double PCap,double PCaNa,double PCaK,double PCaNap,double PCaKp,double &PhiCaNa,double vffrt,double vfrt,double &PhiCaK,double acap,double Ntotdyads,double vss_dyad,double vcyt_dyad,double &anca,int Ndhpr,double **T,double *ZT,double **B,double &PhiCaL,int Nryr,double t,int bcl,double vmyo,double vjsr_dyad,double vnsr_dyad,double kmtrpn,cell &Cell)
{
    //############################8.comp_calc_dyad()#####################################/// 
    int beats = Cell.beats;

    int randU = Cell.randU;

    //int Nx = 100; int Ny=1; int Nz=1;
    int Nx = Cell.unitNum_x;
    int Ny = Cell.unitNum_y;
    int Nz = Cell.unitNum_z;
    //int Nvox = 1;
    int Ndyads = Cell.Ndyads;

    int Nx_diff = (Nx-1)*Nvox + 1;
    int Ny_diff = (Ny-1)*Nvox + 1;
    int Nz_diff = (Nz-1)*Nvox + 1;
    
    double *casstemp = Cell.casstemp;
    double *cansrtemp = Cell.cansrtemp;
    double *cacyttemp = Cell.cacyttemp;
    double *cassdiff = Cell.cassdiff;
    double *cansrdiff = Cell.cansrdiff;
    double *cacytdiff = Cell.cacytdiff;

    
    double nodhpr,noryr,nmodeca_total,nivf_total,nivs_total;
    double factivedyad;
    
    ilca = nodhpr = jrel = noryr = nmodeca_total = nivf_total = nivs_total = 0;
    ilcana=ilcak=0;
    inaca = icab = ipca = icat = inacass = 0;
    cai = casub = cadyad = jsr = nsr = 0;
    Cell.CaMKa_cell = CaMKb_cell = CaMKt_cell = 0;
    factivedyad  = 0;


    //added by qianglan to generate random data using intel MKL lib
    double * l_Random;
    l_Random = Cell.l_Random;
    unsigned long rand_n = Cell.rand_n;


    
//#ifdef TIME
    double T_OStart,T_IStart,T_Oend,T_Iend;
//#endif

#ifdef TIME
    T_OStart = timing();
#endif

    vdRngUniform(0, Cell.Randomstream, rand_n, l_Random, 0.0, 1.0-(1.2e-12));
#ifdef TIME
    T_Oend = timing();
    Cell.T_randomG += T_Oend-T_OStart;
#endif



#ifdef TIME
    T_OStart = timing();
#endif
    
    double hca=exp((qca*v[ix][iy][iz]*frdy)/(R*temp));
    double hna=exp((qna*v[ix][iy][iz]*frdy)/(R*temp));
    double h1=1+Cell.nai/kna3*(1+hna);
    double h2=(Cell.nai*hna)/(kna3*h1);
    double h3=1.0/h1;
    double h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
    double h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
    double h6=1.0/h4;
    double h7=1.0+nao/kna3*(1.0+1.0/hna);
    double h8=nao/(kna3*hna*h7);
    double h9=1.0/h7;
    double h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
    double h11=nao*nao/(h10*kna1*kna2);
    double h12=1.0/h10;
    double k1=h12*cao*kcaon;
    double k2=kcaoff;
    double K3p=h9*wca;//rename k3p to K3p
    double k3pp=h8*wnaca;
    double k3=K3p+k3pp;//replace k3p to K3p
    double K4p=h3*wca/hca;//rename k4p to K4p
    double k4pp=h2*wnaca;
    double k4=K4p+k4pp;//replace k4p to K4p
    double k5=kcaoff;
    double k7=h5*h2*wna;
    double k8=h8*h11*wna;
    double X4=k2*k8*(k4+k5)+k3*k5*(k1+k8);//rename x4 to X4
    km2n=Cell.jca;
    double KKnmn = k2n/km2n;
    double Kmndt = km2n*dt;



    double if1,if2,if3,if4,if5,if6,if7,if8;
    if2 = Cell.d*Cell.f;
    if3 = Cell.d*Cell.jca*Cell.fca;
    if4 = Cell.d*Cell.fp;
    if5 = Cell.d*Cell.jca*Cell.fcap;
    

    ////////////////////////////move from inside of dyad loop
    ///////////////// 

    

    PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
    PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);//step 2: from 87s to 84s
   

   double caoM = 0.341*cao;
   double PhiCaLP2 = 1e-3*0.01*exp(2.0*vfrt);
   double PhiCaLP1 = 4.0*vffrt/(exp(2.0*vfrt)-1.0);//step3: from 84s to 83s

    double jlca_dyadP = -acap*1e3/(2*vds*frdy*Ntotdyads);






      double cassP1 = acap/(Ntotdyads*zca*frdy*vss_dyad);
    double cacytP = acap/(Ntotdyads*zca*frdy*vcyt_dyad); 
    double cassP2 = 1e-3*(vds/vss_dyad);

    double x1P=k2*k4*k7+k5*k7*(k2+k3);
    double x2P=k1*k7*(k4+k5); 
    double x2P2=k4*(k1+k8);
    double x3P= k8*(k2+k3);
    double x3P2= k1*k3*k7;

    double EP1= k2*k4+x2P2+k1*k3+x3P;
    double EP2= x1P+x2P+x3P2+X4;


    ////////////////////////////////////////////////////
    /////////////////////////////////////////////
#ifdef TIME
    T_Oend = timing();
    Cell.T_prepare += T_Oend-T_OStart;
#endif
   

    for (int i=0; i<Ndyads;i++)
    {
#ifdef TIME
    T_OStart = timing();
#endif
        ////%%%%%%8.1 comp_ical_dyad()%%%%%%%%%%%//
#ifdef TIME
        T_IStart = timing();
#endif
        
        anca=1.0/(k2n/km2n+pow(1.0+Kmn/(0.001*Cell.unitsInCell[i].cads),4.0));
        Cell.nca[i]=anca*k2n/km2n-(anca*k2n/km2n-Cell.nca[i])*exp(-km2n*dt);
        int nLCC_dyad_np = 0;
        int nLCC_dyad_p = 0;
	

        double ab5 = (1-(1.0/(1.0+KmCaMK/Cell.CaMKa[i])))*((1-Cell.nca[i])*if2 + Cell.nca[i]*if3);
	double cd5 = (1.0/(1.0+KmCaMK/Cell.CaMKa[i]))*((1-Cell.nca[i])*if4+Cell.nca[i]*if5);//step 2: from 87s to 82s


        /*for (int k = 0;k<Ndhpr;k++)
        {       
            //double random = l_Random[i*(Ndhpr+Nryr)+k];
            double random = l_Random[i*randU+k];
            if ( random<= ab5)
                nLCC_dyad_np ++;
            else if ( random<=(ab5+cd5))//step 2
                nLCC_dyad_p++;
        } */

        double random1 = l_Random[i*randU+0];
        double random2 = l_Random[i*randU+1];
        nLCC_dyad_np = binomial(Cell,Ndhpr,ab5,random1,T[Ndhpr],ZT);
        nLCC_dyad_p = binomial(Cell,Ndhpr,cd5,random2,T[Ndhpr],ZT);


#ifdef TIME
        T_Iend = timing();
        Cell.T_L_Channel+=T_Iend-T_IStart;
#endif


#ifdef TIME
        T_IStart = timing();
#endif
        
        PhiCaL= (Cell.unitsInCell[i].cads*PhiCaLP2-caoM)*PhiCaLP1;///steps3 

 

        double ilca_dyad = (PhiCaL/Ndhpr)*(PCa*(nLCC_dyad_np*1.0) + PCap*(nLCC_dyad_p*1.0));
	double icana_dyad = (PhiCaNa/Ndhpr)*(PCaNa*(nLCC_dyad_np*1.0) + PCaNap*(nLCC_dyad_p*1.0));	//uA/uF
	double icak_dyad = (PhiCaK/Ndhpr)*(PCaK*(nLCC_dyad_np*1.0) + PCaKp*(nLCC_dyad_p*1.0));	//uA/uF/////////////////////step5:no improvment


	ilca += ilca_dyad;	//uA/uF
        ilcana += icana_dyad;
        ilcak += icak_dyad;//////////////////////////////////////////////step 4
  
	double jlca_dyad = jlca_dyadP*ilca_dyad;	//uM/ms///////////////////////////////////////step6



#ifdef TIME
        T_Iend = timing();
        Cell.T_ical_dyad1 += T_Iend-T_IStart;
#endif

        ////%%%%%%8.1 comp_ical_dyad()%%%%%%%%%%%//
#ifdef TIME
    T_Oend = timing();
    Cell.T_ical_dyad += T_Oend-T_OStart;
#endif


#ifdef TIME
    T_OStart = timing();
#endif
        //////////%%%%%%%8.2 comp_irel_dyad()%%%%%%%%%%%//////
        int nryropen = 0;
        Cell.nactive[i] = 0;

#ifdef TIME
        T_IStart = timing();
#endif
        
        nryropen = Cell.nryro1;
#ifdef TIME
        T_Iend = timing();
        Cell.T_irel1+=T_Iend-T_IStart;
#endif


#ifdef TIME
        T_IStart = timing();
#endif
        if (  (1.0*nryropen/Nryr)>0.2) {Cell.trel[i] = t; Cell.nactive[i] = 1;}

        double  kC1O1 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Km,4)));
        double	kO1C1 = 0.5;
        double	kC2O2 = 3.*(pow(Cell.unitsInCell[i].cads,4)/(pow(Cell.unitsInCell[i].cads,4) + pow(Kmcsqn,4)));
        double	kO2C2 = kO1C1;

        //calsequestrin
        double	csqn = csqnbar*pow(kmcsqn,8)/(pow(Cell.unitsInCell[i].cajsr,8) + pow(kmcsqn,8) );
        double	csqnca = csqnbar - csqn;

        double kC1C2 = 1*(csqn/csqnbar);

        double tref;

        
        tref = t-Cell.trel[i]-0.98*bcl;
        if (t>(beats-8)*bcl && t<(beats-8)*bcl + 500) tref = t-Cell.trel[i]-325;

        double kC2C1 = 1*(csqnca/csqnbar)*1/(1+exp(-tref/0.001));//*(t-Cell.trel[i]-tref);

        double	kO1O2 = kC1C2;
        double	kO2O1 = kC2C1;

        int nryrc1,nryro1,nryrc2,nryro2;
        nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;

#ifdef TIME
        T_Iend = timing();
        Cell.T_irel2+=T_Iend-T_IStart;
#endif



//#ifdef TIME
         T_IStart = timing();
//#endif

        //CaculateRyrOpenBinomialC(i,Cell,nryrc1,nryrc2,nryro1,nryro2,dt, kC1O1, kC1C2, kO1C1, kO1O2, kC2C1,kC2O2,kO2C2,kO2O1,gen);

        //CaculateRyrOpenBinomialMKL(i,Cell,nryrc1,nryrc2,nryro1,nryro2,dt, kC1O1, kC1C2, kO1C1, kO1O2, kC2C1,kC2O2,kO2C2,kO2O1);

        CaculateRyrOpenBinomial(i,Ndhpr,Nryr,l_Random,Cell,nryrc1,nryrc2,nryro1,nryro2,dt,kC1O1,kC1C2,kO1C1,kO1O2,kC2C1,kC2O2,kO2C2,kO2O1,T,B,ZT);
        //CaculateRyrOpenBinomialVec(i,Ndhpr,Nryr,l_Random,Cell,nryrc1,nryrc2,nryro1,nryro2,dt, kC1O1, kC1C2, kO1C1, kO1O2, kC2C1,kC2O2,kO2C2,kO2O1,T);

    
        //CaculateRyrOpenOriginal(i,Ndhpr,Nryr,l_Random,Cell,nryrc1,nryrc2,nryro1,nryro2,dt,kC1O1,kC1C2,kO1C1,kO1O2,kC2C1,kC2O2,kO2C2,kO2O1);

//#ifdef TIME
        T_Iend = timing();
        Cell.T_RyrOpen+=T_Iend-T_IStart;
//#endif

#ifdef TIME
      T_IStart = timing();
#endif
        //	nryropen = nryro1 + nryro2;
        nryropen = nryro1;
        
        if(i==Ndyads/2)
        {
            Cell.nryrc1=nryrc1;
            Cell.nryro1=nryro1;
            Cell.nryrc2=nryrc2;
            Cell.nryro2=nryro2;

            Cell.kC1O1 = kC1O1*dt;
            Cell.kC1C2 = kC1C2*dt;
            Cell.kO1C1 = kO1C1*dt;
            Cell.kO1O2 = kO1O2*dt;
            Cell.kC2C1 = kC2C1*dt;
            Cell.kC2O2 = kC2O2*dt;
            Cell.kO2C2 = kO2C2*dt;
            Cell.kO2O1 = kO2O1*dt;
        }

        //if (t> (beats-3)*bcl && t<(beats-3)*bcl+20) nryropen = Nryr;

        //if (t>(beats-5)*bcl) Dx = 0;




        Cell.Poryr[i] =  1.0*nryropen/Nryr;

        double jrel_dyad = dryr*nryropen*(Cell.unitsInCell[i].cajsr*1e3-Cell.unitsInCell[i].cads); //uM/ms
        jrel += jrel_dyad*(vds/vmyo)*(Ntotdyads/Ndyads); //mM/s

        noryr += nryropen;
        
#ifdef TIME
        T_Iend = timing();
        Cell.T_irel3+=T_Iend-T_IStart;
#endif
        //////////////////////////%%%%%%%8.2 comp_irel_dyad()%%%%%%%%%%%///////////////////////

#ifdef TIME
    T_Oend = timing();
    Cell.T_irel_dyad += T_Oend-T_OStart;
#endif


#ifdef TIME
    T_OStart = timing();
#endif
        //////////%%%%%%%8.3 comp_conc_dyad()%%%%%%%%%%%//////
        double jcadiff_ds_ss = (Cell.unitsInCell[i].cads-Cell.unitsInCell[i].cass*1e3)/tauefflux;
        double jefflux = jcadiff_ds_ss;

        Cell.unitsInCell[i].cads =  (jrel_dyad + jlca_dyad + Cell.unitsInCell[i].cass*1e3/tauefflux)*tauefflux;

        double	bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+Cell.unitsInCell[i].cass),2))) + ((bslbar*kmbsl)/(pow((kmbsl+Cell.unitsInCell[i].cass),2))));
        double jcadiff_ss_cyt = (Cell.unitsInCell[i].cass-Cell.unitsInCell[i].cacyt)/taudiff;

        // local submembrane space concentration
        
Cell.unitsInCell[i].cass = Cell.unitsInCell[i].cass + (-bsubspace*(((-2*Cell.inacass_dyad)*cassP1) + jcadiff_ss_cyt - jcadiff_ds_ss*cassP2 ))*dt;  //////step7  no obvious improvement
        


        double jrefill = (Cell.unitsInCell[i].cansr - Cell.unitsInCell[i].cajsr)/taurefill;

        double	bjsr = 1/( 1 + (csqnbar*kmcsqn)/pow((kmcsqn + Cell.unitsInCell[i].cajsr),2));

        Cell.unitsInCell[i].cajsr = Cell.unitsInCell[i].cajsr + bjsr*(-(jrel_dyad*(vds/vjsr_dyad))/1000. + jrefill)*dt;
        
        double Jupnp;
        double Jupp;

        if (iso == 1)
        {
            Jupnp=0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.54*0.00092);
             Jupp=2.75*0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.54*(0.00075));
        }
        else
        {
            Jupnp=0.004375*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.00092);
             Jupp=0.01203125*Cell.unitsInCell[i].cacyt/(Cell.unitsInCell[i].cacyt+0.00075);
        }

        double fJupp=(1.0/(1.0+KmCaMK/Cell.CaMKa[i]));
        double ileak=0.0039375*Cell.unitsInCell[i].cansr/15.0;
        double iup=((1.0-fJupp)*Jupnp+fJupp*Jupp);

        Cell.unitsInCell[i].cansr = Cell.unitsInCell[i].cansr + (iup - ileak - jrefill*(vjsr_dyad/vnsr_dyad))*dt;

// local CaMK bound, active and trapped concentration
        Cell.CaMKb[i] = CaMKo*(1.0-Cell.CaMKt[i] )/(1.0+KmCaM/Cell.unitsInCell[i].cass);
        Cell.CaMKa[i] = Cell.CaMKb[i] + Cell.CaMKt[i];
        Cell.CaMKt[i] += dt*(aCaMK*Cell.CaMKb[i]*(Cell.CaMKb[i]+Cell.CaMKt[i])-bCaMK*Cell.CaMKt[i]);//move 

        double icab_dyad=0,ipca_dyad=0,inaca_dyad=0;//move to top
        


#ifdef TIME
        T_IStart = timing();
#endif
        /////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////
       // calcium currents 
        double k6=h6*Cell.unitsInCell[i].cacyt*kcaon;

      

	double x1=x1P + k2*k4*k6;
        double x2=x2P + x2P2*k6;
        double x3=x3P2+k1*k3*k6+ x3P*k6;
        
        double Edenom=k6*EP1+EP2;


        double allo=1.0/(1.0+pow(KmCaAct/Cell.unitsInCell[i].cacyt,2.0));
        
	double JncxNa=(3.0*(X4*k7-x1*k8)+x3*k4pp-x2*k3pp)/Edenom;
        double JncxCa=(x2*k2-x1*k1)/Edenom;


        double Gncx=0.0008;//////should not be moved outside
        //test 
        if (t>(beats-8)*bcl && t<(beats-8)*bcl + 500) Gncx = 3*Gncx;
        
        inaca_dyad=0.8*Gncx*allo*(zna*JncxNa+zca*JncxCa);
        k6=h6*Cell.unitsInCell[i].cass*kcaon;

  
	x1=x1P + k2*k4*k6;
        x2=x2P + x2P2*k6;
        x3=x3P2+k1*k3*k6+ x3P*k6;
    
	Edenom=k6*EP1+EP2;

        allo=1.0/(1.0+pow(KmCaAct/Cell.unitsInCell[i].cass,2.0));
        
	JncxNa=(3.0*(X4*k7-x1*k8)+x3*k4pp-x2*k3pp)/Edenom;
        JncxCa=(x2*k2-x1*k1)/Edenom;

        Cell.inacass_dyad=0.2*Gncx*allo*(zna*JncxNa+zca*JncxCa);
        double PCab = 2.5e-8;
        icab_dyad=PCab*4.0*vffrt*(Cell.unitsInCell[i].cacyt*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);

        double GpCa = 0.0005;
        ipca_dyad=GpCa*Cell.unitsInCell[i].cacyt/(0.0005+Cell.unitsInCell[i].cacyt);



        //inaca += (inaca_dyad)/Ndyads;
        //inacass += (Cell.inacass_dyad)/Ndyads;
        //ipca += (ipca_dyad)/Ndyads;
        //icab += (icab_dyad)/Ndyads;////////////////////////step 9  obvious improvement from 82 to 79s
        inaca += inaca_dyad;
        inacass += Cell.inacass_dyad;
        ipca += ipca_dyad;
        icab += icab_dyad;

        /////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////
#ifdef TIME
        T_Iend = timing();
        Cell.T_ica_dyad+=T_Iend-T_IStart;
#endif



    	double bmyo;
        bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+Cell.unitsInCell[i].cacyt),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+Cell.unitsInCell[i].cacyt),2))));

        // local myoplasmic Ca concentration
        
        Cell.unitsInCell[i].cacyt = Cell.unitsInCell[i].cacyt + (-bmyo*(((icab_dyad+ipca_dyad-2*inaca_dyad)*cacytP) + (iup-ileak)*(vnsr_dyad/vcyt_dyad) - jcadiff_ss_cyt*(vss_dyad/vcyt_dyad) ))*dt;



        //////////%%%%%%%8.3 comp_conc_dyad()%%%%%%%%%%%//////
#ifdef TIME
        T_Oend = timing();
        Cell.T_conc_dyad += T_Oend-T_OStart;
#endif
    }

	ilca  = ilca/Ndyads;   //uA/uF
        ilcana = ilcana/Ndyads;
        ilcak = ilcak/Ndyads;

	inaca = inaca/Ndyads;
        inacass = inacass/Ndyads;
        ipca = ipca/Ndyads;
        icab = icab/Ndyads;


    //vslDeleteStream(&Randomstream);


#ifdef TIME
    T_OStart = timing();
#endif
    /////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////
    for (int i = 0; i<Ndyads; i++) //xfer ca from reaction to diffusion space
    {
        int iix,iiy,iiz;
        iiz= i/(Nx*Ny);
        iiy = (i - iiz*(Nx*Ny))/Nx;
        iix = i -iiz*(Nx*Ny) - iiy*Nx;
        cacytdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cacyt;
        cansrdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cansr;
        cassdiff[ (Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cass;
        
        cacyttemp[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cacyt;
        cansrtemp[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cansr;
        casstemp[ (Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)] = Cell.unitsInCell[i].cass;
    }


    for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
    {
        // temp variables to compute ca in diffusion space
        /*cacyttemp[i] = cacytdiff[i];
        cansrtemp[i] = cansrdiff[i];
        casstemp[i] = cassdiff[i];*/

        int iz,iy,ix;

        iz= i/(Nx_diff*Ny_diff);    			//3D mapping
        iy = (i - iz*(Nx_diff*Ny_diff))/Nx_diff; 		//3D mapping
        ix = i -iz*(Nx_diff*Ny_diff) - iy*Nx_diff;	//3D mapping

        double bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+cassdiff[i]),2))) + ((bslbar*kmbsl)/(pow((kmbsl+cassdiff[i]),2))));
        double bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+cacytdiff[i]),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+cacytdiff[i]),2))));

        if (ix>0 && ix<Nx_diff-1)
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-1] + cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-1] + cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
            cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-1] + cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
        }

        if (iy>0 && iy<Ny_diff-1)
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-Nx_diff] + cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-Nx_diff] + cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
            cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff] + cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
        }

        if (iz>0 && iz<Nz_diff-1)
        {
            cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*(cacytdiff[i-Nx_diff*Ny_diff] + cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
            casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*(cassdiff[i-Nx_diff*Ny_diff] + cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
            cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff*Ny_diff] + cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
        }

        if (ix==0 && ix!=(Nx_diff-1))
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
        }
        if (ix == Nx_diff-1 && ix != 0)
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-1] -2*cacytdiff[i])*dt/(dx*dx);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-1] - 2*cassdiff[i])*dt/(dx*dx);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-1] -2*cansrdiff[i])*dt/(dx*dx);
        }
        if (iy == 0 && iy != (Ny_diff-1))
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
        }
        if (iy == Ny_diff-1 && iy != 0)
        {
            cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
            casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
        }

        if (iz == 0 && iz != (Nz_diff-1))
        {
            cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
            casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
        }
        if (iz == Nz_diff-1 && iz != 0)
        {
            cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
            casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
            cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
        }

    }

    double *tempPtr;
    tempPtr = cacyttemp;
    cacyttemp = cacytdiff;
    cacytdiff = tempPtr;
    
    tempPtr = casstemp;
    casstemp = cassdiff;
    cassdiff = tempPtr;
    
    tempPtr = cansrtemp;
    cansrtemp = cansrdiff;
    cansrdiff = tempPtr;


       // for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
       // {
       //     cacytdiff[i] = cacyttemp[i]; cassdiff[i] = casstemp[i]; cansrdiff[i] = cansrtemp[i];
       // }


        for (int i = 0; i<Ndyads; i++) //xfer ca from diffusion to reaction space
        {
            int iix,iiy,iiz;
                iiz= i/(Nx*Ny);
                iiy = (i - iiz*(Nx*Ny))/Nx;
                iix = i -iiz*(Nx*Ny) - iiy*Nx;

            Cell.unitsInCell[i].cacyt = cacytdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
            Cell.unitsInCell[i].cansr = cansrdiff[(Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
            Cell.unitsInCell[i].cass = cassdiff[ (Nvox*iiz)*Nx_diff*Ny_diff + (Nvox*iiy)*Nx_diff + (Nvox*iix)];
        }
    /////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////

#ifdef TIME
    T_Oend = timing();
    Cell.T_conc_diff += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    for (int i=0; i<Ndyads; i++)
    {
        cai += Cell.unitsInCell[i].cacyt;
        casub += Cell.unitsInCell[i].cass;
        cadyad += Cell.unitsInCell[i].cads;
        jsr += Cell.unitsInCell[i].cajsr;
        nsr += Cell.unitsInCell[i].cansr;
        factivedyad += Cell.nactive[i];
        Cell.CaMKa_cell += Cell.CaMKa[i];
           CaMKb_cell += Cell.CaMKb[i];
           CaMKt_cell += Cell.CaMKt[i];
    }

    cai = cai/Ndyads;
    casub = casub/Ndyads;
    cadyad = cadyad/Ndyads;
    jsr = jsr/Ndyads;
    nsr = nsr/Ndyads;
    factivedyad = factivedyad/Ndyads;
    Cell.CaMKa_cell = Cell.CaMKa_cell/Ndyads;
    CaMKb_cell = CaMKb_cell/Ndyads;
    CaMKt_cell = CaMKt_cell/Ndyads; 

#ifdef TIME
    T_Oend = timing();
    Cell.T_acculate += T_Oend-T_OStart;
#endif

        Cell.cadyad = cadyad;
        Cell.casub = casub;
        Cell.cai = cai;
        Cell.nsr = nsr;
        Cell.jsr = jsr;

}
///////////////////////////////////////################################################################################################











//////////////////////////////////######################################9.comp_ical()###########################
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void comp_ical(double ***v,int ix,int iy,int iz,double dt,double &km2n,double &anca,double &PhiCaL,double &PhiCaNa,double &PhiCaK,double &ICaL_print,double cadyad,double vffrt,double vfrt,double PCa,double PCap,double PCaNa,double PCaNap,double PCaK,double PCaKp,cell &Cell)
{
    double dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940))/4.230));
    if (iso == 1) dss=1.0/(1.0+exp((-(v[ix][iy][iz]+3.940+14))/4.230));

    double td=0.6+1.0/(exp(-0.05*(v[ix][iy][iz]+6.0))+exp(0.09*(v[ix][iy][iz]+14.0)));
    Cell.d=dss-(dss-Cell.d)*exp(-dt/td);
     ////////////////


    double fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58)/3.696));
    if (iso == 1) fss=1.0/(1.0+exp((v[ix][iy][iz]+19.58+8)/3.696));

    double tff=7.0+1.0/(0.0045*exp(-(v[ix][iy][iz]+20.0)/10.0)+0.0045*exp((v[ix][iy][iz]+20.0)/10.0));
    double tfs=1000.0+1.0/(0.000035*exp(-(v[ix][iy][iz]+5.0)/4.0)+0.000035*exp((v[ix][iy][iz]+5.0)/6.0));
    double Aff=0.6;
    double Afs=1.0-Aff;
    Cell.ff=fss-(fss-Cell.ff)*exp(-dt/tff);
    Cell.fs=fss-(fss-Cell.fs)*exp(-dt/tfs);
     Cell.f=Aff*Cell.ff+Afs*Cell.fs;
    double fcass=fss;
    double tfcaf=7.0+1.0/(0.04*exp(-(v[ix][iy][iz]-4.0)/7.0)+0.04*exp((v[ix][iy][iz]-4.0)/7.0));
    double tfcas=100.0+1.0/(0.00012*exp(-v[ix][iy][iz]/3.0)+0.00012*exp(v[ix][iy][iz]/7.0));
    double Afcaf=0.3+0.6/(1.0+exp((v[ix][iy][iz]-10.0)/10.0));
    double Afcas=1.0-Afcaf;
    Cell.fcaf=fcass-(fcass-Cell.fcaf)*exp(-dt/tfcaf);
    Cell.fcas=fcass-(fcass-Cell.fcas)*exp(-dt/tfcas);
    Cell.fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
    double tjca = 75.0;
    Cell.jca=fcass-(fcass-Cell.jca)*exp(-dt/tjca);
    double tffp=2.5*tff;
    Cell.ffp=fss-(fss-Cell.ffp)*exp(-dt/tffp);
    Cell.fp=Aff*Cell.ffp+Afs*Cell.fs;
    double tfcafp=2.5*tfcaf;
    Cell.fcafp=fcass-(fcass-Cell.fcafp)*exp(-dt/tfcafp);
    Cell.fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;
    
   //for plotting ICaL only//
     km2n=Cell.jca*1.0;
     anca=1.0/(k2n/km2n+pow(1.0+Kmn/cadyad,4.0));
    Cell.nca_print = anca*k2n/km2n-(anca*k2n/km2n-Cell.nca_print)*exp(-km2n*dt);
    double fICaLp=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));
    
      PhiCaL=4.0*vffrt*(1e-3*cadyad*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
      PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
      PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

     ICaL_print=(1.0-fICaLp)*PCa*PhiCaL*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCap*PhiCaL*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaNa_print=(1.0-fICaLp)*PCaNa*PhiCaNa*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaNap*PhiCaNa*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaK_print=(1.0-fICaLp)*PCaK*PhiCaK*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaKp*PhiCaK*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
}








