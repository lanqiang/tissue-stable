#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>

//#include <random>

#include <omp.h>
#include <malloc.h>
#include <stdlib.h>


#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"
#include "UpdateGhost.h"
#include "postprocess.h"
#include "printInfo.h"
#include <time.h>

#define __PAPI__

#ifdef __PAPI__
    #include <papi.h>
#endif


#define Ntotdyads 10000


using namespace std;
//

double ***v, ***vtemp,***dvdt,***dvdtclock;
double ***flagArray;
double ***dtstep;
double t,dt,d_x,Dx;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=500;

ofstream fout;
ofstream f0out;
ostringstream file;
ostringstream file0;

ofstream fcalcium ("calcium");      // Output Data file for whole-cell variables
ofstream fdyad ("dyad");    // Output Data file for dyadic variables
ofstream fcurrents ("currents");

ofstream fcacyt ("cacyt");   // Output Data file for 1D Cai plot (along z axis)
ofstream fcajsr ("cajsr");   // Output Data file for 1D CaSR plot (along z axis)
ofstream fryr("ryrInfo");


int main(int nargs, char** args)
{
    //dt=0.01;
	dt = 0.05;
	int ut = (int)(0.1/dt);
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	//Dx = 0.1;
    Dx = 0.2;

	
	//tstim = 100;
	tstim = 50;
	stimtime = 100;

	time_t now;
    struct tm *ptr_ts;	

	//***************************************************
	Partitioner_t* partitioner;
	int num_procs1=0, num_procs2=0,num_procs3=0;
	int global_N_x,global_N_y,global_N_z;
	int  Nryr = 100;
	int Ndhpr = 15;
	int Ndyads = 100;
	int beats = 120;
	int Unit_X=100;
	int Unit_Y=1;
	int Unit_Z=1;	


	steps = beats*bcl/dt;
	//steps = 100;
    int N_x, N_y,N_z;
	char *output_dir=NULL; 
	int storeStrideX=1,storeStrideY=1,storeStrideZ=1;

	MPI_Init (&nargs, &args);
	if (nargs>1)
            global_N_x = atoi(args[1]);
	if (nargs>2)
            global_N_y = atoi(args[2]);
	if(nargs>3)
	    global_N_z = atoi(args[3]);
	if (nargs>4)
            num_procs1 = atoi(args[4]);
	if (nargs>5)
            num_procs2 = atoi(args[5]);
	if(nargs>6)
	    num_procs3 = atoi(args[6]);
	
	if(nargs>7)
	    Ndyads = atoi(args[7]);
	if(nargs>8)
	    Unit_X = atoi(args[8]);
	if(nargs>9)
	    Unit_Y = atoi(args[9]);
	if(nargs>10)
	    Unit_Z = atoi(args[10]);

	if(nargs>11)
	    storeStrideX = atoi(args[11]);
	if(nargs>12)
	    storeStrideY = atoi(args[12]);
	if(nargs>13)
	    storeStrideZ = atoi(args[13]);
	if(nargs>14)
	    output_dir = args[14];

	if(Ndyads!=Unit_X*Unit_Y*Unit_Z)
	{
		Ndyads = 100;
		Unit_X = 100;
		Unit_Y = 1;
		Unit_Z = 1;
	}

 #ifdef __PAPI__
	int retval;
    if((retval = PAPI_library_init(PAPI_VER_CURRENT))!=PAPI_VER_CURRENT)
	        printf("\nPAPI init error:%s\n",PAPI_strerror(retval));


    

 #endif


    MPI_Comm comm=MPI_COMM_WORLD;
	partitioner = Partitioner_construct(comm,global_N_x,global_N_y,global_N_z,
				                        num_procs1,num_procs2,num_procs3);
	N_x = partitioner->sub_N[0];
	N_y = partitioner->sub_N[1];
	N_z = partitioner->sub_N[2];

	int offsetX,offsetY,offsetZ;
	offsetX = partitioner->my_offset[0];
	offsetY = partitioner->my_offset[1];
	offsetZ = partitioner->my_offset[2];

	int s_ix,s_iy,s_iz;
	int remain;
	int ds;
	remain = offsetX%storeStrideX;
	if(remain==0)
		s_ix=1;
	else
	{
		ds = storeStrideX - remain;
		s_ix = ds+1;
	}	
	remain = offsetY%storeStrideY;
	if(remain==0)
		s_iy=1;
	else
	{
		ds = storeStrideY - remain;
		s_iy = ds+1;
	}	
	remain = offsetZ%storeStrideZ;
	if(remain==0)
		s_iz=1;
	else
	{
		ds = storeStrideZ - remain;
		s_iz = ds+1;
	}	


	cell ***subcells;
	subcells = Allocate3DSubCells(N_x,N_y,N_z);

	v = allocate_3D_double(N_x,N_y,N_z);
	memset(v[0][0],-87.6,N_x*N_y*N_z*sizeof(double));
	vtemp = allocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdt = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdt[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdtclock = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdtclock[0][0],0,N_x*N_y*N_z*sizeof(double));
	dtstep = allocate_3D_double(N_x,N_y,N_z);
	memset(dtstep[0][0],0,N_x*N_y*N_z*sizeof(double));

    int NN = 101;
    double **T;
    double *basePtr = (double *)_mm_malloc(NN*NN*sizeof(double),32);
    T = (double **)_mm_malloc(NN*sizeof(double *),32);
    for(int i=0;i<NN;i++)
        T[i] = basePtr+i*NN;
    //double T[101][101];
    for(int L=0;L<NN;L++)
        for(int C=0;C<=L;C++)
        {
            if((C==0)||(L==C))
                T[L][C] = 1;
            else
                T[L][C] = T[L-1][C]+T[L-1][C-1];
        }

    double p=0.5*dt;
    double **B;
    double *basePtr1 = (double *)_mm_malloc(NN*NN*sizeof(double),32);
    B = (double **)_mm_malloc(NN*sizeof(double *),32);
    for(int i=0;i<NN;i++)
        B[i] = basePtr1+i*NN;
    //double T[101][101];
    for(int L=0;L<NN;L++)
    {
        double p_knk=pow((1-p),L);
        double p1p=p/(1-p);
        B[L][0]=T[L][0]*p_knk;
        for(int C=1;C<=L;C++)
        {
	    p_knk = p_knk*p1p;
            B[L][C]=B[L][C-1]+T[L][C]*p_knk;            
        }
    }

    double *ZT = (double *)_mm_malloc(NN*sizeof(double),32);
    for(int L=0;L<NN;L++)
        ZT[L]=pow((1-0.99999999),L);

	for(int cx = 0; cx < N_x; cx++)
	    for(int cy =0; cy < N_y; cy++)
		for(int cz = 0;cz < N_z;cz++)
		    InitCellValue(subcells[cx][cy][cz],Unit_X,Unit_Y,Unit_Z,Nryr,Ndhpr,beats);

	for(int cx = 1;cx<N_x-1;cx++)
	    for(int cy = 1;cy<N_y-1;cy++)
		for(int cz =1;cz<N_z-1;cz++)
		{
		      compute_cell_id(subcells[cx][cy][cz], cx,cy,cz, partitioner);//also initialize the data random seed
            	      v[cx][cy][cz] = -87.6;
		}

	if(output_dir)
	{
		file << output_dir << "file_" << partitioner->mpi_info.my_rank;
                
		file0 << output_dir << "cell";
	}
	else
	{
		file << "file_" << partitioner->mpi_info.my_rank;
		file0 << "cell";
	}
	fout.open(file.str().c_str());	

    if (partitioner->mpi_info.my_rank == 0)
		f0out.open(file0.str().c_str());
	


	int offset_x = partitioner->my_offset[0];
	int offset_z = partitioner->my_offset[2];

	int total = (N_x-2)*(N_y-2)*(N_z-2);	
   
    //kmp_set_defaults("KMP_AFFINITY=compact");
    //omp_set_num_threads(1);
    double comp_cell_start,comp_cell_end,comp_cell_max;
    double diffusion_start,diffusion_end,diffusion_max;
    double comp_cell_time=0,diffusion_time=0;
    double start;
    MPI_Barrier(MPI_COMM_WORLD);
    start = MPI_Wtime();
	#pragma omp parallel
	{
	        //random_device rd;
            //mt19937 gen(rd());

 #ifdef __PAPI__

	retval = PAPI_thread_init((unsigned long (*)(void))omp_get_thread_num);
	if(retval!=PAPI_OK) {
        printf("PAPI_thread_inite error %s!\n",PAPI_strerror(retval));
    }
    int EventSet = PAPI_NULL;
	//if((retval = PAPI_multiplex_init())!=PAPI_OK)
	//        printf("\nPAPI multiplex init error:%s\n",PAPI_strerror(retval));
	
	if((retval = PAPI_create_eventset(&EventSet))!=PAPI_OK)
	    printf("\nPAPI create event set error:%s\n",PAPI_strerror(retval));
    //if((retval = PAPI_assign_eventset_component(EventSet,0))!=PAPI_OK)
	//    printf("\nPAPI assign event set error:%s\n",PAPI_strerror(retval));
    
    //if((retval = PAPI_set_multiplex(EventSet))!=PAPI_OK)
	//    printf("\nPAPI set multiplex event set error:%s\n",PAPI_strerror(retval));

    int NUM_EVENTS;

	//int Events[]={PAPI_DP_OPS,PAPI_L1_DCM,PAPI_L2_DCM,PAPI_L3_DCM,PAPI_LD_INS,PAPI_SR_INS,PAPI_L3_TCM,PAPI_TOT_CYC};
	//int Events[]={PAPI_LD_INS,PAPI_SR_INS};
    //int Events[]={PAPI_SP_OPS,PAPI_FP_OPS,PAPI_DP_OPS};
   // int Events[]={PAPI_DP_OPS,PAPI_FP_OPS};
    //int Events[]={PAPI_VEC_SP,PAPI_VEC_DP,PAPI_DP_OPS};
    //int Events[]={PAPI_SP_INS};

    //L1 cache info
	int Events[]={PAPI_L1_DCM,PAPI_L2_DCM};
	//int Events[]={PAPI_L1_ICM,PAPI_L2_ICM,PAPI_L1_DCM,PAPI_L2_DCM};
	//int Events[]={PAPI_LD_INS,PAPI_SR_INS,PAPI_L1_ICM,PAPI_L2_ICM,PAPI_L1_DCM,PAPI_L2_DCM};
	//int Events[]={MEM_UOP_RETIRED.ANY_LOADS};
    NUM_EVENTS = sizeof(Events)/sizeof(Events[0]);
	if((retval = PAPI_add_events(EventSet, Events, NUM_EVENTS))!=PAPI_OK)
		printf("PAPI_add_events failed %s!\n",PAPI_strerror(retval));

    //char *EventNames[] = {"INST_RETIRED"};
    //char *EventNames[] = {"MEM_UOPS_RETIRED:ALL_STORES"};
    //NUM_EVENTS = sizeof(EventNames)/sizeof(EventNames[0]);

    //for(int i=0;i<NUM_EVENTS;i++)
	//    if((retval=PAPI_add_named_event(EventSet,EventNames[i])!=PAPI_OK))
	//            printf("PAPI add named event error:%s\n",PAPI_strerror(retval));
	//NUM_EVENTS = PAPI_num_events(EventSet);


	long long *res_papi = (long long*)malloc(NUM_EVENTS*sizeof(long long));
	char EventName[128];
	int num_hwcntrs = 0;

	float real_time, proc_time, mflops;
	long long flpins;

	for( int ii=0;ii<NUM_EVENTS;ii++)
		res_papi[ii] = 0;
		
	if((retval=PAPI_start(EventSet))!=PAPI_OK)
		printf("\nPAPI_read_counters failed! PAPI error:%s\n",PAPI_strerror(retval));

	/*if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
		printf("PAPI_flops failed!\n");*/
 #endif
            //for (int icounter = 0; icounter<steps; icounter++)
            for (int icounter = 0; icounter<steps; icounter++)
            {

                #pragma omp master
                {
                    //MPI_Barrier(MPI_COMM_WORLD);
                    comp_cell_start = MPI_Wtime();
                }
        	    #pragma omp barrier

                #pragma omp for schedule(dynamic,1)
		        for(int I=0;I<total;I++)
		        {
		        	int ix = I/((N_y-2)*(N_z-2))+1;
			        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
			        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                //for (int ix = 1; ix<N_x-1; ix++)
                    //for (int iy = 1; iy<N_y-1; iy++)
                        //for(int iz = 1; iz<N_z-1;iz++)
                            //if ((icounter%10 == 0))//||(fabs(dvdt[ix][iy][iz])>0.2)||(dvdtclock[ix][iy][iz]<20) )
                            {
                                comp_cell(subcells[ix][iy][iz],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr,T,B,ZT);
                                dtstep[ix][iy][iz] = 0;
                            }
                }            
        	    #pragma omp master
		        {
		            comp_cell_end = MPI_Wtime()-comp_cell_start;
		            //MPI_Allreduce(&comp_cell_end,&comp_cell_max,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
                    comp_cell_time+=comp_cell_end;

                    //MPI_Barrier(MPI_COMM_WORLD);
                    diffusion_start = MPI_Wtime();
		        }
        	    #pragma omp barrier
                        

                //////////////////////////////////????????????????????????????????????????//////////////////////////	
                #pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);

        	    #pragma omp master
		        {
	                diffusion_end = MPI_Wtime()-diffusion_start;
	                //MPI_Allreduce(&diffusion_end,&diffusion_max,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
	                diffusion_time+=diffusion_end;

        	        UpdateGhostValue(v,partitioner);
		        }
        	    #pragma omp barrier
                        

                #pragma omp for
                for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {
                            dvdt[ix][iy][iz] = (v[ix][iy][iz] - vtemp[ix][iy][iz])/dt;
                            if (dvdt[ix][iy][iz]>1) 
                                dvdtclock[ix][iy][iz] = 0;
                            dtstep[ix][iy][iz] += dt;
                            dvdtclock[ix][iy][iz] += dt;
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }

                #pragma omp single
                {
                    if (t>=tstim && t<(tstim+dt))
                    {
                        stimtime = 0;
                        ibeat = ibeat+1;
                        tstim = tstim+bcl;
                        if (partitioner->mpi_info.my_rank==0)
                            cout<<"ibeat is :"<<ibeat<<endl;
                    }

                    if (stimtime>=0 && stimtime<=0.5)
                        ist = -80;
                    else
                        ist = 0;

                    if (ibeat>beats-7) {ist = 0;}
                    
                    if (t>(beats-12)*bcl && t<=((beats-12)*bcl+6000))
                    {
                        if (icounter%100 == 0) //  && partitioner->is_master_proc())
                        {
                            for(int ix=s_ix;ix<N_x-1;ix+=storeStrideX)
                                for(int iy=s_iy;iy<N_y-1;iy+=storeStrideY)
                                {
                                    for(int iz=s_iz;iz<N_z-1;iz+=storeStrideZ)
                                        fout << v[ix][iy][iz] << " ";
                                    fout << endl;
                                }		
                        }
                        if(icounter%ut==0)
                        {
                            time(&now);
                            ptr_ts = gmtime(&now);
            
                            if (partitioner->mpi_info.my_rank == 0)
                                //f0out<<t<<"	"<< ptr_ts->tm_hour<<":"<<ptr_ts->tm_min << " " << v[N_x/2][N_y/2][N_z/2]<<endl;
                                f0out<<t<<"  "<< v[N_x/2][N_y/2][N_z/2]<<endl;
                            if (partitioner->mpi_info.my_rank == 0)
                                //if (t>(beats-8)*bcl && t<=((beats-8)*bcl+4000))
                                printInfo(t,subcells[N_x/2][N_y/2][N_z/2],v[N_x/2][N_y/2][N_z/2],fcalcium,fdyad,fcurrents,fcacyt,fcajsr,fryr);
                            
                        }
                /*        if(icounter%999==0)
                        {
                            //printf("####################################################################################################################\n");
                            //printf("###################################################################load barrence info############################################\n");
                            //printf("%lf   %lf   %lf   %lf    %lf   %lf   %lf   %lf    %lf   %lf   %lf   %lf    %lf   %lf   %lf   %lf  \n  ",subcells[1][1][1].T_cell,subcells[2][1][1].T_cell,subcells[3][1][1].T_cell,subcells[4][1][1].T_cell,subcells[5][1][1].T_cell,subcells[6][1][1].T_cell,subcells[7][1][1].T_cell,subcells[8][1][1].T_cell,subcells[9][1][1].T_cell,subcells[10][1][1].T_cell,subcells[11][1][1].T_cell,subcells[12][1][1].T_cell,subcells[13][1][1].T_cell,subcells[14][1][1].T_cell,subcells[15][1][1].T_cell,subcells[16][1][1].T_cell);
                            //printf(" %lf    %lf   %lf   %lf   %lf    %lf   %lf   %lf  \n  ",subcells[1][1][1].T_cell,subcells[2][1][1].T_cell,subcells[3][1][1].T_cell,subcells[4][1][1].T_cell,subcells[5][1][1].T_cell,subcells[6][1][1].T_cell,subcells[7][1][1].T_cell,subcells[8][1][1].T_cell);
                            for(int i=1;i<N_x-1;i++)
                            {
                                printf("%lf     ",subcells[i][1][1].T_cell);
                                subcells[i][1][1].T_cell = 0;
                            }
                            printf("\n");
                        }*/
                    }
                    t+=dt;
                    stimtime += dt;
                    
                }//end single region
            }//end time loop
            
        if(partitioner->mpi_info.my_rank==0)
        {
        #ifdef __PAPI__
            if((retval=PAPI_stop(EventSet,res_papi))!=PAPI_OK)
                printf("\nPAPI_accum_counters failed!PAPI error:%s\n",PAPI_strerror(retval));
            for( int ii=0;ii<NUM_EVENTS;ii++)
            {
                PAPI_event_code_to_name(Events[ii],EventName);
                printf("PAPI Event name: %s, value: %lld on thread %d\n",EventName, res_papi[ii],omp_get_thread_num());
                //printf("PAPI Event name: %s, value: %lld on thread %d \n",EventNames[ii], res_papi[ii],omp_get_thread_num());
            }
            //double BW = res_papi[0]*32*2.6/res_papi[1];
            //printf("bandwidth is %g\n",BW);

            /*if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                        printf("PAPI_flops failed!\n");

            printf("Real_time: \t%f\nProc_time:\t%f\nTotal flpins:\t%lld\n MFLOPS:\t\t%f\n",real_time,proc_time,flpins,mflops);*/
            free(res_papi);
        #endif
        }
	} //end parallel region

    double end = MPI_Wtime()-start;
    double mpitime;
    MPI_Allreduce(&end,&mpitime,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
    printf(" on %d total time is %lf \n  comp cell time %lf \n  diffusion time %lf",partitioner->mpi_info.my_rank,mpitime,comp_cell_time,diffusion_time);

#ifdef PAPI
    PAPI_shutdown();
#endif

	fout.close();
	
  	if (partitioner->mpi_info.my_rank == 0)
		f0out.close();

  	if (partitioner->mpi_info.my_rank == 0)
    {
        printf("Number of random data used in cell 0=%lld\n",subcells[1][1][1].RandUsedN);
        printf("time info!\n");
        printf("|--comp_cell=%g\n",subcells[1][1][1].T_cell);
        printf("   |--comp_ina=%g  percentage=%g \n",subcells[1][1][1].T_ina,(subcells[1][1][1].T_ina)/(subcells[1][1][1].T_cell));
        printf("   |--comp_ito=%g  percentage=%g \n",subcells[1][1][1].T_ito,(subcells[1][1][1].T_ito)/(subcells[1][1][1].T_cell));
        printf("   |--comp_ikr=%g  percentage=%g \n",subcells[1][1][1].T_ikr,(subcells[1][1][1].T_ikr)/(subcells[1][1][1].T_cell));
        printf("   |--comp_iks=%g  percentage=%g \n",subcells[1][1][1].T_iks,(subcells[1][1][1].T_iks)/(subcells[1][1][1].T_cell));
        printf("   |--comp_ik1=%g  percentage=%g \n",subcells[1][1][1].T_ik1,(subcells[1][1][1].T_ik1)/(subcells[1][1][1].T_cell));
        printf("   |--comp_inak=%g  percentage=%g \n",subcells[1][1][1].T_inak,(subcells[1][1][1].T_inak)/(subcells[1][1][1].T_cell));
        printf("   |--comp_inab=%g  percentage=%g \n",subcells[1][1][1].T_inab,(subcells[1][1][1].T_inab)/(subcells[1][1][1].T_cell));
        printf("   |--comp_calc_dyad=%g  percentage=%g \n",subcells[1][1][1].T_calc_dyad,(subcells[1][1][1].T_calc_dyad)/(subcells[1][1][1].T_cell));
        
        printf("      |--comp_randomG=%g  percentage=%g \n",subcells[1][1][1].T_randomG,(subcells[1][1][1].T_randomG)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_prepare=%g  percentage=%g \n",subcells[1][1][1].T_prepare,(subcells[1][1][1].T_prepare)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_ical_dyad=%g  percentage=%g \n",subcells[1][1][1].T_ical_dyad,(subcells[1][1][1].T_ical_dyad)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_L_Channel=%g  percentage=%g \n",subcells[1][1][1].T_L_Channel,(subcells[1][1][1].T_L_Channel)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_ical_dyad1=%g  percentage=%g \n",subcells[1][1][1].T_ical_dyad1,(subcells[1][1][1].T_ical_dyad1)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_irel_dyad=%g  percentage=%g \n",subcells[1][1][1].T_irel_dyad,(subcells[1][1][1].T_irel_dyad)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_irel1=%g  percentage=%g \n",subcells[1][1][1].T_irel1,(subcells[1][1][1].T_irel1)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_irel2=%g  percentage=%g \n",subcells[1][1][1].T_irel2,(subcells[1][1][1].T_irel2)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_RyrOpen=%g  percentage=%g \n",subcells[1][1][1].T_RyrOpen,(subcells[1][1][1].T_RyrOpen)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_irel3=%g  percentage=%g \n",subcells[1][1][1].T_irel3,(subcells[1][1][1].T_irel3)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_conc_dyad=%g  percentage=%g \n",subcells[1][1][1].T_conc_dyad,(subcells[1][1][1].T_conc_dyad)/(subcells[1][1][1].T_calc_dyad));
        printf("         |--comp_ica_dyad=%g  percentage=%g \n",subcells[1][1][1].T_ica_dyad,(subcells[1][1][1].T_ica_dyad)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_conc_diff=%g  percentage=%g \n",subcells[1][1][1].T_conc_diff,(subcells[1][1][1].T_conc_diff)/(subcells[1][1][1].T_calc_dyad));
        printf("      |--comp_acculate=%g  percentage=%g \n",subcells[1][1][1].T_acculate,(subcells[1][1][1].T_acculate)/(subcells[1][1][1].T_calc_dyad));




        printf("   |--comp_ical=%g  percentage=%g \n",subcells[1][1][1].T_ical,(subcells[1][1][1].T_ical)/(subcells[1][1][1].T_cell));
        printf("   |--comp_rest=%g  percentage=%g \n",subcells[1][1][1].T_rest,(subcells[1][1][1].T_rest)/(subcells[1][1][1].T_cell));
    }
    

	_mm_free(T[0]);
	_mm_free(T);
	_mm_free(B[0]);
	_mm_free(B);
	_mm_free(ZT);

        deallocate_3D_double (v,N_x);
        deallocate_3D_double (vtemp,N_x);
        deallocate_3D_double (dvdt,N_x);
        deallocate_3D_double (dvdtclock,N_x);

        deallocate_3D_double (dtstep,N_x);
        for(int cx = 0; cx < N_x; cx++)
            for(int cy = 0;cy < N_y; cy++)
                for(int cz =0; cz < N_z;cz++)
                    releaseCell(subcells[cx][cy][cz]);
        deallocate3DSubCells(subcells,N_x);
        Partitioner_destruct(partitioner);
	MPI_Finalize ();
	printf("finished!\n");
	return 0;
}




