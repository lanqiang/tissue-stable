//define constant value here
//
#define Nvox 1
#define nao 140.0
#define ko 5.4
#define cao 1.8
#define iso 1.0
//#define gna 75.0
#define dx 0.2
#define dy 0.2
#define dz 0.2
#define R 8314.0
#define temp 310.0
#define frdy 96485.0
#define Dsr 0.02
#define Dcyt 0.04
#define kmcsqn 0.5
#define csqnbar 10
#define bsrbar 0.047
#define bslbar 1.124
#define kmbsr 0.00087
#define kmbsl 0.0087
#define trpnbar 0.07
#define cmdnbar 0.05
#define kmcmdn 0.00238
#define aCaMK 0.05
#define bCaMK 0.00068
#define CaMKo 0.05
#define KmCaM 0.0015
#define KmCaMK 0.15

#define ar 0.0011
#define al 0.01
#define pi 3.142
#define vds 1e-13
#define vol_ds 1e-13
#define Kmn 0.002
#define k2n 1000.0


//in 6
#define zna 1.0
#define zca 2.0
#define k1p 949.5
#define k1m 182.4
#define k2p 687.2
#define k2m 39.4
#define k3p 1899.0
#define k3m 79300.0
#define k4p 639.0
#define k4m 40.0
#define Knai0 9.073
#define Knao0 27.78
#define delta -0.1550

#define Kki 0.5
#define Kko 0.3582
#define MgADP 0.05
#define MgATP 9.8
#define Kmgatp 1.698e-7
#define H 1.0e-7
#define eP 4.2
#define Khp 1.698e-7
#define Knap 224.0
#define Kxkur 292.0

#define zk 1.0
#define Pnak 30.0


#define kna1 15.0
#define kna2 5.0
#define kna3 88.12
#define kasymm 12.5
#define wna 6.0e4
#define wca 6.0e4
#define wnaca 5.0e3
#define kcaon 1.5e6
#define kcaoff 5.0e3
#define qna 0.5224
#define qca 0.167


#define Km 8
#define Kmcsqn 150
#define dryr 4
#define tauefflux 7e-4
#define taudiff 0.1
#define taurefill 100
#define KmCaAct 150.0e-6


