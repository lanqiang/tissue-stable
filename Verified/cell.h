#ifndef CELL_H_INCLUDED
#define CELL_H_INCLUDED

#include "randomGenerate.h"
#include "RectPart2D.h"
#include "unit.h"
#include "mkl_vsl.h"
//#include <random>
struct cell
{
    int cellID;
    struct RandData *pRD;
    struct unit *unitsInCell;

    int unitNum_x;
    int unitNum_y;
    int unitNum_z;
	
    int Ndhpr;
    int Ndyads;
    int Nryr;
    unsigned long rand_n;
    int randU;

    int LtypeRandN;
    int RyrRandN;

    long long  RandUsedN;

    int beats;

   //added new variebles
    double f,fca,fp,fcap;

    double m,hf,hs,hsp,j,jp;
    double ml,hl,hlp;
    double nai,ki,nass,kss;

    double ap,a,iF,iS,iFp,iSp;
    double xs1,xs2,xrs,xrf,xk1;
    double d,ff,fcaf,fs,fcas,jca,ffp,fcafp;

    double CaMKa_cell;//added by qianglan
    double nca_print;//added by qianglan
    double inacass_dyad;//added by qianglan

    int nryrc1;
    int nryro1;
    int nryrc2;
    int nryro2;

    double kC1O1,kC1C2,kO1C1,kO1O2,kC2C1,kC2O2,kO2C2,kO2O1;

    double *nca;
    double *CaMKt;
    double *CaMKa;//added by qianglan
    double *CaMKb;//added by qianglan
    double *trel;
    double *Poryr;

    int *nactive;
    //int randcallcount;

    long timestep;
    long totalCellSize;

    VSLStreamStatePtr Randomstream;

    VSLStreamStatePtr BinomialStream;

    
    //std::random_device rd;
    //std::mt19937 gen;
    
    
    //out put info
    double cadyad;
    double casub;
    double cai;
    double nsr;
    double jsr;

    double ina;
    double ICaL_print;
    double Ito;
    double IKs;
    double IKr;
    double IK1;
    double INaK;
    double inaca;
    double inacass;
    double jrel;

    double *casstemp;
    double *cansrtemp; 
    double *cacyttemp;
    double *cassdiff;
    double *cansrdiff;
    double *cacytdiff;
  
    double *l_Random;

    //time info
    double T_cell;
    double T_ina;
    double T_ito;
    double T_ikr;
    double T_iks;
    double T_ik1;
    double T_inak;
    double T_inab;
    double T_calc_dyad;
    double T_ical;
    double T_rest;    
    
    //time info:inside T_calc_dyad
    double T_randomG;
    double T_prepare;
    double T_ical_dyad;
        double T_L_Channel;
        double T_ical_dyad1;
    double T_irel_dyad;
        double T_irel1;
        double T_irel2;
        double T_RyrOpen;
        double T_irel3;
    double T_conc_dyad;
        double T_ica_dyad;
    double T_conc_diff;
    double T_acculate;
        
};


struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z);
void deallocate3DSubCells(struct cell ***SubCells,int Nx);

void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z, int nryr, int ndhpr,int beats);
void releaseCell(struct cell &Cell);

//void generateSeed(struct cell&Cell, int thread_id,int process_id);
void compute_cell_id(struct cell & Cell, int cx, int cy, int cz, Partitioner_t* partitioner);



#endif // CELL_H_INCLUDED
