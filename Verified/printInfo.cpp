#include "printInfo.h"
#include <fstream>

#include "cell.h"
void printInfo(double t,cell& Cell, double v,ofstream &f1,ofstream &f2,ofstream &f3,ofstream &f4,ofstream &f5,ofstream &f6)
{
    int Ndyads = Cell.Ndyads;
    if(f1.is_open())
        //f1<< t <<" "<< v <<" "<< Cell.unitsInCell[Ndyads/2].cads<<" "<<Cell.cassdiff[Ndyads/2]<<" "<<Cell.cacytdiff[Ndyads/2]<< " "<<Cell.cansrdiff[Ndyads/2]<< " " << Cell.unitsInCell[Ndyads/2].cajsr<<endl;
        f1<< t <<"  "<< v <<"  "<< Cell.cadyad<<"  "<<Cell.casub<<"  "<<Cell.cai<< "  "<<Cell.nsr<< "  " << Cell.jsr<<endl;
    if(f2.is_open())
        //f2out<<t<<" "<<cads[Ndyads/2]<<"    "<<cass[Ndyads/2]<<"    "<<cacyt[Ndyads/2]<<"   "<<cansr[Ndyads/2]<<"   "<<cajsr[Ndyads/2]<<endl;
        f2<< t <<"  "<< Cell.unitsInCell[Ndyads/2].cads<<"  "<<Cell.unitsInCell[Ndyads/2].cass<<"  "<<Cell.unitsInCell[Ndyads/2].cacyt<< "  "<<Cell.unitsInCell[Ndyads/2].cansr<< "  " << Cell.unitsInCell[Ndyads/2].cajsr<<endl;
        f3<< t <<"  "<<Cell.ina<<"   "<<Cell.ICaL_print<<"    "<<Cell.Ito<<"   "<<Cell.IKs<<"   "<<Cell.IKr<<"   "<<Cell.IK1<<"   "<<Cell.INaK<<"  "<<Cell.inaca+Cell.inacass<<"  "<<Cell.jrel<<endl;
    
//    for(int i=0;i<Ndyads;i++)
//    {
//        f4<<Cell.unitsInCell[i].cacyt<<" ";
//        f5<<Cell.unitsInCell[i].cajsr<<" ";
//    }
    f6<< t << " " << Cell.nryrc1 << " " << Cell.nryrc2 << " " << Cell.nryro1<< " "<<Cell.nryro2 <<" " <<Cell.kC1O1<<" " <<Cell.kC1C2<<" " <<Cell.kO1C1<<" " <<Cell.kO1O2<<" " <<Cell.kC2C1<<" " <<Cell.kC2O2<<" " <<Cell.kO2C2<<" " <<Cell.kO2O1<< endl; 
}
