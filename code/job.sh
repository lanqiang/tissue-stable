#!/bin/bash

#SBATCH --job-name=t440016
#SBATCH --time=00:20:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=Output_File.out

#SBATCH --ntasks=16
#SBATCH --ntasks-per-node=16
#SBATCH --exclusive
#SBATCH --cpus-per-task=1

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
time mpirun -np 16 ./tissue 4 4
