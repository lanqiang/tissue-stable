#include "allocArray.h"

#include <stdlib.h>

double ** allocate_array2D ( int Nx, int Ny)
{
  double **ptr;
  double *base_ptr = (double*)malloc(Nx*Ny*sizeof(double));
  ptr = (double**)malloc(Nx*sizeof(double*));
  for (int i=0; i<Nx; i++)
    ptr[i] = &(base_ptr[i*Ny]);
  return ptr;
}


double *** all_3D_double(int Nx, int Ny,int Nz)
{
  double ***ptr;
  double *base_ptr = (double*)malloc(Nx*Ny*Nz*sizeof(double));
  ptr = (double***)malloc(Nx*sizeof(double**));
  for (int ix=0; ix<Nx; ix++)
  {
	ptr[ix] = (double**)malloc(Ny*sizeof(double*));
	for (int iy = 0; iy<Ny; iy++)
       {
		ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
       }
   }
   return ptr;
}

int *** all_3D_int( int Nx, int Ny, int Nz)
{
	int ***ptr;
	int *base_ptr = (int *)malloc(Nx*Ny*Nz*sizeof(int));
	ptr =(int ***)malloc(Nx*sizeof(int**));
	for(int ix=0; ix<Nx;ix++)
		{
			ptr[ix] = (int **)malloc(Ny*sizeof(int *));
			for( int iy = 0; iy<Ny; iy++)
				{
					ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
				//	(*ptr)[ix][iy] = (int *)malloc(Ndyads*sizeof(int));
				}
		}
       return ptr;
}


double **** all_4D_double( int Nx, int Ny, int Nz, int Nt)
{
	double ****ptr;
	double *base_ptr = (double*)malloc(Nx*Ny*Nz*Nt*sizeof(double));
	ptr =  (double****)malloc(Nx*sizeof(double***));

	for (int ix = 0; ix<Nx; ix++)
	{
		ptr[ix] = (double***)malloc(Ny*sizeof(double**));
		for (int iy = 0; iy<Ny; iy++)
		{
			ptr[ix][iy] = (double**)malloc(Nz*sizeof(double*));
			for (int idyads = 0; idyads<Nz; idyads++)
			{
				ptr[ix][iy][idyads] = &(base_ptr[ix*Ny*Nz*Nt + iy*Nz*Nt + idyads*Nt]);
			//	(*ptr)[ix][iy][idyads] = (double *)malloc(Nryr*sizeof(double));
			}
		}
	}
	return ptr;
}

int **** all_4D_int( int Nx, int Ny, int Nz, int Nt)
{
	int ****ptr;
	int *base_ptr = (int*)malloc(Nx*Ny*Nz*Nt*sizeof(int));
	ptr =  (int****)malloc(Nx*sizeof(int***));

	for (int ix = 0; ix<Nx; ix++)
	{
		ptr[ix] = (int***)malloc(Ny*sizeof(int**));
		for (int iy = 0; iy<Ny; iy++)
		{
			ptr[ix][iy] = (int**)malloc(Nz*sizeof(int*));
			for (int idyads = 0; idyads<Nz; idyads++)
			{
				ptr[ix][iy][idyads] = &(base_ptr[ix*Ny*Nz*Nt + iy*Nz*Nt + idyads*Nt]);
			//	(*ptr)[ix][iy][idyads] = (int *)malloc(Nryr*sizeof(int));
			}
		}
	}
	return ptr;
}

void deallocate_array2D (double **ptr)
{
	free(ptr[0]);
	free(ptr);
}


void deallocate_array3D_double(double ***ptr,int Nx)
{
	free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    free(ptr[i]);
	free(ptr);
}

void deallocate_array3D_int(int ***ptr,int Nx)
{
	free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    free(ptr[i]);
	free(ptr);
}


void deallocate_array4D_int(int ****ptr,int Nx,int Ny)
{
	free(ptr[0][0][0]);
	for(int i=0;i<Nx;i++)
	{
	     for(int j=0;j<Ny;j++)
		free(ptr[i][j]);
	     free(ptr[i]);
	}
	free(ptr);
}
