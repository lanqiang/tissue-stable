#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>

#include <omp.h>
#include <malloc.h>
#include <stdlib.h>
#define __PAPI__
#ifdef __PAPI__
#include <papi.h>
#endif


#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"

#define global_N_x 400
#define global_N_y 400
#define Nryr  100
#define Ndhpr 15
#define Ndyads 100
#define Ntotdyads 10000
#define beats 5

//#define MPI_TIME


using namespace std;


double **v, **vtemp,**dvdt,**dvdtclock;
double **dtstep;
double t,dt,d_x,Dx;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=1000;

ofstream fout;
//ofstream f1out;
ofstream f0out;
ostringstream file;
ostringstream file0;




int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	Dx = 0.2;

	tstim = 100;
	stimtime = 100;

	steps = beats*bcl/dt;
	//steps = 2;

	//***************************************************
	RectPart2D* partitioner;
	int num_procs1=0, num_procs2=0;
	int N_x, N_y;
	char *output_dir=NULL; 

	MPI_Init (&nargs, &args);
	if (nargs>1)
	  num_procs1 = atoi(args[1]);
	if (nargs>2)
	  num_procs2 = atoi(args[2]);
	if(nargs>3)
	  output_dir = args[3];

	double comp_cell_start, comp_cell_end, comp_cell_time, comp_cell_total=0;
	double diffusion_start, diffusion_end, diffusion_time, diffusion_total=0;
	double comm_start, comm_end, comm_time, comm_total=0;
	double main_start;
	#ifdef MPI_TIME
	#pragma omp master
	{
		MPI_Barrier(MPI_COMM_WORLD);
		main_start = MPI_Wtime();
		
	}
	#endif

//	#ifdef __PAPI__
        //#pragma omp master
    //    {
                float real_time, proc_time,mflops;
                long long flpins;
                int retval;
  //      }
  //      #endif

        
	partitioner = new RectPart2D(global_N_x,global_N_y,
				     num_procs1,num_procs2);
	N_x = partitioner->sub_N_x();
	N_y = partitioner->sub_N_y();

    cell **subcells;
    subcells = Allocate2DSubCells(N_x,N_y);

    v = allocate_array2D(N_x,N_y);
    vtemp = allocate_array2D(N_x,N_y);
    memset(vtemp[0],0,N_x*N_y*sizeof(double));
    dvdt = allocate_array2D(N_x,N_y);
    memset(dvdt[0],0,N_x*N_y*sizeof(double));
    dvdtclock = allocate_array2D(N_x,N_y);
    memset(dvdtclock[0],0,N_x*N_y*sizeof(double));
    dtstep = allocate_array2D(N_x,N_y);
    memset(dtstep[0],0,N_x*N_y*sizeof(double));

    for(int cx = 1; cx < N_x-1; cx++)
        for(int cy = 1;cy < N_y-1; cy++)
        {
            InitCellValue(subcells[cx][cy],Ndyads,1,1,Nryr);
            compute_cell_id(subcells[cx][cy], cx,cy, partitioner);//also initialize the data random seed
	    printf("cell id is : %d \n", subcells[cx][cy].cellID);
            v[cx][cy] = -87.6;
        }

	//Files for output
	if(output_dir)
	{
		file<< output_dir << "file_" <<partitioner->my__rank();
		file0 << output_dir << "v_cell";
	}
	else
	{
		file<< "file_" <<partitioner->my__rank();
		file0 << "v_cell";
	}
	fout.open(file.str().c_str());	
	f0out.open(file0.str().c_str());


	 #ifdef __PAPI__
        //#pragma omp master
        {
                if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                        printf("PAPI_flops failed!\n");
        }
        #endif


	#pragma omp parallel
	{
	
	for (int icounter = 0; icounter<steps; icounter++)
	{
	#ifdef MPI_TIME
		#pragma omp master
		{
		MPI_Barrier(MPI_COMM_WORLD);
		comp_cell_start = MPI_Wtime();
		}
	#endif


	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++)
	//for(int iy=1; iy<N_y-1;iy++)
	{
		for (int iy = 1; iy<N_y-1; iy++)
		//for(int ix = 1; ix<N_x-1; ix++)
		{
			if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy])>0.2)||(dvdtclock[ix][iy]<20) )
			{
				comp_cell(subcells[ix][iy],ix,iy,partitioner->offset_x(),
				partitioner->offset_y(), t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr);
				dtstep[ix][iy] = 0;
			}

		}
	}
	#ifdef MPI_TIME
		#pragma omp master
		{
		comp_cell_end = MPI_Wtime() - comp_cell_start;
		MPI_Allreduce(&comp_cell_end, &comp_cell_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
		comp_cell_total += comp_cell_time;

		MPI_Barrier(MPI_COMM_WORLD);
		diffusion_start = MPI_Wtime();
		}
	#endif

	
	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++)
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			v[ix][iy] += dt*Dx*(vtemp[ix+1][iy] + vtemp[ix-1][iy] + vtemp[ix][iy-1] + vtemp[ix][iy+1] -4*vtemp[ix][iy])
					/(d_x*d_x);
		}
	}

	#ifdef MPI_TIME
		#pragma omp master
		{
		diffusion_end = MPI_Wtime() - diffusion_start;
		MPI_Allreduce(&diffusion_end, &diffusion_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
		diffusion_total += diffusion_time;

		MPI_Barrier(MPI_COMM_WORLD);
		comm_start = MPI_Wtime();
		}
	#endif

	#pragma omp master
//	#pragma omp single
	{
	  int i;
	  partitioner->start_communication(v);

	  // -------- treatment of boundary conditions start ----
	  if (!partitioner->has_lower_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][0] = v[i][1];

	  if (!partitioner->has_upper_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][N_y-1] = v[i][N_y-2];

	  if (!partitioner->has_left_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[0][i] = v[1][i];

	  if (!partitioner->has_right_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[N_x-1][i] = v[N_x-2][i];
	  // -------- treatment of boundary conditions end ----
	  partitioner->finish_communication(v);
	}
	#pragma omp barrier
		
	#ifdef MPI_TIME
		#pragma omp master
		{
		comm_end = MPI_Wtime() - comm_start;
		MPI_Allreduce(&comm_end, &comm_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
		comm_total += comm_time;
		}
	#endif

	#pragma omp for
	for (int ix = 0; ix<N_x; ix++)
	{
		for (int iy = 0; iy<N_y; iy++)
		{
			dvdt[ix][iy] = (v[ix][iy] - vtemp[ix][iy])/dt;
			if (dvdt[ix][iy]>1) dvdtclock[ix][iy] = 0;
			dtstep[ix][iy] += dt;
			dvdtclock[ix][iy] += dt;
			vtemp[ix][iy] = v[ix][iy];
		}
	}

	#pragma omp single
	{
	if (t>=tstim && t<(tstim+dt))
	{
		stimtime = 0;
		ibeat = ibeat+1;
		tstim = tstim+bcl;
		if (partitioner->is_master_proc())
		  cout<<"ibeat is :"<<ibeat<<endl;
	}

	if (stimtime>=0 && stimtime<=0.5)
		ist = -80;
	else
		ist = 0;

//	if (ibeat>beats-7) {ist = 0; Dx=0;}

	if (icounter%1000 == 0) //  && partitioner->is_master_proc())
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			for (int ix = 1; ix<N_x-1; ix++)
			{
				fout<<v[ix][iy]<<"	";

			}
			fout<<endl;
		}


		if (partitioner->my__rank() == 0)
		{
				f0out<<t<<"	"<<v[N_x/2][N_y/2]<<endl;
		}

	}


	t+=dt;
	stimtime += dt;
	}//end single region
	}//end time loop
	
	#ifdef __PAPI__
	#pragma omp master
	{
        	if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                	printf("PAPI_flops failed!\n");
        	printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMPLOs:\t%f\n",real_time,proc_time,flpins,mflops);

        	PAPI_shutdown();
	}
        #endif


	} //end parallel region

	#ifdef __PAPI__
//        #pragma omp master
        {
                if((retval=PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                        printf("PAPI_flops failed!\n");
                printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMPLOs:\t%f\n",real_time,proc_time,flpins,mflops);

                PAPI_shutdown();
        }
        #endif


    free(partitioner);

    deallocate_array2D (v);
    deallocate_array2D (vtemp);
    deallocate_array2D (dvdt);
    deallocate_array2D (dvdtclock);

    deallocate_array2D (dtstep);

    for(int cx = 1; cx < N_x-1; cx++)
        for(int cy = 1;cy < N_y-1; cy++)
            releaseCell(subcells[cx][cy]);

    deallocate2DSubCells(subcells);

	#ifdef MPI_TIME
		#pragma omp master
		{
		double main_end = MPI_Wtime() - main_start;
		double main_time;
		MPI_Allreduce(&main_end, &main_time,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
		cout << "############### main time is " << main_time << "################" << endl;
		cout << "############### comp_cell time is " << comp_cell_total << "################" << endl;
		cout << "############### diffusion time is " << diffusion_total << "################" << endl;
		cout << "############### communication time is " << comm_total << "################" << endl;
		}
	#endif

	MPI_Finalize ();
	printf("finished!\n");
	return 0;
}




