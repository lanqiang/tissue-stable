                                                                 // -*- C++ -*-

#ifndef RectPart2D_h__
#define RectPart2D_h__
#include <mpi.h>

class RectPart2D
{
protected:
  int my_rank, total_MPI_procs;
  int my_rank_x, my_rank_y;
  int left_neighbor_ID, right_neighbor_ID;
  int lower_neighbor_ID, upper_neighbor_ID;

  int num_procs_x, num_procs_y;

  int N_x, N_y;
  int sub_Nx, sub_Ny;
  int my_offset_x, my_offset_y;

  double *left_out_msg, *left_in_msg;
  double *right_out_msg, *right_in_msg;
  double *upper_out_msg, *upper_in_msg;
  double *lower_out_msg, *lower_in_msg;

  MPI_Request left_out_request, left_in_request;
  MPI_Request right_out_request, right_in_request;
  MPI_Request upper_out_request, upper_in_request;
  MPI_Request lower_out_request, lower_in_request;

  MPI_Status status;

public:
  RectPart2D (int N1, int N2, int num_procs1 = 0, int num_procs2 = 0)
  { preparation (N1, N2, num_procs1, num_procs2); }
 ~RectPart2D ();

  void preparation (int N1, int N2, int num_procs1, int num_procs2);


  int total_N_x(){return N_x;}
  int total_N_y(){return N_y;}
  int sub_N_x () { return sub_Nx; }
  int sub_N_y () { return sub_Ny; }
  int offset_x () { return my_offset_x; }
  int offset_y () { return my_offset_y; }
  int my__rank() {return my_rank;}
  int my__rank_x() {return my_rank_x;}
  int my__rank_y() {return my_rank_y;}
  int total__MPI_procs(){return total_MPI_procs;}


  bool is_master_proc () { return (my_rank==0); }

  bool has_left_neighbor () { return (left_neighbor_ID>=0); }
  bool has_right_neighbor () { return (right_neighbor_ID>=0); }
  bool has_lower_neighbor () { return (lower_neighbor_ID>=0); }
  bool has_upper_neighbor () { return (upper_neighbor_ID>=0); }

  void start_communication (double **array2D);
  void finish_communication (double **array2D);
};

#endif
