#ifndef ALLOCARRAY_H_INCLUDED
#define ALLOCARRAY_H_INCLUDED



double ** allocate_array2D ( int Nx, int Ny);

double *** all_3D_double(int Nx, int Ny,int Nz);

int *** all_3D_int( int Nx, int Ny, int Nz);

double **** all_4D_double( int Nx, int Ny, int Nz, int Nt);

int **** all_4D_int( int Nx, int Ny, int Nz, int Nt);

void deallocate_array2D (double **ptr);

void deallocate_array3D_double(double ***ptr,int Nx);

void deallocate_array3D_int(int ***ptr,int Nx);

void deallocate_array4D_int(int ****ptr,int Nx,int Ny);


#endif // ALLOCARRAY_H_INCLUDED
