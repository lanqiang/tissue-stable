#include "cell.h"
#include "unit.h"
#include <cstring>
#include <stdlib.h>

struct cell **Allocate2DSubCells(int sub_N_x,int sub_N_y)
{
    struct cell **SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sizeof(struct cell));
    SubCells = (struct cell**)malloc(sub_N_x*sizeof(struct cell*));
    for(int i=0; i<sub_N_x; i++)
        SubCells[i] = &(cells[i*sub_N_y]);
    return SubCells;

}

void deallocate2DSubCells(struct cell **SubCells)
{
    free(SubCells[0]);
    free(SubCells);
    return;
}


void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z,int nryr)
{
    Cell.nai=6.3;
    Cell.nass = Cell.nai;
    Cell.ki=145;
    Cell.kss = Cell.ki;

    Cell.m=0.01;
    Cell.hf=0.7;
    Cell.hs=0.7;
    Cell.j=0.7;
    Cell.hsp=0.45;
    Cell.jp=.7;

    Cell.ml=0.0002;
    Cell.hl=0.5;
    Cell.hlp=0.27;

    Cell.a=0.001;
    Cell.iF=1;
    Cell.iS=.76;
    Cell.ap=.0005;
    Cell.iFp=1;
    Cell.iSp=.8;

    Cell.d=0;
    Cell.ff=1;
    Cell.fs=.91;
    Cell.fcaf=1;
    Cell.fcas=1;
    Cell.jca=1;
    Cell.ffp=1;
    Cell.fcafp=1;

    Cell.xrf=0;
    Cell.xrs=0.27;
    Cell.xs1=0.25;
    Cell.xs2=0;
    Cell.xk1=1;

    Cell.nca_print = 0;//added by qianglan
   

    Cell.unitNum_x = unitNum_x;
    Cell.unitNum_y = unitNum_y;
    Cell.unitNum_z = unitNum_z;

    int ndyads = Cell.unitNum_x;

    Cell.nca = (double *)malloc(ndyads*sizeof(double)); memset(Cell.nca,0,ndyads*sizeof(double));
    Cell.CaMKt = (double *)malloc(ndyads*sizeof(double));memset(Cell.CaMKt,0,ndyads*sizeof(double));
   
    Cell.trel = (double *)malloc(ndyads*sizeof(double));memset(Cell.trel,0,ndyads*sizeof(double));
    Cell.Poryr = (double *)malloc(ndyads*sizeof(double));memset(Cell.Poryr,0,ndyads*sizeof(double));
    Cell.nactive = (int *)malloc(ndyads*sizeof(int));memset(Cell.nactive,0,ndyads*sizeof(int));

    //malloc unit ptr within cell
    Cell.unitsInCell = (unit *)malloc(ndyads*sizeof(unit));

    //init the value of 1d mem
    for(int i=0; i<ndyads;i++)
    {
        Cell.CaMKt[i] = 0.009;
        Cell.nca[i] = 0.001;
        Cell.unitsInCell[i].nryr = nryr;
        initUnitValue(Cell.unitsInCell[i]);
    }

    //malloc random generator
    Cell.pRD = (struct RandData *)malloc(sizeof(struct RandData));
    //Cell.randcallcount  = 0;
    Cell.timestep = 0;

    return;
}


void releaseCell(struct cell &Cell)
{
    int ndyads = Cell.unitNum_x;
    for(int i=0; i<ndyads;i++)
        releaseUnit(Cell.unitsInCell[i]);
    free(Cell.unitsInCell);

    free(Cell.pRD);

    free(Cell.nca);
    free(Cell.CaMKt);
    free(Cell.trel);
    free(Cell.Poryr);
    free(Cell.nactive);

    return;
}

void compute_cell_id(struct cell & Cell, int cx, int cy, RectPart2D* partitioner)
{
    int total_N_x =  partitioner->total_N_x();
    int total_N_y = partitioner->total_N_y();
    int my_offset_x = partitioner->offset_x();
    int my_offset_y = partitioner->offset_y();

    int cell_id = (my_offset_y + cy - 1)*total_N_x + my_offset_x + cx -1;
    Cell.cellID = cell_id+1;//id from 1
    //Cell.cellID = 1;

    /*int my_rank_x = partitioner->my__rank_x();
    int my_rank_y = partitioner->my__rank_y();
    int sub_Nx = partitioner->sub_N_x();
    int sub_Ny = partitioner->sub_N_y();
    int cell_id = my_rank_y*((sub_Ny-2)*total_N_x)+my_rank_x*((sub_Ny-2)*(sub_Nx-2))+(sub_Nx-2)*(cy-1)+(cx-1);
    Cell.cellID = cell_id;*/
    Cell.pRD->myrandomseed = -Cell.cellID;
    Cell.pRD->iy = 0;
    Cell.pRD->randcallcount = 0;

    Cell.totalCellSize = total_N_x*total_N_y;


    return;

}

